import traceback
from datetime import date as current_date
from datetime import timedelta

import config
from common.sql_helpers import connect_to_mssql_server
from employee_leave_check.employee_leave_config import LEAVE_ERROR_MSG, NO_REPORTEE_FOUND_MSG, \
    ERR_MSG_KEY, DURATION_MISSING_MSG, DURATION_MISSING_MSG_NOT_TODAY


def validate_manager(user_id, cursor):
    """
    user_id:user id of user
    cursor: cursor to scan database
    """
    employee_list = []
    if cursor is not None:
        # start = time.time()
        query = "select OrganizationID,EmployeeID from dbo.HrEmployee where UserID=%s"
        cursor.execute(query, user_id)
        org_id, emp_id = cursor.fetchall()[0]
        query = "select EmployeeID from dbo.HrEmployee where L1ManagerID=%s and OrganizationID=%s"
        cursor.execute(query, (emp_id, org_id))
        emp_id = cursor.fetchall()
        for employee in emp_id:
            employee_list.append(employee[0])

    return employee_list


def get_employee_leave_details(employee_list, curr_date, end_date, crsr):
    """
    employee_list: List of employees under user_id
    curr_date:  end date of given duration
    start_date: today as start date
    cursor: connection to scan database
    """
    leave_data = []
    if crsr is not None:
        # query = " select FirstName, LastName, LeaveDuration,TheDate from " \
        #         "(select NameAsPerAdhaar,LeaveDuration,TheDate,UserID " \
        #         " from ( select EmployeeID,LeaveDuration," \
        #         " FromDateID,b.TheDate,ToDateID FROM  TmEmployeeLeave a left join" \
        #         " TimeDimension b on a.FromDateID = b.TimeDimensionID where " \
        #         " EmployeeID in %s and b.TheDate <=%s and " \
        #         " b.TheDate >=%s)" \
        #         " c left join HrEmployee d on c.EmployeeID = d.EmployeeID) temp inner join " \
        #         " SysUser su on temp.UserID = su.UserID " \
        #         "order by FirstName, LastName, TheDate"
        query = " select FirstName, LastName, LeaveDuration, y.TheDate as start_date,end_date from " \
                " (select FirstName, LastName, LeaveDuration,TheDate as end_date,FromDateID " \
                " from (select NameAsPerAdhaar,LeaveDuration,TheDate,UserID,FromDateID " \
                " from ( select EmployeeID,LeaveDuration, " \
                " FromDateID,b.TheDate,ToDateID FROM  TmEmployeeLeave a left join " \
                " TimeDimension b on a.ToDateID = b.TimeDimensionID where " \
                " EmployeeID in %s ) " \
                " c left join HrEmployee d on c.EmployeeID = d.EmployeeID) temp inner join " \
                " SysUser su on temp.UserID = su.UserID ) x left join  TimeDimension y on x.FromDateID = y.TimeDimensionID   " \
                " where ( end_date >=%s and  " \
                " y.TheDate <=%s) " \
                "  order by FirstName, LastName, TheDate"

        crsr.execute(query, (employee_list, curr_date, end_date))
        result = crsr.fetchall()
        leave_data = []
        for records in result:
            leave_start_date = records[3].strftime('%d %b, %Y')
            leave_end_date = records[4].strftime('%d %b, %Y')
            leave_duration = records[2]
            if leave_duration == None:
                leave_duration = (records[4] - records[3]).days + 1
            leave_data.append(
                [str(records[0]) + " " + str(records[1]), leave_start_date, leave_end_date, leave_duration])
        crsr.close()
    return leave_data


def get_leave_data(query_text, user_id):
    employee_leave_response = {ERR_MSG_KEY: LEAVE_ERROR_MSG}
    query_text = query_text.lower()
    cursor = None
    try:
        # user_id = 2900754
        # user_id = 2060312
        duration_not_given = 0
        duration = ""
        cursor = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_list = validate_manager(user_id, cursor)
        if employee_list:
            curr_date = current_date.today()
            # curr_date = datetime.strptime('2019-10-02','%Y-%m-%d')
            if "today" in query_text or query_text == '1':
                end_date = curr_date
                duration = "today"
            elif "tomorrow" in query_text or query_text == '2':
                end_date = curr_date + timedelta(days=1)
                duration = "tomorrow"
            elif any(val in query_text for val in ["2 week", "two week"]) or query_text == '4':
                end_date = curr_date + timedelta(days=14)
                duration = "2 weeks"
            elif any(val in query_text for val in ["1 week", "one week", "next week", "week"]) or query_text == '3':
                end_date = curr_date + timedelta(days=7)
                duration = "1 week"
            elif "month" in query_text or query_text == '5':
                end_date = curr_date + timedelta(days=30)
                duration = "1 month"
            else:
                duration_not_given = 1
                end_date = curr_date
                duration = "today"
            if end_date is not None:
                leave_data = get_employee_leave_details(employee_list, curr_date, end_date, cursor)
                employee_leave_response = {"leave_details": leave_data}
                if duration_not_given == 1:
                    employee_leave_response["more_options"] = DURATION_MISSING_MSG_NOT_TODAY
            else:
                employee_leave_response = {"duration_missing": DURATION_MISSING_MSG}
        else:
            employee_leave_response = {ERR_MSG_KEY: NO_REPORTEE_FOUND_MSG}
    except Exception:
        print("Employee-Leave check handler", traceback.format_exc())
    try:
        cursor.close()
    except:
        pass
    return employee_leave_response, duration


if __name__ == "__main__":
    print(get_leave_data("I want to see leave of my employees for next 1 month", 2060312))
