LEAVE_ERROR_MSG = "Error fetching leave data, Please try again"
NO_REPORTEE_FOUND_MSG = "Sorry, you do not have any direct reportees."
DURATION_MISSING_MSG = "Please type the corresponding number to check for different dates:<br>" \
                       " 0. Today<br>" \
                       " 1. Tomorrow<br>" \
                       " 2. One Week<br>" \
                       " 3. Two Weeks<br>" \
                       " 4. One Month"
#
# DURATION_MISSING_MSG = "Please enter duration value:<br>" \
#                        "- 1 (Today)<br>" \
#                        "- Tomorrow<br>" \
#                        "- 1 week<br>" \
#                        "- 2 week<br>" \
#                        "- 1 month"

DURATION_MISSING_MSG_NOT_TODAY = "Please type the corresponding number to check for different dates:<br>" \
                                 " 1. Tomorrow<br>" \
                                 " 2. One Week<br>" \
                                 " 3. Two Weeks<br>" \
                                 " 4. One Month"

# DURATION_MISSING_MSG_NOT_TODAY = "To see leaves for other duration, please type a value from below:<br>" \
#                        "- Tomorrow<br>" \
#                        "- 1 week<br>" \
#                        "- 2 week<br>" \
#                        "- 1 month"

DURATION_MISSING_KEY = "duration_missing"
ERR_MSG_KEY = "err_msg"
