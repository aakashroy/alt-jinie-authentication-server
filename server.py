"""Alt Jinie Authentication Server"""
import base64
import copy
import json
import os
import re
import traceback
import uuid
from datetime import datetime, timedelta
from urllib import urlencode

import boto3
import bottle
import emails
import requests
from dateutil import parser
from jiniepush.jinie_notifications import send_meeting_reminder_card
from common import teams_tasks
from jinja2 import Environment, FileSystemLoader

import alt_jinie_actions
import alt_jinie_topics
import config
import experimental_features
import jinie_notification_messaging_templates
import ask_jinie.zoho_crm.zohocrm_helpers as zohocrm_helpers
from active_mq_conn_mgr import ActiveMQConnectionManager
from apis import scheduler_helper_apis
from ask_jinie import entrypoint, artefact_flows, zippi_task
from ask_jinie.artefact_flows import artefact_flow_handler
from ask_jinie.calendars.meeting_times_helpers import cancel_meeting
from ask_jinie.google_meetings import google_meetings
from ask_jinie.msteams_meetings.msteams_meetings import create_msteams_meeting
from ask_jinie.zippi_task import task_functions
from ask_jinie.zoho_meetings import zoho_meetings
from common import mongo_helpers, logging, optimizations, emails
from common.generic_functions import get_first_name, get_outlook_teaser, get_zoho_teaser
from common.jinie_response_helpers import create_jinie_response
from common.mongo_helpers import get_refresh_token_from_alt_id, \
    fetch_subscription_pending_users, fetch_org_configuration, \
    update_user_with_next_motd_time, mongo_update_user_timezone, mongo_update_zoho_meeting_token, \
    get_org_id_from_alt_id, update_approved_rejected_email_details, get_user_location
from common.o365_helpers import o365_meeting_handler, reply_all, get_user_data
from common.optimizations import json_dumps_minify
from common.security import ps_decrypt, ps_encrypt
from conversational_flows import handle_conversational_flows
from diy import diy_index
from email_handling.incoming_mail_handler import handle_incoming_email
from employee_leave_check.employee_leave_config import DURATION_MISSING_MSG, ERR_MSG_KEY, DURATION_MISSING_KEY
from employee_leave_check.employee_leave_data import get_leave_data
from experimental_features import experimental_handler
from idea_generation import idea_generation as idea_generation_root
from idea_generation import intent_mapping as idea_generation_intents
from msgraphhelpers import make_api_call
from reminders import qilo_reminders
from session_cache import SessionCache, ConversationalFlowCache
from weather import weather_api_helper

user_ids_filter = set([1077498, 1628060, 1630376, 1653286, 1736034, 1749823, 1774591, 1827260, 1883740, 1929074,
                       2060312, 2165707, 2205395, 2207564, 2269784, 2653243, 2721489, 2809629, 28298, 28404, 3195257,
                       3218689, 3226554, 3228916, 3593659, 3593663, 3714697, 4109416, 4109424, 4109427, 4258171])

import xml.etree.ElementTree as ET
from bottle import response as bottle_response

os.environ['OAUTHLIB_RELAX_TOKEN_SCOPE'] = '1'
os.environ['OAUTHLIB_IGNORE_SCOPE_CHANGE'] = '1'

ROOT_FOLDER = os.path.abspath(os.path.dirname(__file__))

bottle.TEMPLATE_PATH = ['./static/templates']


@bottle.route('/')
@bottle.view('index.html')
def index(user_id=""):
    """
    Index path function
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    if not user_id:
        user_id = "-1"

    return {"user_id": user_id}


# #TODO: Web Implementation
# @bottle.route('/create_bulk_tasks')
# @bottle.view('bulk_create_task.html')
# def task():
#     return
#
#
# #TODO: Web Implementation
# @bottle.route('/bulk_task_creation', method="POST")
# def create_task():
#     proceed = False
#     uploaded_file = None
#     try:
#         uploaded_file = bottle.request.files.filename
#         uploaded_file_prefix, uploaded_file_extension = os.path.splitext(uploaded_file.filename)
#         if uploaded_file.file and uploaded_file_extension != ".csv":
#             proceed = True
#     except Exception:
#         traceback.print_exc()
#     if not proceed:
#         return btc_error_codes.ERROR_MESSAGE_CODES["EC0001"] + " | Filename used: {}".format(uploaded_file.filename)
#
#     try:
#         error_code = None
#         csv_reader = csv.reader(uploaded_file.file, delimiter=',')
#         header = ""
#         num_rows = 0
#         for row in csv_reader:
#             if not header:
#                 for item in row:
#                     header = header + item.strip()
#                 if header != "Title,Assigner,Assignee,Workgroup,Due Date,Description":
#                     error_code = btc_error_codes.ERROR_MESSAGE_CODES["EC0002"] + " | Header was: {}".format(header)
#                     break
#             else:
#                 num_rows = num_rows + 1
#                 if num_rows > 5000:
#                     error_code = btc_error_codes.ERROR_MESSAGE_CODES["EC0004"]
#                     break
#         if error_code:
#             return error_code
#         elif num_rows == 0:
#             return btc_error_codes.ERROR_MESSAGE_CODES["EC0003"]
#         else:
#             # Everything looks good, insert into mongo and enqueue
#             pass
#     except Exception:
#         traceback.print_exc()
#         return "Some issue occurred in processing you request, please go back and try again."


@bottle.route('/static/<filepath:path>')
def server_static(filepath):
    """
    Handler for static files, used with the development server.
    :param filepath: Filepath
    :return:
    """
    abs_path = os.path.join(ROOT_FOLDER, 'static')
    abs_path = abs_path + "/" + filepath + ".gz"
    if os.path.isfile(abs_path):
        filepath = filepath + ".gz"
    return bottle.static_file(filepath, root=os.path.join(ROOT_FOLDER, 'static'))


@bottle.route('/fulfillment_webhook', method="POST")
def fulfillment_webhookfulfillment_webhook():
    postdata = json.loads(bottle.request.body.read())
    session = postdata.get("session", "")
    if session:
        user_id = session.split("/")[-1].split("_")[0]
        print("User ID: " + user_id)
        qr = postdata.get("queryResult", {})
        intent_display_name = qr.get("intent", {}).get("displayName", "")
        print(intent_display_name)
        print(idea_generation_intents.INTENT_MAP)
        if intent_display_name in idea_generation_intents.INTENT_MAP:
            response = idea_generation_root.intent_fulfill(user_id, qr)
            return {'fulfillmentText': response}
        elif intent_display_name in ["Outlook_Jinie", "MOTD_TEASER"]:
            response_content = get_outlook_teaser(user_id)
            response_content = json.dumps(response_content)
            return {'fulfillmentText': response_content}
        elif intent_display_name == "ML_ZOHO_LINK_ACCOUNT":
            response_content = get_zoho_teaser(user_id)
            response_content = json.dumps(response_content)
            print(response_content)
            return {'fulfillmentText': response_content}
        elif intent_display_name == "weather":
            qt = qr.get("queryText", "")
            params = qr.get("parameters", {})
            city_name = params.get("geo-city", "")
            response = weather_api_helper.weather_report_by_city(city_name, True)
            return {'fulfillmentText': response}
        elif intent_display_name == "AIR_QUALITY_DATA":
            params = qr.get("parameters", {})
            city_name = params.get("geo-city", "")
            response = weather_api_helper.aq_report_by_city(city_name, True)
            return {'fulfillmentText': response}

        elif intent_display_name == "MOTD_LINK_REMINDER":
            user_id = int(user_id)
            orgId = get_org_id_from_alt_id(user_id, config)
            result = fetch_org_configuration(orgId, config)
            for res in result:
                conf = res["motd_remind_after_days"]
            print(conf)
            after = (datetime.now() + timedelta(days=conf))
            print(after)
            date = str(after.date())
            time_after = after.strftime("%H:%M:%S")
            date_time = date + " " + time_after
            update_user_with_next_motd_time(user_id, date_time)
            return {'fulfillmentText': "Sure! I will remind you later"}
        elif intent_display_name == "ML_SCHEDULE_MEETING":
            qt = qr.get("queryText", "")
            if qt:
                sc = SessionCache.get()
                if sc.get(user_id, None) is None:
                    sc[user_id] = dict({"attendees": ""})
                    print("Saved user id: {}".format(user_id))
                if qr.get("parameters", {}).get("attendees", ""):
                    if not sc[user_id]["attendees"]:
                        sc[user_id]["attendees"] = qt
                        print("Saved attendees {} for user id {}".format(qt, user_id))
                else:
                    sc[user_id]["attendees"] = ""
                logging.log(logging.SEVERITY_DEBUG, "FULLFILMENT-WEBHOOK", "Webhook Call",
                            {"session": postdata})
        elif intent_display_name == "ML_TEAMS_MEETING":
            params = qr.get("parameters", {})
            meeting_subject = params.get("subject", None)
            meeting_start_time = params.get("meeting_start_time", {}).get("date_time", None)
            if meeting_subject and meeting_start_time:
                response = create_msteams_meeting(user_id, meeting_subject, meeting_start_time)
            else:
                response = "I need the meeting subject and the meeting start time to continue. Please try again."
            print(response)
            return {'fulfillmentText': response}
        elif intent_display_name == "ML_CHECK_REPORTEE_LEAVES" or \
                intent_display_name == "ML_CHECK_REPORTEE_LEAVE_END_CONVERSATION":
            qt = qr.get("queryText", "")
            employee_leave_response, duration = get_leave_data(qt, user_id)
            if "leave_details" in employee_leave_response:
                leave_date = employee_leave_response["leave_details"]
                within_str = "within "
                if duration == "today":
                    within_str = ""
                if len(leave_date) > 0:
                    response_str = "Below team member(s) are on leave " + within_str + "<b>" + duration + "</b>"
                    for leaves in leave_date:
                        response_str = response_str + "<br><b>" + "- " + leaves[0] + "</b>" + " is on " + "<b>" + str(
                            int(leaves[3])) + "</b>" + " day leave from " + "<b>" + leaves[1] + "</b>" + " to " \
                                       + "<b>" + leaves[2] + "</b>"

                    return {'fulfillmentText': response_str}

                else:
                    if "more_options" in employee_leave_response:
                        print("here")
                        response_str_more_options = "None of your team members are on leave " + within_str + "<b>" + duration + "</b>" + "<br>" + \
                                                    employee_leave_response["more_options"]
                        return {'fulfillmentText': response_str_more_options}
                    else:
                        return {
                            'fulfillmentText': "None of your team members are on leave " + within_str + "<b>" + duration + "</b>"}
            else:
                if DURATION_MISSING_KEY in employee_leave_response:
                    return {'fulfillmentText': DURATION_MISSING_MSG}
                else:
                    final_response = employee_leave_response.get(ERR_MSG_KEY)
                    return {'fulfillmentText': final_response}
        else:
            cfc = ConversationalFlowCache.get()
            result = handle_conversational_flows(postdata, cfc)
            if result:
                return result
            else:
                qt = qr.get("queryText", "")
                data = {"sender": user_id, "message": qt}
                headers = {
                    "Content-Type": "application/json"
                }
                res = requests.post(config.RASA_NLU_URL, json=data, headers=headers)
                # res = requests.post("https://sohum-services.peoplestrong.com/api/nlu_model/", json=data, headers=headers)

                postdata = json.loads(res.text)

                logging.log(logging.SEVERITY_DEBUG, "RASA-QUERY", "Dumping response from RASA",
                            {"rasa_response": postdata, "query": qt})

                if "fullfillmentText" in postdata:
                    print("fullfillmentText")
                    result = postdata["fullfillmentText"]
                    result_str = ""
                    for rows in result:
                        result_str = result_str + rows + "<br>"
                    return {'fulfillmentText': result_str}
                elif "fullfillmentText_Other" in postdata:
                    print("fullfillmentText_Other")
                    print(postdata)
                    result = postdata["fullfillmentText_Other"]
                    return {'fulfillmentText': result}
                else:
                    print("followupEventInput")
                    return {
                        "followupEventInput":
                            {"name": postdata.get("event_name")
                             }
                    }

    return postdata


@bottle.route('/login', method="POST")
def login():
    """
    Login Page
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    params = {
        'client_id': config.CLIENT_ID,
        'redirect_uri': config.REDIRECT_URI,
        'response_type': 'code',
        'scope': ' '.join(unicode(i) for i in config.SCOPES),
        'state': user_id
    }

    signin_url = config.AUTH_ENDPOINT.format(urlencode(params))

    return bottle.redirect(signin_url)



@bottle.route('/login/authorized')
def authorized():
    """
    Authorization check
    :return:
    """
    # import pdb
    # pdb.set_trace()
    logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Started processing an onboarding request")
    name = "User"
    is_authorized = False
    me_response_json = None
    try:
        user_id = bottle.request.params.get('state')

        auth_code = bottle.request.params.get('code')
        post_data = {'grant_type': 'authorization_code',
                     'code': auth_code,
                     'redirect_uri': config.REDIRECT_URI,
                     'scope': ' '.join(unicode(i) for i in config.SCOPES),
                     'client_id': config.CLIENT_ID,
                     'client_secret': config.CLIENT_SECRET
                     }
        print "post data : ", json.dumps(post_data, indent=4)
        tokens = requests.post(config.TOKEN_ENDPOINT, data=post_data)
        tokens_json = tokens.json()
        # print "token json :",tokens_json
        logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Dumping Tokens",
                    {"post_data": post_data, "tokens_json": tokens_json})

        get_me_url = config.MS_GRAPH_ENDPOINT.format('/me')
        me_response = make_api_call('GET', get_me_url, tokens_json['access_token'])
        me_response_json = me_response.json()
        if me_response_json.get("mail") is None:
            me_response_json["mail"] = me_response_json.get("userPrincipalName")

        logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "User Details from MS",
                    {"user_ms_data": me_response_json})

        if me_response_json.get("givenName", None) and me_response_json.get("mail", None):

            # name = me_response_json.get("givenName")
            user_details = None
            if user_id and user_id != "-1":
                user_details = mongo_helpers.get_user_details_by_alt_id(user_id, config)

            if user_details is None:
                user_details = mongo_helpers.get_user_details_from_email(me_response_json["mail"], config)

                if not user_details and "userPrincipalName" in me_response_json:
                    user_details = mongo_helpers.get_user_details_from_email(me_response_json["userPrincipalName"],
                                                                             config)

                logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "User Details from Mongo",
                            {"user_mongo_data": user_details})

            name = get_first_name(user_details["first_name"], user_details["middle_name"], user_details["last_name"])

            if user_details is not None and 'enabled' in user_details['allowed_services'] \
                    and user_details['allowed_services']['enabled'] is True:
                logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Enqueuing . . .")
                message = copy.deepcopy(jinie_notification_messaging_templates.ONBOARDING_MESSAGE)
                message['message_type'] = alt_jinie_topics.ONBOARDING
                message['destination_alt_user_id'] = user_details["alt_id"]
                message['emp_id'] = user_details["emp_id"]
                message['allowed_services'] = user_details['allowed_services']
                message['tokens'] = tokens_json
                message['me'] = me_response_json
                ActiveMQConnectionManager.send(destination=config.ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE,
                                               body=optimizations.json_dumps_minify(message),
                                               headers={'persistent': 'true'})
                logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Enqueued . . .")
                is_authorized = True
                logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Success",
                            {"name": me_response_json["givenName"], "mail": me_response_json["mail"]})
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "USER-ONBOARDING", traceback.format_exc(),
                    {"name": me_response_json.get("givenName", "NA"),
                     "mail": me_response_json.get("mail", "NA")} if me_response_json is not None else None)

    if is_authorized:
        redirect_suffix = '/hello/' + name
    else:
        redirect_suffix = '/oops/' + name

    return bottle.redirect(config.SUBDOMAIN + redirect_suffix)


@bottle.route('/hello/<name>')
@bottle.view('hello.html')
def hello(name=""):
    """
    Greeting on onboarding user
    :param name:
    :return:
    """
    return {"name": name}


@bottle.route('/oops/<name>')
@bottle.view('oops.html')
def oops(name=""):
    """
    Error Page while onboarding user
    :param name: Name
    :return:
    """
    return {"name": ", " + name}


@bottle.route('/update_timezone', method="POST")
def update_user_timezone():
    """
        Update Timezone API
        :return:
    """
    postdata = {}
    json_response = {"success": False}
    try:
        postdata = json.loads(bottle.request.body.read())
        logging.log(logging.SEVERITY_DEBUG, "ASKJINIE", "Updatetimezone Recieved processing a request", postdata)
        mongo_update_user_timezone(int(postdata.get("user_id", -1)), postdata.get("timezone", "Asia/Kolkata"), config)
        json_response["success"] = True
        return json_response
    except Exception:
        exception_string = traceback.format_exc()
        logging.log(logging.SEVERITY_CRITICAL, "ASKJINIE", exception_string, postdata)
        json_response["status"] = exception_string
        return json_response


@bottle.route('/get_timezone', method="GET")
def get_user_timezone():
    tzinfo = "Asia/Kolkata"
    user_dtls = mongo_helpers.get_user_details_by_alt_id(bottle.request.params.get('user_id'), config)
    if user_dtls:
        tzinfo = user_dtls.get("timezone", tzinfo)
    return tzinfo


@bottle.route('/actions/o365/emails/approve', method="GET")
@bottle.view('go_back_zippi.html')
def approve_email():
    print("Approve email called | approve_email")
    alt_id = bottle.request.params.get('alt_id')
    o365_email_resource_id = bottle.request.params.get('id')
    user_data = get_user_data(alt_id, config)
    approval_email_action = "<b>You have already taken action on this task.</b>"
    display_msg = ""
    if user_data and o365_email_resource_id not in user_data.get("approved_rejected_mail_ids", []):
        approved_rejected_email_list = user_data.get("approved_rejected_mail_ids", [])
        approved_rejected_email_list.append(o365_email_resource_id)
        approved_rejected_email_list = list(set(approved_rejected_email_list))
        approved_email_list = user_data.get("approved_mail_ids", [])
        approved_email_list.append(o365_email_resource_id)
        approved_email_list = list(set(approved_email_list))
        approved_rejected_task_email_list = user_data.get("approved_rejected_task_mail_ids", [])
        approved_rejected_task_email_list.append(o365_email_resource_id)
        approved_rejected_task_email_list = list(set(approved_rejected_task_email_list))
        update_approved_rejected_email_details(int(alt_id), approved_rejected_task_email_list,
                                               approved_rejected_email_list, approved_email_list, config,
                                               True)
        reply_all(o365_email_resource_id, user_data, config, True)
        display_msg = "<b>You have approved the email!</b> \n\n<b>From:</b> {} \n\n<b>Subject:</b> {}".format(
            base64.urlsafe_b64decode(bottle.request.params.get('frm')),
            base64.urlsafe_b64decode(bottle.request.params.get('subj'))
        )
        approval_email_action = "You have approved using Jinie :)"

    return {
        "approval_email_action": approval_email_action,
        "approval_text": display_msg
    }


@bottle.route('/actions/o365/emails/reject', method="GET")
@bottle.view('go_back_zippi.html')
def reject_email():
    print("Reject email called | reject_email")
    alt_id = bottle.request.params.get('alt_id')
    o365_email_resource_id = bottle.request.params.get('id')
    user_data = get_user_data(alt_id, config)
    approval_email_action = "<b>You have already taken action on this task.</b>"
    display_msg = ""
    if user_data and o365_email_resource_id not in user_data.get("approved_rejected_mail_ids", []):
        approved_rejected_email_list = user_data.get("approved_rejected_mail_ids", [])
        approved_rejected_email_list.append(o365_email_resource_id)
        approved_rejected_email_list = list(set(approved_rejected_email_list))
        rejected_email_list = user_data.get("rejected_mail_ids", [])
        rejected_email_list.append(o365_email_resource_id)
        rejected_email_list = list(set(rejected_email_list))
        approved_rejected_task_email_list = user_data.get("approved_rejected_task_mail_ids", [])
        approved_rejected_task_email_list.append(o365_email_resource_id)
        approved_rejected_task_email_list = list(set(approved_rejected_task_email_list))
        update_approved_rejected_email_details(int(alt_id), approved_rejected_task_email_list,
                                               approved_rejected_email_list, rejected_email_list, config,
                                               False)
        reply_all(o365_email_resource_id, user_data, config, False)
        display_msg = "<b>You have rejected the email!</b> \n\n<b>From:</b> {} \n\n<b>Subject:</b> {}".format(
            base64.urlsafe_b64decode(bottle.request.params.get('frm')),
            base64.urlsafe_b64decode(bottle.request.params.get('subj'))
        )
        approval_email_action = "You have rejected using Jinie :)"

    return {
        "approval_email_action": approval_email_action,
        "approval_text": display_msg
    }


@bottle.route('/listeners/emails/o365/created', method="POST")
def listen_o365_email_created():
    try:
        if bottle.request.query.validationToken:
            logging.log(logging.SEVERITY_DEBUG, "O365_EMAIL_HANDLER", "Incoming Webhook test from Microsoft")
            return bottle.request.query.validationToken

        postdata = json.loads(bottle.request.body.read())
        message = {
            "postdata": postdata,
            "action": "email/o365/created"
        }
        logging.log(logging.SEVERITY_DEBUG, "O365_EMAIL_HANDLER", "Incoming Email", message)

        if config.EMAIL_HANDLING_LOGIC == "SERVER":
            handle_incoming_email(postdata, config)
            logging.log(logging.SEVERITY_DEBUG, "MEETING-ACTIONS", "Processed")
        elif "http" in config.EMAIL_HANDLING_LOGIC:
            requests.post(url=config.EMAIL_HANDLING_LOGIC, json=postdata)
        elif config.EMAIL_HANDLING_LOGIC == "IGNORE":
            logging.log(logging.SEVERITY_DEBUG, "O365_EMAIL_HANDLER", "Incoming Email handling is disabled.")
        else:
            ActiveMQConnectionManager.send(
                destination=config.ACTIVE_MQ_INCOMING_EMAIL_QUEUE,
                body=optimizations.json_dumps_minify(message),
                headers={'persistent': 'true'}
            )

            logging.log(logging.SEVERITY_DEBUG, "MEETING-ACTIONS", "Enqueued")
    except Exception:
        print("Caught general exception in listen_o365_email_created:")
        traceback.print_exc()

    bottle.response.status = 202
    return ""


@bottle.route('/motd_adoption', method="POST")
def motd_adoption():
    """
            send email API
            :return:
    """
    postdata = {}
    json_response = {"success": False}

    try:

        postdata = json.loads(bottle.request.body.read())

        user_id = postdata.get("user_id", "")
        orgid = postdata.get("orgId", "")

        now = datetime.now()

        result = fetch_org_configuration(orgid, config)

        for doc in result:
            conf = doc["motd_remind_after_days"]

        after = (datetime.now() + timedelta(days=conf))

        date = str(after.date())
        time_after = after.strftime("%H:%M:%S")

        date_time = date + " " + time_after
        update_user_with_next_motd_time(user_id, date_time)

        json_response["success"] = True

        return json_response
    except Exception:
        exception_string = traceback.format_exc()
        print(exception_string)
        json_response["status"] = exception_string
        return json_response


@bottle.route('/get_users_list', method="POST")
def get_users_list():
    """
            send email API
            :return:
    """
    postdata = {}
    json_response = {"success": False}
    response_type = []
    try:

        postdata = json.loads(bottle.request.body.read())

        orgid = postdata.get("orgId", "")
        groupid = postdata.get("groupId", "")

        record = fetch_subscription_pending_users(orgid, config)

        users_list = []
        for alt_id in record:
            users_list.append(alt_id["alt_id"])

        print(users_list)

        url = "https://xmppaltmessanger.peoplestrong.com/XmppJinie/internal/getGroupSubscribers";
        data = {"groupId": groupid}
        headers = {"Content-Type": "application/json"}

        group_subscribers = requests.post(url, json=data, headers=headers)

        response_data = group_subscribers.json()["responseData"]
        group_users = response_data["Subscribers"]

        pending_users_notification = []
        for i in group_users:
            print(i)
            if i in users_list:
                pending_users_notification.append(i)

        json_response["success"] = True
        json_response["results"] = pending_users_notification

        return json_response

    except Exception:
        exception_string = traceback.format_exc()
        print(exception_string)
        json_response["status"] = exception_string
        return json_response


@bottle.route('/askjinie', method="POST")
def ask_jinie_endpoint():
    """
    Ask Jinie Endpoint
    :return:
    """
    message = ""
    postdata = {}
    json_response = {"success": False}
    response_type = ""
    intent = ""
    try:
        print(bottle.request.body.read())
        postdata = json.loads(bottle.request.body.read())
        logging.log(logging.SEVERITY_DEBUG, "ASKJINIE", "Started processing a request", postdata)
        intent = postdata.get("intent", "")
        skills = postdata.get("skills", [])
        blood_group = postdata.get("blood_group", "")
        blood_confirmation = postdata.get("blood_confirmation", "")
        attendees = postdata.get("attendees", [])
        response_type = postdata.get("response_type", "")

        alt_user_id = int(postdata.get("user_id", -1))
        duration = postdata.get("duration", 30)  # in minutes

        sc = SessionCache.get()
        meeting_attendees = sc.get(str(alt_user_id), {}).get("attendees", "")
        sc.pop(alt_user_id, None)

        minute_variation = ['minutes', 'mins', 'mins.', 'min', 'min.']
        if isinstance(duration, dict):
            amount = duration.get('amount')
            unit = duration.get('unit')
            if unit in minute_variation:
                unit = "min"
            elif unit == 'h':
                unit = int(amount) * 60
            duration = str(amount) + " " + unit
        if duration == '':
            duration = 30

        query = postdata.get('query', "")
        meeting_location = postdata.get('meeting_location', 'Office')

        if not isinstance(duration, int):
            duration = int(duration)

        additional_parameters = {
            'skills': skills,
            'blood_group': blood_group,
            'blood_confirmation': blood_confirmation,
            'attendees': attendees,  # string
            'duration': duration,
            # 'when': when,
            'title': "Meeting created using jinie",
            'meeting_start_time': postdata.get("meeting_start_time", ""),
            'meeting_date': postdata.get("meeting_date", ""),
            'meeting_location': meeting_location,
            'mail_body': 'Blocking your calender for a Jinie Invite',
            'query': query.lower(),
            'ml_course_price': postdata.get("ml_course_price", "paid"),
            'ml_course_level': postdata.get("ml_course_level", "all"),
            "org_id": int(postdata.get("org_id", -1)),
            "email_id": postdata.get("email_id", -1),
            "msteams_meeting_start_time": postdata.get("msteams_meeting_start_time", ""),
            "subject": postdata.get("subject", ""),
            "meeting_attendees": meeting_attendees if meeting_attendees else postdata.get("meeting_attendees", ""),
            "meeting_duration": postdata.get("meeting_duration", ""),
            "meeting_title": postdata.get("meeting_title", ""),
            "meeting_info": postdata.get("meeting_info", ""),
            "mail_id": postdata.get("mail_id", ""),
            "approver_email": postdata.get("approver_email", ""),
            "approvee_alt_id": postdata.get("alt_id", ""),
            "email_sub": postdata.get("email_sub", ""),
            "task_creator_name": postdata.get("creator_name", ""),
            "task_creator_alt_id": postdata.get("creator_userid", ""),
            "title_task": postdata.get("title", ""),
            "assignee_userid": postdata.get("assignee_userid", ""),
            "assignee_name": postdata.get("assignee_name", ""),
            "remark": postdata.get("remark", ""),
            "expiryTime": postdata.get("expiryTime", "")
        }

        if intent and alt_user_id > 0:

            if intent in artefact_flows.intent_mapping.INTENT_MAP:
                return artefact_flow_handler.artefact_flow_handler(intent, alt_user_id, additional_parameters)
            elif intent in experimental_features.intent_mapping.INTENT_MAP:
                return experimental_handler.handle_experimental_features(intent, alt_user_id, additional_parameters)
            elif intent in zippi_task.intent_mapping.INTENT_MAP:
                return task_functions.handle_approve_reject_createTask(intent,
                                                                       additional_parameters)

            # Separate the ask_jinie_handler into 2 parts (one for course reco)
            # Also ask_jinie_handler can return better error statuses
            handler_response, error_message = entrypoint.ask_jinie_handler(intent, alt_user_id, additional_parameters,
                                                                           3)
            # if response_type == "direct":
            #     return handler_response
            print "Inside Server"
            print handler_response, error_message
            if error_message:
                json_response["ex"] = error_message
            elif handler_response is not None:
                message = handler_response
                json_response["results"] = handler_response
                json_response["success"] = True
        else:
            json_response["ex"] = "Empty Intent or invalid user"

    except Exception:
        exception_string = traceback.format_exc()
        logging.log(logging.SEVERITY_CRITICAL, "ASKJINIE", exception_string, postdata)
        json_response["ex"] = exception_string

    logging.log(logging.SEVERITY_DEBUG, "ASKJINIE", "All done for this request",
                {'postdata': postdata, 'message': message})

    if response_type == "json":
        if "results" in json_response:
            if not isinstance(json_response["results"], list):
                json_response["results"] = [json_response["results"]]
            else:
                modified_results = []
                for result in json_response["results"]:
                    if "course_id" in result \
                            and "course_name" in result:
                        modified_results.append({
                            "course_id": result["course_id"],
                            "course_name": result["course_name"]

                        })
                json_response["results"] = modified_results

        return json_response
    else:
        return create_jinie_response(intent, json_response)
    # return {
    #     "messageCodeTO": {
    #         "status": "SUCCESS",
    #         "code": "S100",
    #         "displayMsg": message
    #     }
    # }


@bottle.route('/actions/calendar/create', method="GET")
@bottle.view('create_event.html')
def create_meeting_render_page():
    try:
        get_params = bottle.request.params.get('params')
        get_params = base64.urlsafe_b64decode(ps_decrypt(get_params))
        get_params_dict = json.loads(get_params)
        print get_params_dict
        return {"event_params": bottle.request.params.get('params'), "title": get_params_dict.get('t'),
                "attendees": get_params_dict.get('at'), "start_date_time_tz": get_params_dict.get('st'),
                "end_date_time_tz": get_params_dict.get('et'), "mbody": "This Event is Scheduled through Jinie"}
    except:
        return "<H2 style='text-align: center'>Invalid URL</H2>"


@bottle.route('/actions/calendar/create', method="POST")
def create_meeting():
    try:
        print "Inside Post"
        print bottle.request.body.read()
        title = bottle.request.forms.get('title')
        attendees_list = bottle.request.forms.get('attendees').strip("[]").split(", ")
        print attendees_list, type(attendees_list)
        start_date_time = bottle.request.forms.get('start_date')
        end_date_time = bottle.request.forms.get('end_date')
        event_params = bottle.request.forms.get('params')
        mbody = bottle.request.forms.get('mbody')
        # print title, attendees
        # postdata = json.load(bottle.
        # request.body.read())
        # print postdata
        postdata = base64.urlsafe_b64decode(ps_decrypt(event_params))
        postdata_dict = json.loads(postdata)
        refresh_token_actual = get_refresh_token_from_alt_id(postdata_dict.get('oai'), config)
        refresh_token_recieved = postdata_dict.get('rt')
        access_token = postdata_dict.get('atok')
        alt_id = postdata_dict.get('oai')
        # print refresh_token_recieved
        # print refresh_token_actual
        # print type(refresh_token_recieved)
        # print type(refresh_token_actual)
        # print start_date_time
        # print end_date_time
        sdp = parser.parse(start_date_time)
        edp = parser.parse(end_date_time)
        # attendees_list = postdata_dict.get('at')
        attendees_list_of_dict = list()
        for al in attendees_list:
            al = al.strip("\'").strip('u\'')
            attendees_list_of_dict.append({'email': al, 'name': al})
        location = "Office"
        if refresh_token_actual == refresh_token_recieved:

            if sdp < edp:

                response = o365_meeting_handler(config,
                                                attendees_list_of_dict,
                                                start_date_time,
                                                end_date_time,
                                                access_token,
                                                title,
                                                location,
                                                mbody)
                if response is not None:
                    # generating open meeting and cancel meeting links here
                    meeting_id = response['id']
                    # print(meeting_id)
                    # send_custom_notification(alt_id, 'Meeting scheduled successfully <br/>'
                    #                                  'To cancel meeting <a href="https://www.google.com"> Click here </a>')
                    param_dict = {'meeting_id': str(meeting_id),
                                  'altid': str(alt_id)}
                    param_encode = base64.urlsafe_b64encode(json_dumps_minify(param_dict))  # give it a dictionary
                    cipher_params_get = ps_encrypt(param_encode)

                    # cancel_meeting_url = config.SUBDOMAIN + '/meeting/cancel?meetingid=' + str(meeting_id) + '&altid='\
                    #                      + str(alt_id)
                    cancel_meeting_url = config.SUBDOMAIN + '/meeting/cancel?params={}'.format(cipher_params_get)
                    open_meeting_url = response['webLink']

                    ########################################################

                    # print "Response: ", response
                    # send_simple(alt_id=str(alt_id),
                    #             message="Thanks! Meeting Has been Scheduled. <br> Kindly Check your Calendar in Outlook")
                    attendees = list()
                    for al in attendees_list_of_dict:
                        attendees.append({"name": al["name"], "id": al["email"]})
                    greeting = "Here are your Meeting Details"

                    date = re.findall("\d{4}-\d{2}-\d{2}", start_date_time)[0]
                    date = datetime.strptime(date, "%Y-%m-%d")
                    new_date = date.strftime("%d-%b")
                    s_time = re.findall("\d{2}:\d{2}", start_date_time)[0]
                    s_time = datetime.strptime(s_time, "%H:%M")
                    s_time = s_time.strftime("%I:%M %p")
                    e_time = re.findall("\d{2}:\d{2}", end_date_time)[0]
                    e_time = datetime.strptime(e_time, "%H:%M")
                    e_time = e_time.strftime("%I:%M %p")
                    meeting_time = new_date + " " + s_time + " - " + e_time

                    meeting_actions = [{"label": "Cancel Meeting", "val": cancel_meeting_url},
                                       {"label": "Open in Outlook", "val": open_meeting_url}]
                    # print "open meeting: ", open_meeting_url
                    send_meeting_reminder_card(str(alt_id), greeting, title, meeting_time, location, attendees,
                                               meeting_actions,
                                               verbose=False)

                    return "<p style='text-align: center'>Thanks! Meeting Has been Scheduled.<br> You will be notified through Jinie in a While.</p>"
                else:
                    return "Error! Please Contact Support"
            else:
                return "<p style='text-align: center'>End Date/time is Greater than Start Date/Time</p>"
        else:
            return "<p style='text-align: center'>Session Time Out Please Initiate new Session</p>"
    except Exception as e:
        print("exception: ", traceback.print_exc())
        return "<p style='text-align: center'>Microsoft Exception: " + str(e) + "</p>"


@bottle.route('/actions/calendar/<meeting_id>/<alt_user_id>/<action>')
@bottle.view('jinieaction.html')
def meeting(meeting_id, alt_user_id, action):
    """
    Handle Meeting
    :param meeting_id: MeetingId
    :param alt_user_id: AltUserId
    :param action: Action
    :return:
    """

    success = False
    message = dict()
    action_str = ""
    try:

        logging.log(logging.SEVERITY_DEBUG, "MEETING-ACTIONS", "Processing",
                    {"meeting_id": meeting_id, "alt_user_id": alt_user_id})

        message = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_REQUEST)
        message['message_type'] = alt_jinie_topics.ACTION
        message['destination_alt_user_id'] = int(alt_user_id)

        action_identified = True

        if action == alt_jinie_actions.ACTION_CALENDAR_LATE_STR:
            action_str = "\"I will be late\""
            message['action'] = {"meeting_id": meeting_id, "action": alt_jinie_actions.ACTION_CALENDAR_LATE}
        elif action == alt_jinie_actions.ACTION_CALENDAR_NOT_ATTENDING_STR:
            action_str = "\"I won't be attending\""
            message['action'] = {"meeting_id": meeting_id, "action": alt_jinie_actions.ACTION_CALENDAR_NOT_ATTENDING}
        elif action == alt_jinie_actions.ACTION_CALENDAR_ORGANIZER_LATE_STR:
            action_str = "\"Delaying the meeting\""
            message['action'] = {"meeting_id": meeting_id, "action": alt_jinie_actions.ACTION_CALENDAR_ORGANIZER_LATE}
        else:
            logging.log(logging.SEVERITY_ERROR, "MEETING-ACTIONS",
                        "No action could be identified | meeting_id: " + str(meeting_id) + " | alt_user_id: " + str(
                            alt_user_id) + " | action: " + str(action))
            action_identified = False

        if action_identified:
            logging.log(logging.SEVERITY_DEBUG, "MEETING-ACTIONS", "Enqueing", message)

            ActiveMQConnectionManager.send(destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                           body=optimizations.json_dumps_minify(message),
                                           headers={'persistent': 'true'})

            logging.log(logging.SEVERITY_DEBUG, "MEETING-ACTIONS", "Enqueued")

            success = True

    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MEETING-ACTIONS", traceback.format_exc(),
                    {"meeting_id": meeting_id, "alt_user_id": alt_user_id, "message": message})

    if success:
        return_dict = {
            "user_message_title": "I have received your request.",
            "user_message": "I am working on it. Your request for {} is been processed. To restart your conversation please go back to Jinie.".format(
                action_str
            )
        }
    else:
        return_dict = {
            "user_message_title": "Oops!",
            "user_message": "There was some error processing your request, please try again later."
        }

    return return_dict


@bottle.route('/jinie_tts', method="POST")
def jinie_text_to_speech():
    bottle_response.content_type = 'application/octet-stream'
    postdata = bottle.request.body.read()
    xml_body = ET.fromstring(postdata)
    text = xml_body.find('voice').text
    print text
    format = 'mp3'
    user = 'Aditi'
    try:
        # polly_client = boto3.client('polly')
        polly_client = boto3.Session(
            aws_access_key_id='AKIAJR46QX7INIHQWBCQ',
            aws_secret_access_key='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',
            region_name='us-west-2').client('polly')

        response_output = polly_client.synthesize_speech(OutputFormat=format, Text=text, VoiceId=user)
        print response_output
        if 'AudioStream' in response_output:
            return response_output['AudioStream']
        else:
            return {"status": "Failed to convert"}
    except Exception as e:
        print('synthesize_speech exception: ', e)
        return {"status": "Error"}


@bottle.route('/qilo_reminders', method="POST")
def jinie_weather_report():
    body = bottle.request.body
    headers = bottle.request.headers
    postdata = json.loads(bottle.request.body.read().decode('utf-8'))
    headerdata = {
        "api-access-token": headers["api-access-token"],
        "api-key": headers["api-key"],
        "api-org": headers["api-org"],
        "Content-Type": "application/json"}
    return qilo_reminders.qilo_team_reminders(postdata, headerdata)


@bottle.route('/jinie/organization_onboarding', method="POST")
def organization_onboarding():
    """
    Handle USER SYNCING CONFIGURATIONS FOR ONBOARDING ORGANIZATIONS
    :return:
    """

    postdata = json.loads(bottle.request.body.read().decode('utf-8'))
    org_id = int(postdata.get("org_id", 0))
    if org_id == 0:
        return {"status": False, "message": "Organization is mandatory"}
    else:
        result = mongo_helpers.update_org_config(postdata, config)
        if result['sync']:
            message = dict()
            message["org_id"] = org_id
            message['message_type'] = 22
            ActiveMQConnectionManager.send(destination="/queue/user_scheduling_queue",
                                           body=optimizations.json_dumps_minify(message),
                                           headers={'persistent': 'true'})
            print "syncing users data"
        return result


@bottle.route('/weather_report', method="POST")
def jinie_weather_report():
    body = bottle.request.body
    postdata = json.loads(bottle.request.body.read().decode('utf-8'))
    city_name = postdata.get("city_name", "")
    response = weather_api_helper.weather_report_by_city(city_name)
    return response


@bottle.route('/get_bulk_notifications', method="POST")
def get_wish_notification():
    try:
        postdata = json.loads(bottle.request.body.read().decode('utf-8'))

        org_id = postdata.get("org_id", 0)
        # user_text = postdata.get("user_text", 0)
        # dummy_user = postdata.get("dummy_user", 0)

        dummy_user = "3593663"
        user_text = "Holi"
        org_id = int(org_id)
        payload = {
            "text": user_text,
            "languageCode": "en",
            "reply": False,
            "paramMap": {},
            "userId": dummy_user
        }
        headers = {
            "Content-Type": "application/json",
            "Authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
        }
        res = requests.post("https://messenger.peoplestrong.com/XmppJinie/getNotificationPayloadFromDataSources",
                            json=payload, headers=headers)
        apiData = json.loads(res.text)
        print(apiData)
        xml = str(apiData["xml"])
        stanzaId = apiData["stanzaId"]
        fromJid = apiData["fromJid"]
        domain = apiData["domain"]

        user_ids_from_db = experimental_handler.wish_design_msg(int(org_id), config)
        if user_ids_filter:
            user_ids_from_db = list(set(user_ids_from_db).intersection(user_ids_filter))
        print(user_ids_from_db)
        messagePayload = []
        for user in user_ids_from_db:
            new_stanza_id = uuid.uuid1()
            print("New User" + str(user))
            print("New StanzaId" + str(new_stanza_id))
            xmluser = xml.replace(str(dummy_user), str(user))
            xmluser = xmluser.replace(str(stanzaId), str(new_stanza_id))
            userPayload = {
                "xml": xmluser,
                "stanzaId": str(new_stanza_id),
                "domain": domain,
                "fromJid": fromJid
            }
            messagePayload.append(userPayload)
        response = json.dumps(messagePayload)
        return response
    except Exception:
        traceback.print_exc()
        return {"status": False}


@bottle.route('/meeting/cancel')
def cancel_meeting_route():
    try:
        get_params = bottle.request.params.get('params')  # using params parameter to extract altid and meetingid
        get_params = base64.urlsafe_b64decode(ps_decrypt(get_params))
        params_dict = json.loads(get_params)

        meeting_id = params_dict['meeting_id']
        alt_id = params_dict['altid']
        # meeting_id = bottle.request.query['meetingid']
        # alt_id = bottle.request.query['altid']
        response = cancel_meeting(alt_id,
                                  meeting_id,
                                  config)
        # print response
        if response:
            return "<h2>Meeting cancelled successfully.You can go back to using zippi now :)</h2>"
        else:
            return "<h2>Oops! Meeting could not be cancelled at the moment due to some internal error or " \
                   "because we are unable to reach the Office 365 servers. Please try again later or cancel meeting using outlook.</h2>"
    except Exception:
        logging.log(logging.SEVERITY_DEBUG, "CANCEL-MEETING", "Exception: " + traceback.format_exc())
        return "<h2>Meeting could not be cancelled</h2>"


@bottle.route('/scheduler_helper_apis/get_managers', method="GET")
def scheduler_helper_apis_get_managers():
    org_id = bottle.request.params.get('org_id')
    limit = bottle.request.params.get('limit', "")
    emp_status_ids = bottle.request.params.get('emp_status_ids')
    return scheduler_helper_apis.get_managers(org_id, emp_status_ids, limit)


@bottle.route('/scheduler/dev/null', method="POST")
def dummy_xmppjinie_sendmessage():
    print("Dumping packet . . .")
    postdata = json.loads(bottle.request.body.read().decode('utf-8'))
    print(json.dumps(postdata, indent=4))
    print("Packet dumped . . .")


@bottle.route('/zoho_login')
@bottle.view('zoho_login.html')
def zoho_login(user_id=""):
    """
    Index path function
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    if not user_id:
        user_id = "-1"

    return {"user_id": user_id}


@bottle.route('/zoho/link_account', method="POST")
def zoho_link_account():
    """
    Login Page
    :return:
    """
    try:
        user_id = bottle.request.forms.get("user_id")
        username = bottle.request.forms.get("username")
        password = bottle.request.forms.get("password")
        link_url = str(config.SUBDOMAIN) + "/zoho_login?user_id=" + str(user_id)
        first_name = "User"
        user_details = None
        authTokenData = None
        if user_id and user_id != "-1":
            user_details = mongo_helpers.get_user_details_by_alt_id(user_id, config)
            if user_details is not None:
                print user_details
                first_name = user_details['first_name']
                authTokenData = zoho_meetings.zoho_get_authtoken(username, password)
            if authTokenData and "AUTHTOKEN" in authTokenData:
                zoho_meetings_details = {"domain": authTokenData["domain"], "authToken": authTokenData["AUTHTOKEN"]}
                updateResult = mongo_update_zoho_meeting_token(user_details['alt_id'], zoho_meetings_details, config)
                if updateResult:
                    redirect_template = 'zoho_hello.html'
                    redirect_data = {'name': first_name}
                else:
                    reason = "Problem while linking your account. Please try later"
                    redirect_template = 'zoho_oops.html'
                    redirect_data = {'name': first_name, 'reason': reason, "link_url": link_url}
            else:
                reason = "You have submitted invalid account details."
                redirect_template = 'zoho_oops.html'
                redirect_data = {'name': first_name, "reason": reason, "link_url": link_url}
        else:
            reason = "You have not registered in Zippi. Please register your account in Zippi. "
            redirect_template = 'zoho_oops.html'
            redirect_data = {'name': first_name, 'reason': reason, "link_url": link_url}

    except Exception:
        exception_string = traceback.format_exc()
        print(exception_string)
        redirect_template = 'zoho_oops.html'
        redirect_data = {'name': first_name, "reason": "Problen While linking your account", "link_url": link_url}

    return bottle.template(redirect_template, redirect_data)


@bottle.route('/zoho_oops/<name>')
@bottle.view('zoho_oops.html')
def oops(name=""):
    """
    Error Page
    :param name: Name
    :return:
    """
    return {"name": ", " + name}


@bottle.route('/send_email', method="POST")
def send_email_to_user():
    postdata = {}
    json_response = {"success": False}

    try:

        file_loader = FileSystemLoader(searchpath="./")
        env = Environment(loader=file_loader)
        template = env.get_template('static/templates/welcome-zippi-mailer.html')
        postdata = json.loads(bottle.request.body.read().decode('utf-8'))
        logging.log(logging.SEVERITY_DEBUG, "ASKJINIE", "Updatetimezone Recieved processing a request", postdata)
        recipient_email = postdata.get("recipient_email", "")
        username = postdata.get("username", "")
        domain = postdata.get("domain", "")
        login = postdata.get("login", "")
        password = postdata.get("password", "")
        renderedTemplate = template.render(username=username, domain=domain, loginId=login, password=password)
        message = emails.html(subject='Welcome to Zippi',
                              html=renderedTemplate,
                              mail_from=('Zippi Messenger', 'support@zippi.co'))
        res = message.send(to=recipient_email,
                           smtp={'host': 'email-1.peoplestrong.com',
                                 'port': 587,
                                 'user': 'altml@alt.peoplestrong.com',
                                 'password': 'AltData@456',
                                 'tls': True})

        json_response["success"] = True
        return json_response
    except Exception:
        exception_string = traceback.format_exc()
        print(exception_string)
        logging.log(logging.SEVERITY_CRITICAL, "ASKJINIE", exception_string, postdata)
        json_response["success"] = False
        json_response[
            "data"] = "error occured"
        json_response["status"] = exception_string
        return json_response


@bottle.route('/diy/enable_faqs', method="POST")
def enable_faqs():
    try:
        postdata = json.loads(bottle.request.body.read())
        autherization = bottle.request.headers.get("Authorization", "")
        if (autherization == "Admin@1234"):
            result = diy_index.run_diy(postdata)
            return result
        else:
            return {"status": False, "message": "Authorizarion Failed", "data": {}}
    except Exception:
        traceback.print_exc()
        return {"status": False, "message": "OOPS some problem while processing your request", "data": {}}


@bottle.route('/google/authorize', method="GET")
def gsuit_uthorized():
    logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Started processing an onboarding request")
    result = {"status": "OK"}
    is_authorized = False
    user_name = "User"
    try:
        data = bottle.request.params
        authorised_res = google_meetings.link_user_account(data)
        is_authorized = authorised_res["is_authorized"]
        user_name = authorised_res["user_name"]
        if is_authorized:
            redirect_suffix = '/google/hello/' + user_name
        else:
            redirect_suffix = '/google/oops/' + user_name

    except Exception as e:
        print(e)
        redirect_suffix = '/oops/' + user_name

    return bottle.redirect(config.SUBDOMAIN + redirect_suffix)


@bottle.route('/google/hello/<name>')
@bottle.view('google_hello.html')
def hello(name=""):
    """
    Greeting on onboarding user
    :param name:
    :return:
    """
    return {"name": name}


@bottle.route('/google/oops/<name>')
@bottle.view('google_oops.html')
def oops(name=""):
    """
    Error Page while onboarding user
    :param name: Name
    :return:
    """
    return {"name": ", " + name}


@bottle.route('/google_meet')
@bottle.view('google_meet.html')
def google_index(user_id=""):
    """
    Index path function
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    if not user_id:
        user_id = "-1"

    return {"user_id": user_id}


@bottle.route('/google/login', method="POST")
def google_login():
    """
    Login Page
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    print("Work")
    params = {
        'client_id': config.GOOGLE_MEET_CLIENT_ID,
        'redirect_uri': config.GOOGLE_MEET_REDIRECT_URL,
        'response_type': 'code',
        'scope': config.GOOGLE_MEET_SCOPE,
        'access_type': 'offline',
        'state': user_id,
        'prompt': 'consent'
    }
    print("base_url" + config.GOOGLE_MEET_LINK_URL)
    # signin_url = config.GOOGLE_MEET_LINK_URL.format(urlencode(params))
    signin_url = config.GOOGLE_MEET_LINK_URL + "?" + urlencode(params)
    print("Sign URL" + signin_url)

    return bottle.redirect(signin_url)


@bottle.route('/poc/hyundai/forms')
@bottle.view('form_poc.html')
def index(user_id=""):
    """
    Index path function
    :return:
    """
    user_id = bottle.request.params.get('user_id')
    if not user_id:
        user_id = "-1"

    return {"user_id": user_id}


@bottle.route('/poc_form_onsubmit', method="POST")
@bottle.view('form_poc_ack.html')
def poc_form_onsubmit():
    bus_route = bottle.request.forms.get('inlineFormCustomSelectPref1')
    boarding_point = bottle.request.forms.get('inlineFormCustomSelectPref2')
    user_details = mongo_helpers.get_user_details_by_alt_id(bottle.request.params.get('user_id'), config)
    if user_details:
        ack_message = "Hello {}!, Your boarding details are as follows: \n\nBus Route: {}\n\n" \
                      "Boarding Point: {}".format(
            user_details["full_name"], bus_route, boarding_point
        )
    else:
        ack_message = "Sorry, you are not authorized to use this feature."
    return {"ack_text": ack_message}


@bottle.route('/location_store', method="POST")
def location_store():
    """
                user location update
                at the time of opening app API
                 :return:
        """
    json_response = {"message" : {"message" : "error occured", "code" : "EC201"}}


    try:

        postdata = json.loads(bottle.request.body.read())

        is_location_enabled = postdata.get("isLocEnabled", "")
        if is_location_enabled:
            result = get_user_location(postdata, config)
            if result["status"] is True:
                json_response = {"message" : {"message" : "Success", "code" : "EC200"}}

    except Exception:
        traceback.print_exc()

    return json_response


@bottle.route('/testing/getworklifeappurl', method="POST")
def getworklifeappurl():
    json_response = {"success": False}
    try:
        postdata = json.loads(bottle.request.body.read())
        app_url = postdata.get("app_url")
        portal_type = postdata.get("portalType")
        company_url = postdata.get("companyURL")
        user_name = postdata.get("userName")

        auth_token_api = 'https://hrms.peoplestrong.com/service/jinie/chatUserSSOLogin'
        data = {"portalType": portal_type, "companyURL": company_url, "userName": user_name}
        headers = {"Content-Type": "application/json"}

        token_data = requests.post(auth_token_api, json=data, headers=headers)

        if token_data.json()["messageCodeTO"]["status"] == "Success":
            token = token_data.json()["data"]
            json_response["success"] = True
            json_response[
                "url"] = app_url + "&companyURL=" + company_url + "&portalType=" + portal_type + "&authToken=" + token
    except Exception:
        json_response["success"] = False
        json_response["url"] = ""
        json_response["error"] = traceback.format_exc()

    return json_response


@bottle.route('/zohocrm/authorize', method="GET")
def zoho_authorized():
    logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Started processing an zohocrm request")
    user_name = "User"
    try:
        data = bottle.request.params
        authorised_res = zohocrm_helpers.link_zohocrm_account(data)
        is_authorized = authorised_res["is_authorized"]
        user_name = authorised_res["user_name"]
        if is_authorized:
            redirect_suffix = '/google/hello/' + user_name
        else:
            redirect_suffix = '/google/oops/' + user_name

    except Exception as e:
        redirect_suffix = '/oops/' + user_name
    return bottle.redirect(config.SUBDOMAIN + redirect_suffix)


@bottle.route('/teams/getTasks')
@bottle.view('teams/tasks.html')
def get_tasks():
    """
        Error Page while onboarding user
        :param name: email
        :return:
        """
    task_data = []
    try:
        email = bottle.request.params.get('email')
        user_details = mongo_helpers.get_user_details_from_email(email, config)
        if user_details is not None:
            user_id = user_details["alt_id"]
            tasks_list = teams_tasks.get_user_tasks(str(user_id))
            if tasks_list is not None:
                task_data = tasks_list
    except Exception :
        task_data = []
    return {"task_data": task_data}


@bottle.route('/teams/getGoals')
@bottle.view('teams/goals.html')
def get_goals():
    """
    Error Page while onboarding user
    :param name: email
    :return:
    """
    goals_data = []
    try:
        email = bottle.request.params.get('email')
        user_details = mongo_helpers.get_user_details_from_email(email, config)
        if user_details is not None:
            user_id = user_details["alt_id"]
            token_data = teams_tasks.get_user_token(str(user_id))
            if token_data is not None:
                post_data = {
                    "userId": str(user_id),
                    "domain": token_data["host"]
                }
                headers = {"content-type":"application/json", "Authorization":token_data["token"]}
                goals = requests.post("https://messenger.peoplestrong.com/XmppJinie/myGoals/1.1", data=json.dumps(post_data), headers=headers)
                goals_json = goals.json()
                if goals_json["message"]["code"] =="EC200":
                    goals_list = goals_json["responseData"]["list"]
                    for goal in goals_list:
                        goal_name = goal["goal_name"]
                        if goal_name not in goals_data:
                            goals_data.append(goal_name)
    except Exception :
        goals_data = []
    return {"goals": goals_data, "len":len(goals_data)}


@bottle.route('/teamslogin/authorized')
def teams_authorized():
    """
    Authorization check
    :return:
    """
    logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Started processing an onboarding request")
    signin_base_url = "https://messenger.peoplestrong.com/tasksTab/simple-end"
    try:
        state = bottle.request.params.get('state')
        auth_code = bottle.request.params.get('code')
        post_data = {'grant_type': 'authorization_code',
                        'code': auth_code,
                        'redirect_uri': config.TEAMS_CLIENT_DATA["redirect_url"],
                        'scope': "User.Read",
                        'client_id': config.TEAMS_CLIENT_DATA["client_id"],
                        'client_secret': config.TEAMS_CLIENT_DATA["client_secret"]
                        }
        print "post data : ", json.dumps(post_data, indent=4)
        tokens = requests.post(config.TOKEN_ENDPOINT, data=post_data)
        tokens_json = tokens.json()
        logging.log(logging.SEVERITY_DEBUG, "USER-ONBOARDING", "Dumping Tokens",
                        {"post_data": post_data, "tokens_json": tokens_json})

        get_me_url = config.MS_GRAPH_ENDPOINT.format('/me')
        me_response = make_api_call('GET', get_me_url, tokens_json['access_token'])
        me_response_json = me_response.json()
        if me_response_json.get("mail") is None:
            me_response_json["mail"] = me_response_json.get("userPrincipalName")
        headers={"content-type":"application/json", "Authorization":""}
        ps_oauth_payload ={
                        "portalType": "Employee Portal",
                        "companyURL": "hrms.peoplestrong.com",
                        "userName": "gorla.edukondalu@peoplestrong.co"
                    }
        ps_auth_res = requests.post("https://mobile.peoplestrong.com/service/jinie/chatUserSSOLogin",
                                    data=json.dumps(ps_oauth_payload),headers=headers)

        ps_auth_res_json = ps_auth_res.json()
        params = {
            'access_token': ps_auth_res_json['data'],
            'email': me_response_json["mail"],
            'state':state,
            'domain':"hrms.peoplestrong.com"
        }
        signin_url = signin_base_url + "#" + urlencode(params)
    except Exception as e:
        print(e)
        params = {
            'error': "OOPs unable to Authenticate your login details"
        }
        signin_url = signin_base_url + "#" + urlencode(params)

    return bottle.redirect(signin_url)


if __name__ == '__main__':
    # get_names(19)  # TODO: Only org id 19 as of now!!! Make code changes to accomodate other orgs
    logging.log(logging.SEVERITY_DEBUG, "ALT-JINIE-SERVER-STARTUP", "Starting Bottle Server (Paste)")
    ActiveMQConnectionManager.initialize(config.ACTIVEMQ_CONNECTION)
    # bottle.run(app=bottle.app(), server='paste', host=config.WEBSERVER_HOST, port=config.WEBSERVER_PORT)
    bottle.run(app=bottle.app(), server='paste', host="0.0.0.0", port=config.WEBSERVER_PORT, debug=False)
    ActiveMQConnectionManager.disconnect()
