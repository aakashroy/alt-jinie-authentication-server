import pysolr
import requests
import traceback
import json
import sys
from common import logging, optimizations
from active_mq_conn_mgr import ActiveMQConnectionManager


def get_team_member_details_from_solr(email, org_id):
    # solrdb = pysolr.Solr('http://solr.peoplestrong.com:8983/solr/alt-search/')
    solrdb = pysolr.Solr('http://10.226.0.242:8983/solr/alt-search')
    result = solrdb.search(q="official_email:\"{email}\" AND org_id:{org_id}".
                           format(email=email, org_id=org_id), fl="id",
                           rows=1)
    result = [x for x in result]
    return result


def send_to_queue(request_email, request_user_id, level, days, qilo_org_id, org_id, qilo_header):
    message = dict()

    message['qilo_header'] = qilo_header
    message['params'] = {
        'request_email': request_email,
        'request_user_id': request_user_id,
        'level': level,
        'days': days,
        'qilo_org_id': qilo_org_id,
        'org_id': org_id
    }

    message['message_type'] = 20
    ActiveMQConnectionManager.send(destination="/queue/user_scheduling_queue",
                                   body=optimizations.json_dumps_minify(message),
                                   headers={'persistent': 'true'})


def qilo_team_reminders(postdata, header):
    response = {"status": True,
                "message": "Some problem occurred while sending reminder."
                }
    try:
        logging.log(logging.SEVERITY_DEBUG, "qilo_team_reminders", "Received Payload",
                    postdata)
        org_id = int(postdata.get("org_id", 0))
        qilo_org_id = int(postdata.get("qilo_org_id", 0))
        user_id = int(postdata.get("user_id", 0))
        email = postdata.get("email_id", "")
        level_input = postdata.get("level", "L1")
        days = int(postdata.get("days", 15))
        if level_input=="L1":
            level = 1
        elif level_input=="L2":
            level =2
        elif level_input =="All":
            level=3
        else:
            response["message"] = "I received an invalid hierarchy level. Please provide a valid hierarchy level."
            return response
        qilo_header = header
        team_list = get_qilo_team_okr(qilo_org_id, user_id, email,qilo_header)
        if len(team_list) > 0:
            send_to_queue(email, user_id, level, days, qilo_org_id, org_id, qilo_header)
            response["message"] = "I have accepted your check-in reminder request. " \
                                      "I will let you know once I have reminded the respective team members to check-in."
        else:
            response["message"] = "Sorry, you have no reportees under you."
    except Exception:
        exception_string = traceback.format_exc()
        print str(exception_string)
    return response


def get_qilo_team_okr(qilo_org_id, user_id, email,header):
    result = []
    try:
        api_url = "https://performanceapi.peoplestrong.com/api/v1/rest/reporteeList"
        headers = header
        data = {"emp_code": str(user_id), "org_id": qilo_org_id, "emp_email": email}
        res = requests.post(api_url, json=data, headers=headers)
        json_data = json.loads(res.text)
        if isinstance(json_data, list):
            for user in json_data:
                try:
                    user_checkin_days = int(user["checkin_days_ago"])
                except ValueError:
                    user_checkin_days = sys.maxint
                user_data = {"last_checkin": user_checkin_days,
                             "email": user["email"]}
                result.append(user_data)
        return result
    except Exception:
        exception_string = traceback.format_exc()
        print str(exception_string)
        result = []
    return result


if __name__ == '__main__':
    # get_qilo_team_okr(19,11)
    headers = {
        "api-access-token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI4MSIsImlzcyI6Imh0dHBzOi8vd3d3LnFpbG90ZWNoLmNvbSJ9.lf__uPT3itUQ-7ohBrM-fOi5Xpi5l8GX1JGIZYc-kqo",
        "api-key": "81HI28BB4D86PPK3QK3H2",
        "api-org": "81",
        "Content-Type": "application/json"}
    postdata = {"org_id":"19",
                "qilo_org_id":"81",
                "user_id":"2060312",
                "email_id":"harsimran.walia@zippi.co",
                "level":"L2",
                "days":"10",
                "domain":"hrms.peoplestrong.com"
                }
    print(type(postdata))
    qilo_team_reminders(postdata, headers)
    #qilo_team_remainders(19, 81, 1749823, "mukesh.antil@peoplestrong.com", 2, 10)
