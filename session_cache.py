from expiringdict import ExpiringDict


class SessionCache:
    __expiring_cache = None

    @staticmethod
    def get():
        if SessionCache.__expiring_cache == None:
            SessionCache.__expiring_cache = ExpiringDict(max_len=10000, max_age_seconds=300)
        return SessionCache.__expiring_cache


class ConversationalFlowCache:
    __expiring_cache = None

    @staticmethod
    def get():
        if ConversationalFlowCache.__expiring_cache == None:
            ConversationalFlowCache.__expiring_cache = ExpiringDict(max_len=10000, max_age_seconds=300)
        return ConversationalFlowCache.__expiring_cache
