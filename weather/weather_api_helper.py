# -*- coding: utf-8 -*-
import json
import pytz
from datetime import datetime


import requests
import traceback


def aq_report_by_city(city_name, isWebHookResponse=False):


    json_response = {
        "messageCodeTO": {
            "status": "SUCCESS",
            "code": "S100",
            "displayMsg": "Sorry, Air Quality Data was not available for that city."
        }
    }

    table_webhook_response = {
        "custom_design": {
            "design": {
                "selectionOptions": [],
                "label": "Here's the Air Quality Data report:",
                "type": "Table",
                "selectedValue": [],
                "displaySeq": "column_0,column_1",
                "dataArray": [
                    {
                        "column_0": "PARAMETER",
                        "column_1": "VALUE (µg/m³)"
                    },
                    {
                        "column_0": "PM2.5",
                        "column_1": "##PM2.5##"
                    },
                    {
                        "column_0": "PM10",
                        "column_1": "##PM10##"
                    },
                    {
                        "column_0": "CO",
                        "column_1": "##CO##"
                    },
                    {
                        "column_0": "SO2",
                        "column_1": "##SO2##"
                    },
                    {
                        "column_0": "NO2",
                        "column_1": "##NO2##"
                    },
                    {
                        "column_0": "O3",
                        "column_1": "##O3##"
                    }
                ]}}}
    try:
        now = datetime.now(pytz.timezone("UTC"))
        print(now)
        date1 = now.strftime("%Y-%m-%d")
        api_url = "https://api.openaq.org/v1/latest?city=" + city_name
        aq_api_res = requests.get(api_url)
        print(aq_api_res.json())
        aqi_res_result = aq_api_res.json()["results"]
        status_code = aq_api_res.status_code
        print(aqi_res_result)
        o3 = 0
        pm25 = 0
        pm10 = 0
        no2 = 0
        so2 = 0
        co = 0
        o3count=0
        pm25Count=0
        pm10Count=0
        no2Count = 0
        so2Count = 0
        coCount = 0
        if status_code == 200:
            for i in range(0, len(aqi_res_result)):
                measurements = aqi_res_result[i]["measurements"]

                for j in range(0, len(measurements)):
                    last_update = measurements[j]["lastUpdated"]
                    first_chars = last_update[0:10]
                    first_chars = first_chars.encode('ascii', 'ignore')



                    if first_chars == date1:
                        param = measurements[j]["parameter"]
                        value= measurements[j]["value"]
                        if param == "o3":
                            o3count=o3count+1
                            o3 =o3+value
                        elif param == "pm25":
                            pm25Count=pm25Count+1
                            pm25 = pm25+value

                        elif param == "pm10":
                            pm10Count=pm10Count+1
                            pm10 = pm10+value
                        elif param == "no2":
                            no2Count=no2Count+1
                            no2 = no2+value
                        elif param == "so2":
                            so2Count=so2Count+1
                            so2 = so2+value
                        elif param == "co":
                            coCount=coCount+1
                            co = co+value
            if o3count>0:
               o3=o3/o3count
               o3=float("{0:.2f}".format(o3))
               o3Resp= str(o3)+" ("+str(0)+"-"+str(100)+")"
            if pm25Count>0:
               pm25=pm25/pm25Count
               pm25 = float("{0:.2f}".format(pm25))
               pm25Resp= str(pm25)  + " (" + str(0) + "-" + str(60) + ")"
            if pm10Count>0:
               pm10=pm10/pm10Count
               pm10 = float("{0:.2f}".format(pm10))
               pm10Resp = str(pm10)  + " (" + str(0) + "-" + str(100) + ")"
            if no2Count > 0:
                no2 = no2/no2Count
                no2 = float("{0:.2f}".format(no2))
                no2Resp = str(no2) + " (" + str(0) + "-" + str(80) + ")"
            if so2Count > 0:
                so2 = so2 / so2Count
                so2 = float("{0:.2f}".format(so2))
                so2Resp = str(so2) + " (" + str(0) + "-" + str(80) + ")"
            if coCount > 0:
                co = co/coCount
                co = float("{0:.2f}".format(co))
                coResp = str(co) + " (" + str(0) + "-" + str(2000) + ")"


            displayMsg = json.dumps(table_webhook_response)
            displayMsg = displayMsg.replace("##O3##", o3Resp)
            displayMsg = displayMsg.replace("##PM2.5##", str(pm25Resp))
            displayMsg = displayMsg.replace("##PM10##", str(pm10Resp))
            displayMsg = displayMsg.replace("##SO2##", str(so2Resp))
            displayMsg = displayMsg.replace("##NO2##", str(no2Resp))
            displayMsg = displayMsg.replace("##CO##", str(coResp))


            if isWebHookResponse:
               return displayMsg
            else:
                displayMsg = "Here's the AQ Data report for " + str(city_name)  + "PM2.5=" + str(
                    pm25Resp) + "PM10=" + str(pm10Resp) +  "O3= "+str(o3Resp)
                json_response["messageCodeTO"]["displayMsg"] = displayMsg
                return json_response

        elif status_code == 404:
             displayMsg = "Sorry, AQ Data report was not available for " + city_name + ", you can try asking for a different location."
             if isWebHookResponse:
                return displayMsg
             else:
                 json_response["messageCodeTO"]["displayMsg"] = displayMsg
                 return json_response
        else:
             displayMsg = "Sorry, an error occurred while searching the results. You can ask me again about the AQ Data after a while."
             if isWebHookResponse:
                 return displayMsg
             else:
                  json_response["messageCodeTO"]["displayMsg"] = displayMsg
                  return json_response
    except Exception:
              exception_string = traceback.format_exc()
              print(exception_string)
              displayMsg = json_response["messageCodeTO"]["displayMsg"]
              if isWebHookResponse:
                 return displayMsg
              else:
                 return json_response



def weather_report_by_city(city_name, isWebHookResponse=False):
    """
            get Weather Report
            :return:
    """
    json_response = {
        "messageCodeTO": {
            "status": "SUCCESS",
            "code": "S100",
            "displayMsg": "Sorry, weather report was not available for that city."
        }
    }
    table_webhook_response = {
        "custom_design": {
            "design": {
                "selectionOptions": [],
                "label": "Here's the weather report:",
                "type": "Table",
                "selectedValue": [],
                "displaySeq": "column_0,column_1",
                "dataArray": [
                    {
                        "column_0": "City",
                        "column_1": "##CITYNAME##"
                    },
                    {
                        "column_0": "Condition",
                        "column_1": "##CONDITION##"
                    },
                    {
                        "column_0": "Current Temp",
                        "column_1": "##NOW_TEMP##°С"
                    },
                    {
                        "column_0": "Min Temp",
                        "column_1": "##MIN_TEMP##°С"
                    },
                    {
                        "column_0": "Max Temp",
                        "column_1": "##MAX_TEMP##°С"
                    },
                    {
                        "column_0": "Feels Like",
                        "column_1": "##FEELS_LIKE##°С"
                    },
                    {
                        "column_0": "Humidity",
                        "column_1": "##HUMIDITY##%"
                    },
                    {
                        "column_0": "Visibility",
                        "column_1": "##VISIBILITY## meters"
                    },
                    {
                        "column_0": "Cloudines",
                        "column_1": "##COUDINESS##%"
                    },
                    {
                        "column_0": "Wind Speed",
                        "column_1": "##WIND## miles/hour"
                    }
                ]}}}
    try:
        api_url = "https://api.openweathermap.org/data/2.5/weather?q=" + city_name + "&appid=13c4e479424273865cd460064fec290c"
        weather_api_res = requests.get(api_url)
        weather_api_json = weather_api_res.json()
        status_code = weather_api_res.status_code
        print(weather_api_json)
        if status_code == 200:
            w_desc = weather_api_json["weather"][0]["main"]
            w_feels_like_kelvin = weather_api_json["main"]["feels_like"]
            w_temp_kelvin = weather_api_json["main"]["temp"]
            w_temp_min_kelvin = weather_api_json["main"]["temp_min"]

            w_feels_like_celsius = round(float(w_feels_like_kelvin) - 273.15)
            w_temp_min_celsius = round(float(w_temp_min_kelvin) - 273.15)

            w_temp_max_kelvin = weather_api_json["main"]["temp_max"]
            w_temp_max_celsius = round(float(w_temp_max_kelvin) - 273.15)

            w_temp_celsius = round(float(w_temp_kelvin) - 273.15)

            w_humidity = weather_api_json["main"]["humidity"]
            w_city = weather_api_json["name"]
            wind = weather_api_json["wind"]["speed"]
            w_visibility = weather_api_json["visibility"]
            w_cloudiness = weather_api_json["clouds"]["all"]
            displayMsg = json.dumps(table_webhook_response)
            displayMsg = displayMsg.replace("##FEELS_LIKE##", str(w_feels_like_celsius))
            displayMsg = displayMsg.replace("##COUDINESS##", str(w_cloudiness))
            displayMsg = displayMsg.replace("##VISIBILITY##", str(w_visibility))

            displayMsg = displayMsg.replace("##CONDITION##", str(w_desc))
            displayMsg = displayMsg.replace("##CITYNAME##", str(w_city))
            displayMsg = displayMsg.replace("##NOW_TEMP##", str(w_temp_celsius))
            displayMsg = displayMsg.replace("##MIN_TEMP##", str(w_temp_min_celsius))
            displayMsg = displayMsg.replace("##MAX_TEMP##", str(w_temp_max_celsius))
            displayMsg = displayMsg.replace("##WIND##", str(wind))
            displayMsg = displayMsg.replace("##HUMIDITY##", str(w_humidity))
            print(isWebHookResponse)
            if isWebHookResponse:
                return displayMsg
            else:
                displayMsg = "Here's the weather report for " + str(w_city) + ": <b>" + str(
                    w_temp_celsius) + "°С</b>. Temperature from " + str(w_temp_min_celsius) + " to " + str(
                    w_temp_max_celsius) + " °С, wind " + str(wind) + " m/s. humidity " + str(w_humidity) + "%"
                json_response["messageCodeTO"]["displayMsg"] = displayMsg
                return json_response
        elif status_code == 404:
            displayMsg = "Sorry, weather report was not available for " + city_name + ", you can try asking for a different location."
            if isWebHookResponse:
                return displayMsg
            else:
                json_response["messageCodeTO"]["displayMsg"] = displayMsg
                return json_response
        else:
            displayMsg = "Sorry, an error occurred while searching the results. You can ask me again about the weather after a while."
            if isWebHookResponse:
                return displayMsg
            else:
                json_response["messageCodeTO"]["displayMsg"] = displayMsg
                return json_response
    except Exception:
        exception_string = traceback.format_exc()
        print(exception_string)
        displayMsg = json_response["messageCodeTO"]["displayMsg"]
        if isWebHookResponse:
            return displayMsg
        else:
            return json_response
