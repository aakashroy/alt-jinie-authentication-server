"""Read users from a csv to send Jinie Onboarding notifications"""

import csv
import sys
import traceback

from pymongo import MongoClient

ALLOWED_SERVICES = {
    "enabled": True,
    "motd": {
        "enabled": True,
        "schedule": "0 8 * * *"
    },
    "event_notifications": {
        "enabled": True
    },
    "leaves": {
        "enabled": True,
        "schedule": "30 10 * * *"
    },
    "regularization": {
        "enabled": True,
        "schedule": "0 9 * * 2"
    },
    "timesheet": {
        "enabled": True,
        "schedule": "0 10 10-16 * 2"
    }
}

if __name__ == '__main__':
    try:
        CLIENT = MongoClient("mongodb://appuser:password@mongo1.peoplestrong.com:27017")
        DB = CLIENT['altjinie']
        COLLECTION = DB['users']
        with open('add_users.csv') as f:
            READER = csv.DictReader(f)
            ARG = sys.argv
            if len(sys.argv) == 2:
                if ARG[1] == '-i':
                    for row in READER:
                        empid = int(row['empid'].strip())
                        altid = int(row['altid'].strip())
                        emailid = row['emailid'].strip()
                        result = COLLECTION.find_one({'emp_id': empid})
                        if result is not None:
                            COLLECTION.update_one({'emp_id': empid}, {'$set': {'email': emailid, 'alt_id': altid}})
                        elif result is None:
                            emp_details = COLLECTION.insert_one(
                                {
                                    'alt_id': altid,
                                    "allowed_services": ALLOWED_SERVICES,
                                    'email': emailid,
                                    'emp_id': empid
                                }
                            )
                        else:
                            raise Exception('Given Wrong Argument should be -i or blank')
            elif len(sys.argv) == 1:
                for row in READER:
                    empid = int(row['empid'].strip())
                    altid = int(row['altid'].strip())
                    emailid = row['emailid'].strip()
                    result = COLLECTION.find_one({'emp_id': empid})
                    if result is not None:
                        pass
                    elif result is None:
                        emp_details = COLLECTION.insert_one(
                            {
                                'alt_id': altid,
                                "allowed_services": ALLOWED_SERVICES,
                                'email': emailid,
                                'emp_id': empid
                            }
                        )
            else:
                raise Exception('More than 1 argument passed')
    except Exception as e:
        print "Traceback: ", traceback.format_exc()
