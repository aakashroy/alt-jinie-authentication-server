"""
Module to fix MongoDB documents as per changes in the development life cycle
"""
import re
import string

from pymongo import MongoClient

import config


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def clean_string(inputstr):
    outputstr = inputstr.lower()
    outputstr = translate_punctuation_to_empty(outputstr)
    outputstr = re.sub(r'\s+', ' ', outputstr)
    return outputstr.strip()


def add_derived_fields_mongo():
    """
    Function to fix mongodb documents
    :return: None
    """
    client = MongoClient(config.MONGO_CONNECTION)
    database = client['altjinie']
    collection = database['users']

    for user in collection.find():
        new_fields = dict()
        new_fields["search_email"] = user["email"].lower()
        new_fields["search_f_name"] = clean_string(user["first_name"])
        new_fields["search_fm_name"] = clean_string(user["first_name"] + user["middle_name"])
        new_fields["search_fl_name"] = clean_string(user["first_name"] + user["last_name"])
        new_fields["search_fml_name"] = clean_string(user["first_name"] + user["middle_name"] + user["last_name"])

        update_record = {
            "$set": new_fields
        }

        print collection.update_one({"alt_id": user["alt_id"]}, update_record)


if __name__ == "__main__":
    add_derived_fields_mongo()
