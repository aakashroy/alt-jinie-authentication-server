"""Send notification for signup"""
import json

import requests
from jiniepush import jinie_notifications
from pymongo import MongoClient

from common import send_notfn_to_jinie

MAX_USERS_AT_ONCE = 100


def get_userlist_payload(alt_id, message):
    return {
        "surveyID": None,
        "userId": alt_id,
        "tenantId": None,
        "organizationId": None,
        "action": "",
        "messageType": None,
        "text": [
            {
                "design": [
                    {
                        "label": message,
                        "type": "TEXT",
                        "subType": "",
                        "displayDataType": "Info",
                        "selectionType": "none",
                        "questionID": None,
                        "tableTitle": None,
                        "tableSubTitle": None,
                        "sequence": None,
                        "dataArray": None,
                        "genericArray": None,
                        "displaySeq": None,
                        "cellTrackerID": None,
                        "action": "",
                        "messageId": None,
                        "selectionOptions": None,
                        "selectedValue": [],
                        "flow": None
                    }
                ]
            }
        ]
    }


def get_notification_payload(alt_ids, message):
    users_list = []
    for alt_id in alt_ids:
        users_list.append(get_userlist_payload(alt_id, message))

    return {
        "input": {
            "messageType": "NOTIFICATION",
            "action": None,
            "usersList": users_list
        }
    }


def send_many(alt_ids, message):
    for i in range(0, len(alt_ids), MAX_USERS_AT_ONCE):
        alt_ids_sliced = alt_ids[i: i + MAX_USERS_AT_ONCE]
        notification_payload = get_notification_payload(alt_ids_sliced, message)
        response = requests.post(url="https://altmessenger.peoplestrong.com/AltChatServer/jinieNotificationRequest/1.0",
                                 headers={"Content-Type": "application/json"},
                                 data=json.dumps(notification_payload, separators=(',', ':')),
                                 timeout=30)

        print "Received response: " + json.dumps(response, indent=4, default=str)

        if response.status_code == 200:
            print "Sent to " + str(len(alt_ids_sliced)) + " users . . ."
        else:
            print "CRITICAL ERROR: "


def send_all():
    """
    Function to send notification to all
    :return:
    """
    client = MongoClient("mongodb://appuser:password@mongo1.peoplestrong.com:27017")
    database = client['altjinie']
    collection = database['users']

    messages = [
        "Hi ##_USER_FIRST_NAME_##!<br/>"
        "Now you can make me your <b>personal productivity assistant</b> :) <br/><br/>"
        "In case you have been unable to click on the link below, I request you to update the Alt Messenger App and try again.<br/><br/>"
        "<a href='https://daas.peoplestrong.com'>Click Here once you're done!</a><br/>"]

    for user in collection.find():
        if 'updated_on' not in user:
            for message in messages:
                send_notfn_to_jinie.send(user['alt_id'], message)
            print "Sent to: " + user['email']
        else:
            print "Not sent to: " + user['email']


def send_one(alt_id, force):
    """
    Function to send notification to a user
    :param alt_id: AltId
    :param force: Force
    :return:
    """
    client = MongoClient("mongodb://appuser:password@mongo1.peoplestrong.com:27017")
    database = client['altjinie']
    collection = database['users']

    messages = [
        "Hi ##_USER_FIRST_NAME_##!<br/>"
        "Now you can make me your <b>personal productivity assistant</b> :) <br/><br/>"
        "In case you have been unable to click on the link below, I request you to update the Alt Messenger App and try again.<br/><br/>"
        "<a href='https://daas.peoplestrong.com'>Click Here once you're done!</a><br/>"]

    for user in collection.find({'alt_id': alt_id}):
        if 'updated_on' not in user or force:
            for message in messages:
                jinie_notifications.send_simple(alt_id=str(alt_id), message=message, format_message=False, verbose=True)
            print "Sent to: " + user['email']
        else:
            print "Not sent to: " + user['email']


def send_test(alt_id):
    jinie_notifications.send_simple(str(alt_id),
                                    message="Hi Harsimran <br/> you have <b>8</b> Regularization Pending from <b>4</b> Employees",
                                    format_message=False, verbose=True)

if __name__ == "__main__":
    send_one(321798, True)
    # send_all()
    # send_one(321798, True)
    # send_one(2878966, True)
    # send_one(1077498, True)
