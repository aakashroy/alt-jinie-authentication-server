"""
Module to fix MongoDB documents as per changes in the development life cycle
"""
from pymongo import MongoClient

import config
from common.sql_helpers import connect_to_mssql_server


def fix_mongodb_documents_2():
    """
    Function to dix mongodb documents - second variation
    :return:
    """
    client = MongoClient(config.MONGO_CONNECTION)
    database = client['altjinie']
    collection = database['users']
    for user in collection.find({"org_id": 19}):
        user["allowed_services"].update(
            {
                "regularization": {
                    "enabled": True,
                    "schedule": "0 9 * * 2"
                },
                "motd": {
                    "enabled": True,
                    "schedule": "0 8 * * *"
                },
                "leaves": {
                    "enabled": True,
                    "schedule": "30 10 * * *"
                },
                "enabled": True,
                "timesheet": {
                    "enabled": True,
                    "schedule": "0 10 15-21 * 2"
                },
                "event_notifications": {
                    "enabled": True
                },
                "trending_courses": {
                    "enabled": True,
                    "schedule": "45 8 * * 2"
                }
            }
        )
        update_record = {
            "$set": {
                "allowed_services": user["allowed_services"]
            }
        }

        print collection.update_one({"alt_id": user["alt_id"]}, update_record)


def fix_mongodb_documents():
    """
    Function to fix mongodb documents
    :return: None
    """
    client = MongoClient(config.MONGO_CONNECTION)
    database = client['altjinie']
    collection = database['users']

    sql_con = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
    sql_con.execute("select su.UserId, FirstName, MiddleName, LastName from dbo.HrEmployee as hre \
                    inner join dbo.SysUser as su on su.UserID=hre.UserID \
                    where hre.OrganizationId=19 and hre.EmploymentStatusID in (210,214) \
                    and hre.OfficialEmailAddress is not NULL")
    db_records = sql_con.fetchall()
    db_records_map = {w: (x, y, z) for (w, x, y, z) in db_records}

    mismatch = []

    for user in collection.find():
        db_record = db_records_map[user["alt_id"]]
        if db_record:
            update_record = {
                "$set": {
                    "first_name": db_record[0].strip() if db_record[0] else "",
                    "middle_name": db_record[1].strip() if db_record[1] else "",
                    "last_name": db_record[2].strip() if db_record[2] else "",
                }
            }

            print collection.update_one({"alt_id": user["alt_id"]}, update_record)
        else:
            print "Not Updated ? ? ?"
            exit(1)

    print "Length: " + str(len(mismatch))
    print mismatch


if __name__ == "__main__":
    fix_mongodb_documents_2()
