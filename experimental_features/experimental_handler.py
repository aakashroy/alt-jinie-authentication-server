from experimental_features.helpjuice import search_helpjuice
import traceback
import pymongo


def handle_experimental_features(intent, alt_user_id, additional_parameters):
    if intent == "ERP_Oracle_Fusion":
        return search_helpjuice(alt_user_id, additional_parameters)
    return None


def wish_design_msg(org_id, conf):
    result = dict()
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": org_id})
        print result
        result = [x.get("alt_id", -1) for x in result]
    except Exception:
        traceback.print_exc()

    return result


if __name__ == "__main__":
    import json

    print(json.dumps(handle_experimental_features("ERP_Oracle_Fusion", 404, {
        "query": "Who is Jinie?"
    }), indent=4))
