import base64

import requests
import pdfkit

config = pdfkit.configuration(wkhtmltopdf='/usr/local/bin/wkhtmltopdf')


def search_helpjuice(alt_user_id, additional_parameters):
    print("EXP::HelpJuice Called")
    sr = requests.get("https://yumcha.helpjuice.com/search?query={}".format(additional_parameters["query"])).json()
    ans = sr[0]
    desc = ans["question"]["long_answer_sample"] + " . . ."
    ansurl = "https://yumcha.helpjuice.com{}".format(ans["question"]["url"])
    pdfraw = pdfkit.from_url(ansurl, False)
    # pdfkit.from_url(ansurl, "test.pdf")
    print("EXP::HelpJuice PDF Conversion Complete")
    file_byte_array = base64.b64encode(pdfraw)
    display_filename = "{}.pdf".format(ans["question"]["slug"])
    print("EXP::HelpJuice Sending {} bytes . . .".format(len(file_byte_array)))
    return {

        "messageCode": {
            "code": "EC200",
            "message": "NA",
            "description": "NA",
            "validationKeys": None,
            "forcedAppUpdate": False
        },
        "responseData": [
            {
                "message": desc,
                "fileByteArray": file_byte_array,
                "fileName": display_filename
            }
        ],
        "response": None,
        "appUpdateResponseTO": None,
        "totalRecords": 0,
        "responseMap": None
    }
