""" Project Configuration """
MONGO_CONNECTION = "mongodb://localhost:27017"

MONGO_DB_ALT_JINIE = "altjinie"

MONGO_COLL_USERS = "users"

CLIENT_ID = 'f2360d90-fb83-42a3-be72-45ef742de58d'
CLIENT_SECRET = 'Hcash3fw4sV:[9@Mhw:F65/JMf[UlXwg'

SUBDOMAIN = "https://4f8c0161.ngrok.io"
# SUBDOMAIN = "https://3810c094.ngrok.io"

REDIRECT_URI = SUBDOMAIN + '/login/authorized'

AUTHORITY_URL = 'https://login.microsoftonline.com'
AUTH_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/authorize?{0}')
TOKEN_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/token')
MS_GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0{0}'

SCOPES = [
    'openid',
    'offline_access',  # This is the most imp, for getting refresh token!
    'Calendars.Read',
    'Calendars.Read.Shared',
    'Calendars.ReadWrite.Shared',
    'Calendars.ReadWrite',
    'Contacts.Read',
    'Contacts.Read.Shared',
    'Mail.Read',
    'Mail.Read.Shared',
    'People.Read',
    'User.Read',
    'Mail.Send'
]  # Add other scopes/permissions as needed.

MYSQL_CONNECTION_EJABBERD = {
    "host": "192.168.2.114",
    "user": "admin",
    "passwd": "admin@123",
    "database": "ejabberd"}

ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE = "/queue/user_registration_inlimbo_queue"
ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE = "/queue/notification_sending_queue"
ACTIVE_MQ_INCOMING_EMAIL_QUEUE = "/queue/incoming_email_queue"

ACCESS_TOKEN_TIMEOUT_THRESHOLD = 10 * 60  # In seconds

ACTIVEMQ_CONNECTION = [('127.0.0.1', 61613)]

WEBSERVER_HOST = "localhost"

WEBSERVER_PORT = 7075

FAKE_JINIE_API_FOR_INTERNAL_TESTING = False

MYSQL_CREDENTIALS = {"IP": "10.0.2.17", "PORT": '3306', "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355"}

MSSQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"}

TALENTPACT_DB = "TalentPact"
QTRAIN_DB = "qtrain"

DEFAULT_COURSE_RECOMMENDER_SOURCE_OVERRIDE = "MOOC"

RASA_NLU_URL = "https://sohum-services.peoplestrong.com/api/nlu_model/"