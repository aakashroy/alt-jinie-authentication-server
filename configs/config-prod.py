""" Project Configuration """
MONGO_CONNECTION = "mongodb://appuser:password@mongo1.peoplestrong.com:27017"

MONGO_DB_ALT_JINIE = "altjinie"

MONGO_COLL_USERS = "users"

CLIENT_ID = '2861a4b4-6853-4318-ac15-2a623d10a66b'
CLIENT_SECRET = 'lbWEOHJR12=mkctxX102^(+'

SUBDOMAIN = "https://daas.peoplestrong.com"
# SUBDOMAIN = "https://3810c094.ngrok.io"

REDIRECT_URI = SUBDOMAIN + '/login/authorized'

AUTHORITY_URL = 'https://login.microsoftonline.com'
AUTH_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/authorize?{0}')
TOKEN_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/token')
MS_GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0{0}'

SCOPES = [
    'openid',
    'offline_access',  # This is the most imp, for getting refresh token!
    'Calendars.Read',
    'Calendars.Read.Shared',
    'Calendars.ReadWrite.Shared',
    'Calendars.ReadWrite',
    'Contacts.Read',
    'Contacts.Read.Shared',
    'Mail.Read',
    'Mail.Read.Shared',
    'People.Read',
    'User.Read',
    'Mail.Send'
]  # Add other scopes/permissions as needed.

MYSQL_CONNECTION_EJABBERD = {
    "host": "192.168.2.114",
    "user": "admin",
    "passwd": "admin@123",
    "database": "ejabberd"}

ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE = "/queue/user_registration_inlimbo_queue"
ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE = "/queue/notification_sending_queue"
ACTIVE_MQ_INCOMING_EMAIL_QUEUE = "/queue/incoming_email_queue"

ACCESS_TOKEN_TIMEOUT_THRESHOLD = 10 * 60  # In seconds

ACTIVEMQ_CONNECTION = [('10.226.0.76', 61613)]
# ACTIVEMQ_CONNECTION = [('127.0.0.1', 61613)]

WEBSERVER_HOST = "10.226.0.83"
# WEBSERVER_HOST = "localhost"

WEBSERVER_PORT = 7070

FAKE_JINIE_API_FOR_INTERNAL_TESTING = False

MYSQL_CREDENTIALS = {"IP": "13.71.27.119", "PORT": "3306", "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355"}

# MSSQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"}
# MSSQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "AltMApp", "PASSWORD": "H0n35TL1F31SGr@t3"}
MSSQL_CREDENTIALS = {"IP": "10.226.0.187", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"}

TALENTPACT_DB = "TalentPact"
QTRAIN_DB = "qtrain"

DEFAULT_COURSE_RECOMMENDER_SOURCE_OVERRIDE = "MOOC"

RASA_NLU_URL = "https://sohum-services.peoplestrong.com/api/nlu_model/"