import re

import config
from common import sql_helpers


def get_managers(org_id, emp_status_ids, limit):
    result = []
    emp_status_ids = re.sub(r"\s+", "", emp_status_ids)
    emp_status_ids = emp_status_ids.split(",")
    crsr = sql_helpers.connect_to_mssql_server("TalentPact", config.MSSQL_CREDENTIALS)
    if crsr is not None:
        query = "select hre.L1ManagerID from dbo.HrEmployee as hre \
        inner join dbo.SysUser as su on su.UserID=hre.UserID \
        where hre.OrganizationId=%s and hre.EmploymentStatusID in %s and L1ManagerID is not NULL"
        crsr.execute(query, (org_id, emp_status_ids))
        result = crsr.fetchall()
        result = list(set([x[0] for x in result]))
        query = "select hre.UserId from dbo.HrEmployee as hre \
        inner join dbo.SysUser as su on su.UserID=hre.UserID \
        where hre.OrganizationId=%s and hre.EmploymentStatusID in %s and hre.EmployeeID in %s"
        crsr.execute(query, (org_id, emp_status_ids, result))
        result = crsr.fetchall()
        result = list(set([x[0] for x in result]))
        if limit:
            result = result[0: int(limit)]
        result = [{"user_id": x} for x in result]
    return {"user_ids": result}


if __name__ == "__main__":
    get_managers(19, "210, 214")
