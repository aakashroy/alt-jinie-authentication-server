import traceback


def get_employees_from_name(org_id, name, solr_conn, match_method="exact"):
    results = None
    match_method = "~" if match_method == "fuzzy" else ""
    if org_id > 0 and name:
        try:
            name = name.strip()
            q = "(auto_name:\"{name}\"{match_method} OR name:\"{name}\"{match_method}) AND org_id:{org_id} AND chat_entity_type:user AND active:true".format(
                name=name, match_method=match_method, org_id=org_id
            )
            print(q)
            results = solr_conn.search(
                q=q, rows=50)
        except:
            traceback.print_exc()

    return results
