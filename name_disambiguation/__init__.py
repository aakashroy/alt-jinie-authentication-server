import pysolr
from py2neo import Graph

from name_disambiguation.neo4j_helpers import get_best_match_from_neo4j
from name_disambiguation.solr_helpers import get_employees_from_name

n4jdb = Graph(host="neo4j.peoplestrong.com", password="neo4j@123")
solrdb = pysolr.Solr('http://10.226.0.242:8983/solr/alt-search/')


class NameDisambiguation(object):
    def __init__(self, name, user_id, org_id, use_neo4j=True, ordering=None):
        self.name = name
        self.user_id = user_id
        self.org_id = org_id
        self.ordering = ordering
        if self.ordering is None:
            self.ordering = [('e', 'l1_mgr_id'), ('e', 'l2_mgr_id'), ('e', 'hr_mgr_id'), ('r', 'L1_MANAGES'),
                             ('r', 'L2_MANAGES'), ('r', 'HR_MANAGES')]
        self.use_neo4j = use_neo4j

    def resolve(self):
        resolved = None
        match_type = None
        match_rule = None
        if self.name:
            for match_method in ["exact", "fuzzy"]:
                employees = get_employees_from_name(self.org_id, self.name, solr_conn=solrdb, match_method=match_method)
                if employees and employees.docs:
                    employees = employees.docs
                    print("Emps found in solr: {}".format(len(employees)))
                    if len(employees) == 1:
                        resolved = employees[0]
                        match_type = "d"
                        match_rule = "SINGLE_MATCH"
                    elif self.use_neo4j:
                        emp_ids = [x["employee_id"] for x in employees]
                        resolved_emp, match_type, match_rule = get_best_match_from_neo4j(self.user_id,
                                                                                         self.org_id,
                                                                                         emp_ids,
                                                                                         self.ordering,
                                                                                         n4jdb)
                        if resolved_emp:
                            resolved = [x for x in employees if x["id"] == str(resolved_emp["user_id"])][0]
                if resolved:
                    break
        return resolved, match_type, match_rule


if __name__ == "__main__":
    print NameDisambiguation("Vishal", 2205395, 19).resolve(n4jdb, solrdb)
    print NameDisambiguation("Harsimran", 2205395, 19).resolve(n4jdb, solrdb)
    print NameDisambiguation("Vivek", 2205395, 19).resolve(n4jdb, solrdb)
    print NameDisambiguation("Aakash", 2205395, 19).resolve(n4jdb, solrdb)
    print NameDisambiguation("Aabhas", 2205395, 19).resolve(n4jdb, solrdb)
    print NameDisambiguation("Rohit", 2205395, 19).resolve(n4jdb, solrdb)
