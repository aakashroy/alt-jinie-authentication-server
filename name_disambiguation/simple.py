import re
import string
import traceback

import pysolr
import spacy

from name_disambiguation import NameDisambiguation

nlp = spacy.load("en_core_web_sm")


def generate_ngrams(s, n):
    # Use the zip function to help us generate n-grams
    # Concatentate the tokens into ngrams and return
    ngrams = zip(*[s[i:] for i in range(n)])
    return [" ".join(ngram) for ngram in ngrams]


def disambiguate_names_from_text(org_id, text):
    response = {
        "results": [],
        "success": False
    }
    emails = []
    try:
        # First extract email addresses
        emails = re.findall(r'[\w\.-]+@[\w\.-]+', text)
        text = str(re.sub(r'[\w\.-]+@[\w\.-]+', "", text))
        text = re.sub("\s+", " ", text)
        text = text.translate(None, string.punctuation)
        text = u"" + text.lower()
        doc = nlp(text)
        word_tokens = [token.text for token in doc if (token.tag_ in ["NNP", "NN", "JJ", "IN", "DT", "RB", "NNS"])]
        query_n_grams = generate_ngrams(word_tokens, 2) + word_tokens
        j = 0
        while j < len(query_n_grams):
            print("Checking: {}".format(query_n_grams[j]))
            profile, _, _ = NameDisambiguation(query_n_grams[j], None, org_id, use_neo4j=False).resolve()
            if profile:
                names = query_n_grams[j].split(" ")
                emails.append(profile["official_email"])
                i = j + 1
                while i < len(query_n_grams):
                    for name in names:
                        if query_n_grams[i] == name:
                            del query_n_grams[i]
                            i = i - 1
                            break
                    i = i + 1
            j = j + 1
    except Exception:
        traceback.print_exc()
    if emails:
        response['results'] = list(set(emails))
        response['success'] = True
    return response


if __name__ == "__main__":
    print(disambiguate_names_from_text(19, "Amit garg and Harsimran Waila Rishabh Tickoo"))
