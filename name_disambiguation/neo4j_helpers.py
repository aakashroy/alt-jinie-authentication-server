import sys
from collections import OrderedDict


def get_best_match_from_neo4j(requester_id, org_id, emp_ids, ordering, n4jdb):
    ordering_dict = OrderedDict()

    for match_type, rule in ordering:
        ordering_dict[rule] = list()

    for emp_id in emp_ids:
        for match_type, rule in ordering:
            if rule not in ordering_dict:
                continue
            if match_type == "e":
                query = """match(emp :Employee)
                    where emp.{}={}
                    and emp.user_id={}
                    and emp.org_id={}
                    return emp
                    """.format(rule, emp_id, requester_id, org_id)
                results = n4jdb.run(query).data()
                if results:
                    query = """match(emp :Employee)
                                where emp.emp_id={}
                                and emp.org_id={}
                                return emp
                                """.format(results[0]["emp"][rule], org_id)
                    results = n4jdb.run(query).data()
                    ordering_dict[rule].append((results[0]["emp"], match_type, rule, 0))
                    break
            elif match_type == "r":
                query = """match (empA :Employee{{user_id:{user_id}}}) <-[:{relationship}]-(empB :Employee)
                    match (empC :Employee)<-[r:L1_MANAGES*]-(empB)
                    where not empC.user_id={user_id}
                    and empC.emp_id={emp_id}
                    and empC.org_id={org_id}
                    and empA.org_id={org_id}
                    and empB.org_id={org_id}
                    return empC, size(r)
                    """.format(
                    user_id=requester_id,
                    relationship=rule,
                    emp_id=emp_id,
                    org_id=org_id
                )
                results = n4jdb.run(query).data()
                if results:
                    ordering_dict[rule].append((results[0]["empC"], match_type, rule, results[0]["size(r)"]))
                    break

        del_flag = False
        for k in ordering_dict.keys():
            if ordering_dict[k] and not del_flag:
                del_flag = True
                continue
            if del_flag:
                del ordering_dict[k]

    min_value = sys.maxint
    ret_value = None, None, None
    for key, values in ordering_dict.items():
        for value in values:
            if value[3] < min_value:
                min_value = value[3]
                ret_value = value[0], value[1], value[2]
    return ret_value
