from conversational_flows.send_email import send_email_handler

HANDLER_TABLE = {
    "ML_CONV_SEND_EMAIL": send_email_handler,
    "ML_CONV_SEND_EMAIL_ERR_HANDLER": send_email_handler,
    "ML_CONV_SEND_EMAIL_FALLBACK": send_email_handler,
    "ML_CONV_SEND_EMAIL_END": send_email_handler
}


def handle_conversational_flows(session_data, cfc):
    user_message = {}
    intent_name = session_data["queryResult"]["intent"]["displayName"]
    if intent_name in HANDLER_TABLE:
        session_id = session_data.get("session", "")
        user_id = session_id.split("/")[-1].split("_")[0]
        if session_id in cfc:
            cfc[session_id]["conversation_context"].update(session_data)
            cfc[session_id]["follow_up"] = True
            user_message, end_conversation = HANDLER_TABLE[intent_name](cfc[session_id])

        else:
            cfc[session_id] = {
                "original_query": session_data.get("queryResult", {}).get("queryText", ""),
                "user_id": user_id,
                "conversation_context": session_data,
                "follow_up": False,
                "error_counter": 0
            }
            if len(user_id) > 15:
                cfc[session_id]["user_id"] = 2205395
            user_message, end_conversation = HANDLER_TABLE[intent_name](cfc[session_id])
        if end_conversation:
            print("Popping session ID: {}".format(session_id))
            cfc.pop(session_id, None)
    return user_message
