import json
import re
import string

import spacy

import config
from common import o365_helpers, mongo_helpers
from common.generic_functions import get_office_365_integration_embed_link
from common.mongo_helpers import get_org_id_from_alt_id
from name_disambiguation import NameDisambiguation

ERROR_MESSAGES = {
    "ERR_EMAIL_MISSING": "Who do you want to send the e-mail to? <br/> <i>You can provide a list of names and / or email addresses</i>",
    "ERR_SUBJECT_MISSING": "What should be the subject?",
    "ERR_BODY_MISSING": "Sure, and the body?"
}

nlp = spacy.load("en_core_web_sm")


def generate_ngrams(s, n):
    # Use the zip function to help us generate n-grams
    # Concatentate the tokens into ngrams and return
    ngrams = zip(*[s[i:] for i in range(n)])
    return [" ".join(ngram) for ngram in ngrams]


def rchop(thestring, ending):
    if thestring.endswith(ending):
        return thestring[:-len(ending)]
    return thestring


def store_email(session_data):
    print("Store email called")
    if session_data.get("email_addresses", []):
        print("Already have email addresses")
        return ""
    message = ""
    query = session_data["conversation_context"].get("queryResult", {}).get("queryText", "")
    matches = re.findall(r'[\w\.-]+@[\w\.-]+', query)
    if matches:
        session_data["email_addresses"] = matches
        print("Email addresses parsed and stored")

    query = str(re.sub(r'[\w\.-]+@[\w\.-]+', "", query))
    normalized_query = re.sub("\s+", " ", query)
    normalized_query = normalized_query.translate(None, string.punctuation)
    normalized_query = u"" + normalized_query.lower()
    normalized_query = re.sub(r"\b(to|send|an|email|and)\b", "", normalized_query)

    normalized_query = re.sub(r"subject.*", "", normalized_query)
    normalized_query = re.sub(r"body.*", "", normalized_query)

    doc = nlp(normalized_query)
    word_tokens = [token.text for token in doc if (token.tag_ in ["NNP", "NN", "JJ", "IN", "DT", "RB", "NNS"])]
    print([(token.text, token.tag_) for token in doc])
    query_n_grams = generate_ngrams(word_tokens, 2) + word_tokens
    new_emails = list()
    user_id = int(session_data["user_id"])
    org_id = get_org_id_from_alt_id(user_id, config)
    j = 0
    while j < len(query_n_grams):
        profile, _, _ = NameDisambiguation(query_n_grams[j], user_id, org_id).resolve()
        if profile:
            names = query_n_grams[j].split(" ")
            new_emails.append(profile["official_email"])
            i = j + 1
            while i < len(query_n_grams):
                for name in names:
                    if query_n_grams[i] == name:
                        del query_n_grams[i]
                        i = i - 1
                        break
                i = i + 1
        j = j + 1

    if new_emails:
        session_data["email_addresses"] = session_data.get("email_addresses", []) + new_emails

    if "email_addresses" not in session_data:
        message = ERROR_MESSAGES["ERR_EMAIL_MISSING"]
        print("Email address missing, sending error message")

    is_follow_up = session_data.get("follow_up", False)
    print("Is follow up? (store_email) ", is_follow_up)
    if is_follow_up:
        session_data["conversation_context"]["queryResult"]["queryText"] = ""
    return message


def store_subject(session_data):
    print("Store subject called")
    if session_data.get("subject", ""):
        print("Already have subject")
        return ""
    stop_word = "subject"
    unstop_word = "body"
    query = session_data["conversation_context"].get("queryResult", {}).get("queryText", "")
    subject_part = list()

    is_follow_up = session_data.get("follow_up", False)
    print("Is follow up? (store_subject) ", is_follow_up)
    if is_follow_up:
        session_data["conversation_context"]["queryResult"]["queryText"] = ""

    writing_state = 1 if is_follow_up else 0
    for word in query.split():
        if not is_follow_up and re.search(r"\b" + stop_word + r"\b", word.lower()):
            writing_state = 1
            stop_word = stop_word + "_DONE"
            continue
        elif writing_state == 1 and re.search(r"\b" + unstop_word + r"\b", word.lower()):
            break
        if writing_state == 1:
            subject_part.append(word)

    subject_part = (" ".join(subject_part)).strip()
    subject_part = subject_part.strip(" ,")
    subject_part = rchop(subject_part, "and")

    if not subject_part:
        print("Subject missing, sending error message")
        return ERROR_MESSAGES["ERR_SUBJECT_MISSING"]

    session_data["subject"] = subject_part
    print("Subject parsed and stored")

    return ""


def store_body(session_data):
    print("Store body called")
    if session_data.get("body", ""):
        print("Already have body")
        return ""
    stop_word = "body"
    query = session_data["conversation_context"].get("queryResult", {}).get("queryText", "")
    body_part = list()

    is_follow_up = session_data.get("follow_up", False)
    print("Is follow up? (store_subject) ", is_follow_up)
    if is_follow_up:
        session_data["conversation_context"]["queryResult"]["queryText"] = ""

    writing_state = 1 if is_follow_up else 0
    for word in query.split():
        if not is_follow_up and re.search(r"\b" + stop_word + r"\b", word.lower()):
            writing_state = 1
            stop_word = stop_word + "_DONE"
            continue
        if writing_state == 1:
            body_part.append(word)

    body_part = (" ".join(body_part)).strip()
    body_part = body_part.strip(" ,")
    body_part = rchop(body_part, "and")

    if not body_part:
        print("Body missing, sending error message")
        return ERROR_MESSAGES["ERR_BODY_MISSING"]

    session_data["body"] = body_part
    print("Body parsed and stored")

    return ""


def get_confirmation(session_data):
    if session_data.get("confirmation", ""):
        print("Have confirmation!")
        return ""
    query = session_data["conversation_context"].get("queryResult", {}).get("queryText", "").lower()

    is_follow_up = session_data.get("follow_up", False)
    print("Is follow up? (store_subject) ", is_follow_up)
    if is_follow_up:
        session_data["conversation_context"]["queryResult"]["queryText"] = ""

    if query in ["yes", "y", "no", "n"]:
        if "y" in query:
            session_data["confirmation"] = "y"
        elif "n" in query:
            session_data["confirmation"] = "n"
            session_data["conversation_end_message"] = "Sure, I won't send the email."
            return "n"
    elif session_data.get("email_addresses", []) and session_data.get("subject", "") and session_data.get("body"):
        msg_body = "<b>Ready to send an email to: </b><br/>"
        for eml in session_data["email_addresses"]:
            msg_body = msg_body + " > " + eml + "<br/>"
        msg_body = msg_body + "<br/><b>with subject: </b><br/>"
        msg_body = msg_body + session_data["subject"] if session_data["subject"] else "Empty Subject"
        msg_body = msg_body + "<br/><br/><b>and body: </b><br/>"
        msg_body = msg_body + session_data["body"] if session_data["body"] else "Empty Body"
        msg_body = msg_body + "<br/><br/>Reply with: YES | NO ?"
        return json.dumps({
            "custom_design": {
                "design": {
                    "selectionOptions": [

                    ],
                    "label": msg_body,
                    "type": "QUESTION",
                    "selectedValue": [],
                    "buttons": [
                        {
                            "label": "Yes",
                            "action": 2,
                            "messageText": "Yes"
                        },
                        {
                            "label": "No",
                            "action": 2,
                            "messageText": "No"
                        }

                    ]
                }
            }
        })
    else:
        print("get_confirmation Error!")
    return ""


def send_email(session_data):
    if session_data.get("email_addresses", []) and session_data.get("subject", "") and session_data.get("body"):
        subject = session_data.get("subject", "")
        body = session_data.get("body", "")
        # if "eom" not in subject.lower():
        #     subject = subject + " <EOM>"
        user_data = o365_helpers.get_user_data(session_data["user_id"], config)
        o365_helpers.send_mail(session_data["email_addresses"], body, subject, user_data, config)
        if session_data.get("one_shot", False):
            session_data["conversation_end_message"] = "Done!"
        else:
            session_data[
                "conversation_end_message"] = "Done! <br><i>I know I ask too many questions, though. Next time, you can say: Send an email to abc@xyz.com with subject 'Hi There!' and body 'How are you doing?' and it'll be done :)</i>"

        print("Email Sent!")
    else:
        print("send_email Error!")
    return ""


VALIDATION_SEQUENCE = [store_email, store_subject, store_body, get_confirmation, send_email]


def check_scope(alt_id, scope_name):
    user_details = mongo_helpers.get_user_details_by_alt_id(alt_id, config)
    if "scopes" in user_details:
        if scope_name in user_details["scopes"]:
            return True
        else:
            return False
    return False


def send_email_handler(session_data):
    if "scopes_checked" not in session_data:
        if not check_scope(session_data["user_id"], "Mail.Send"):
            return {
                       "fulfillmentText": "Hmm, looks like you don't have this feature enabled."
                                          "<br/>No worries, {} "
                                          "to enable this feature :)".format(
                           get_office_365_integration_embed_link(session_data["user_id"], text="Click here"))
                   }, True
    else:
        session_data["scopes_checked"] = True

    return_message = ""
    end_conversation = False
    intent_name = session_data["conversation_context"]["queryResult"]["intent"]["displayName"]
    if intent_name == "ML_CONV_SEND_EMAIL_END":
        return_message = {"fulfillmentText": session_data["conversation_end_message"]}
        end_conversation = True
    elif session_data.get("validation_message", ""):
        print("Already have validation message to send!")
        val_msg = session_data["validation_message"]
        session_data.pop("validation_message")
        return_message = {"fulfillmentText": val_msg}
    else:
        _ret_msg = ""
        start = 0
        if not session_data.get("follow_up", False):
            for i in range(0, len(VALIDATION_SEQUENCE) - 2):
                r = VALIDATION_SEQUENCE[i](session_data)
                if r:
                    _ret_msg = r
            if not _ret_msg:
                session_data["one_shot"] = True
                start = len(VALIDATION_SEQUENCE) - 2

        for i in range(start, len(VALIDATION_SEQUENCE)):
            return_message = VALIDATION_SEQUENCE[i](session_data)
            if return_message:
                session_data["error_counter"] = session_data["error_counter"] + 1
                session_data["validation_message"] = return_message
                return_message = {
                    "followupEventInput": {
                        "name": "ml-conv-send-email-ctxt",
                        "parameters": {
                        },
                        "languageCode": "en-US"
                    }
                }
                break
        if (i == (len(VALIDATION_SEQUENCE) - 1)) \
                or session_data.get("confirmation", "") == "n" \
                or session_data["error_counter"] > 5:
            if session_data["error_counter"] > 5:
                session_data[
                    "conversation_end_message"] = "Oops, I think I may not be able to help any further, I am not yet trained to the capacity of humans :) Bye!"
            return_message = {
                "followupEventInput": {
                    "name": "ML_CONV_SEND_EMAIL_END_EVENT",
                    "parameters": {
                    },
                    "languageCode": "en-US"
                }
            }
    print ("send_email_handler Returning,", return_message, end_conversation)
    return return_message, end_conversation


if __name__ == "__main__":
    store_email({"user_id": 2205395,
                 "conversation_context": {"queryResult": {
                     "queryText": "Send an email to Vishal Saha and Harsimran and Vivek Sethi, Gurjeet, Aditya"}},
                 "follow_up": False,
                 "error_counter": 0})
