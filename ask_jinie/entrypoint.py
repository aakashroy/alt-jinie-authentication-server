"""Entry point for handling all Jinie Intents"""
import copy
import time

import stomp

import alt_jinie_actions
import config
import jinie_notification_messaging_templates
from ask_jinie import transactional, course_recommendation, calendars, performance, msteams_meetings,zoho_meetings,google_meetings
from ask_jinie.bloodgroup.blood_group_normalization import blood_group_normalization
from ask_jinie.calendars.handlers import o365_meeting_handler
from ask_jinie.course_recommendation.recommender.handlers import course_recommendation_handler
# from ask_jinie.transactional.common_functions import getnamebyid, getempsalary, getworksite, my_blood_group, get_confirm_status, get_grade, get_joining_date
from ask_jinie.msteams_meetings.msteams_meetings import handle_msteams_meeting_query
from ask_jinie.zoho_meetings.zoho_meetings import handle_zoho_meeting_query
from ask_jinie.google_meetings.google_meetings import handle_google_meeting_query
from ask_jinie.performance.handlers import handle_performance_query
from ask_jinie.transactional.common_functions import *
from common import logging, sql_helpers, optimizations

TRANSFORM_FN_DICT = {'getName': getnamebyid, 'getSalary': getempsalary, 'worksite': getworksite,
                     'bloodgroup': my_blood_group, 'getConfirmStatus': get_confirm_status, 'getGrade': get_grade,
                     'getJoiningDate': get_joining_date, 'getConfirmationDueDate': get_confirmation_dueDate,
                     'getMaritalStatus': get_marital_status, "getDateofMarriage": get_dateof_mariage,
                     'getGender': get_gender, 'getBirthPlace': get_birth_place, 'getFatherName': get_father_name,
                     'getDateOfBirth': get_date_of_birth, 'getAge': get_age, 'getCountryOfBirth': get_country_of_birth,
                     'getPersonalEmailId': get_personal_emailid, 'getMobileNumber': get_mobile_number,
                     'getDesignation': get_designation, 'getEmployeeStatus': get_employee_status,
                     'getEmployeeCategory': get_employee_category, 'getDependentDetails': get_dependent_details,
                     'getEmergencyContacts': get_emergency_contacts, 'getBranchName': get_branch_name,
                     'getIFSCCode': get_ifsc_code,
                     'getESCINumber': get_esci_Number}


# Only returns false as of now on graceful error scenarios
def ask_jinie_handler(intent, alt_user_id, additional_parameters, num):
    """
    Ask jinie handler
    :param intent: Intent
    :param alt_user_id: AltUserId
    :param additional_parameters: Additional Parameters (if any)
    :param num: num
    :return: Message for particular intent
    """
    message = ""
    error_message = ""
    if intent in transactional.intent_mapping.INTENT_MAP:
        if 'DB' not in transactional.intent_mapping.INTENT_MAP[intent]:
            message = transactional.intent_mapping.INTENT_MAP[intent]["onError"]
            if additional_parameters['blood_group']:
                if additional_parameters['blood_confirmation'] == "No":
                    message = transactional.intent_mapping.INTENT_MAP[intent]["onSuccess_no"]
                elif additional_parameters['blood_confirmation'] == "Yes":
                    reqd_bl_grp = blood_group_normalization(additional_parameters['blood_group'])
                    if reqd_bl_grp:
                        message = transactional.intent_mapping.INTENT_MAP[intent]["onSuccess"]
                        notify_msg = copy.deepcopy(jinie_notification_messaging_templates.NOTIFICATION_REQUEST)
                        notify_msg['message_type'] = 2
                        notify_msg['destination_alt_user_id'] = int(alt_user_id)
                        notify_msg['action'] = {"blood_group": additional_parameters['blood_group'],
                                                "action": alt_jinie_actions.NOTIFY_BLOOD_DONORS}
                        activemq_conn = stomp.Connection(config.ACTIVEMQ_CONNECTION)
                        activemq_conn.start()
                        activemq_conn.connect()

                        activemq_conn.send(destination=config.ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE,
                                           body=optimizations.json_dumps_minify(notify_msg),
                                           headers={'persistent': 'true'})
                        activemq_conn.disconnect()
                    else:
                        message = transactional.intent_mapping.INTENT_MAP[intent]["onError"]
                else:
                    message = transactional.intent_mapping.INTENT_MAP[intent]["onError"]
        else:
            crsr = sql_helpers.connect_to_mssql_server(transactional.intent_mapping.INTENT_MAP[intent]['DB'],
                                                       config.MSSQL_CREDENTIALS)
            if crsr is not None:
                start = time.time()
                select_clause = "select " + transactional.intent_mapping.INTENT_MAP[intent]['DB_Column']
                from_clause = " from " + transactional.intent_mapping.INTENT_MAP[intent]['DB_Table']
                where_clause = " where UserID=%s"
                query = select_clause + from_clause + where_clause
                crsr.execute(query, (alt_user_id))
                results = crsr.fetchall()
                message = transactional.intent_mapping.INTENT_MAP[intent]["onError"]
                if results and results[0][0] is not None and unicode(results[0][0]):
                    if 'transform' in transactional.intent_mapping.INTENT_MAP[intent].keys():
                        transform_result = TRANSFORM_FN_DICT[
                            transactional.intent_mapping.INTENT_MAP[intent]['transform']](crsr, results[0][0])
                        if transform_result is not None:
                            if 'design' in transactional.intent_mapping.INTENT_MAP[intent].keys() \
                                    and transactional.intent_mapping.INTENT_MAP[intent]['design'] == 'table':
                                message = {"theader": transactional.intent_mapping.INTENT_MAP[intent]['theader'],
                                           "tbody": transform_result,
                                           "label": transactional.intent_mapping.INTENT_MAP[intent]["onSuccess"]}
                            else:
                                message = transactional.intent_mapping.INTENT_MAP[intent]["onSuccess"]. \
                                    replace("##INTENT_RESPONSE_VALUE##", unicode(transform_result))
                        else:
                            if 'design' in transactional.intent_mapping.INTENT_MAP[intent].keys() \
                                    and transactional.intent_mapping.INTENT_MAP[intent]['design'] == 'table':
                                message = {"theader": transactional.intent_mapping.INTENT_MAP[intent]['theader'],
                                           "tbody": transform_result,
                                           "label": transactional.intent_mapping.INTENT_MAP[intent]["onError"]}
                            else:
                                message = transactional.intent_mapping.INTENT_MAP[intent]["onError"]

                    else:
                        message = transactional.intent_mapping.INTENT_MAP[intent]["onSuccess"].replace(
                            "##INTENT_RESPONSE_VALUE##",
                            unicode(results[0][0]))
                else:
                    if 'design' in transactional.intent_mapping.INTENT_MAP[intent].keys() \
                            and transactional.intent_mapping.INTENT_MAP[intent]['design'] == 'table':
                        message = {"theader": transactional.intent_mapping.INTENT_MAP[intent]['theader'],
                                   "tbody": None,
                                   "label": transactional.intent_mapping.INTENT_MAP[intent]["onError"]}

                end = time.time()
                logging.log(logging.SEVERITY_DEBUG, "ASKJINIE", "Took: " + str(end - start) + " seconds.")
            else:
                error_message = "Connection Error"
    elif intent in course_recommendation.intent_mapping.INTENT_MAP:
        message, error_message = course_recommendation_handler(intent, alt_user_id, additional_parameters['skills'],
                                                               additional_parameters['ml_course_level'],
                                                               additional_parameters['duration'],
                                                               additional_parameters['ml_course_price'],
                                                               num)
    elif intent in calendars.intent_mapping.INTENT_MAP:

        message, error_message = o365_meeting_handler(intent, alt_user_id, additional_parameters)
    elif intent in performance.intent_mapping.INTENT_MAP:
        message, error_message = handle_performance_query(intent, alt_user_id, additional_parameters)
    elif intent in msteams_meetings.intent_mapping.INTENT_MAP:
        message, error_message = handle_msteams_meeting_query(intent, alt_user_id, additional_parameters)
    elif intent in zoho_meetings.intent_mapping.INTENT_MAP:
        message, error_message = handle_zoho_meeting_query(intent, alt_user_id, additional_parameters)
    elif intent in google_meetings.intent_mapping.INTENT_MAP:
        message, error_message = handle_google_meeting_query(intent, alt_user_id, additional_parameters)
    else:
        error_message = "Unknown Intent"
    return message, error_message


if __name__ == "__main__":
    # print ask_jinie_handler("ML_MY_BLOOD_GROUP", "28298", "O+", 3)
    # print ask_jinie_handler("ML_AGE", "28298", "O+", 3)
    print ask_jinie_handler("ML_GOOGLE_MEETING", 2721489, "", 19)
    # print ask_jinie_handler("ML_SCHEDULE_MEETING",2653216 , { "attendees": ["nishant garg"],
    #                                                         "duration":45,
    #                                                         "meeting_date":"25/sep/2019",
    #                                                         "title":"Test meeting",
    #                                                         "meeting_start_time": "3:00 pm",
    #                                                         'meeting_location':'office',
    #                                                         'mail_body':'hello'
    #                                                         }
    #                                                         , 3)

    # print ask_jinie_handler("ML_FIND_MEETING_TIMES", 2653216,
    #                         {"attendees": "nishant.garg@peoplestrong.com",
    #                          "duration": 45,
    #                          "when": "05/sep/2019",
    #                          "title": "Test meeting"},3)
