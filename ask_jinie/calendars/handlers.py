import base64
import pprint
import re
import traceback
from datetime import datetime, timedelta

import pymongo
from jiniepush.api_responses import get_cards_api_response

import config
from ask_jinie.calendars.meeting_times_helpers import find_meeting_times, schedule_meeting
from common.generic_functions import sort_calendar_link_on_time
# from ask_jinie.calendars.find_meeting_times_helpers import find_meeting_times
from common.mongo_helpers import get_org_id_from_alt_id
from common.optimizations import json_dumps_minify
from common.security import ps_encrypt
from name_disambiguation.simple import disambiguate_names_from_text

__G_NAMES = None


def get_names(org_id):
    global __G_NAMES
    if __G_NAMES is None:
        __G_NAMES = get_user_details_from_first_name(config, org_id=org_id)
    return __G_NAMES


def get_user_details_from_first_name(conf, org_id):
    result = []
    # s = time()
    conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
    database = conn[conf.MONGO_DB_ALT_JINIE]
    coll = database[conf.MONGO_COLL_USERS]
    for obj in coll.find():
        if obj['org_id'] == org_id:
            first = obj['first_name'].lower()
            middle = obj['middle_name'].lower()
            last = obj['last_name'].lower()

            result.append([first + " " + middle + " " + last, first + " " + last, first + " " + middle, first])

    return result


def find_users(sentence, names=None, org_id=None):
    if names:
        found_name = []

        for name_l in names:
            for name in name_l:
                if len(name) <= 2:
                    continue
                name = re.sub('\s+?', ' ', name)
                name = name.strip()
                # regex = r'(\b|\s?,\s?)' + name + r'(\b|\s?,\s?)'
                regex = r'(\b)' + name + r'(\b)'
                if re.search(regex, sentence):
                    found_name.append(name)
                    sentence = sentence.replace(name, '')
                    break
        return found_name
    else:
        result = disambiguate_names_from_text(org_id, sentence)
        if result['success']:
            return result['results']
    return []


def o365_meeting_handler(intent, alt_user_id, additional_parameters):
    if intent == "ML_FIND_MEETING_TIMES":
        errormessage = ""
        response_msg = ""
        try:
            attendees = additional_parameters["attendees"]
            duration = additional_parameters["duration"]
            when = additional_parameters["meeting_date"]
            title = additional_parameters["title"]
            query = additional_parameters['query']
            # days = ['today', 'tomorrow', 'next week', 'later this week']
            # if not when:
            #     for day in days:
            #         if day in query:
            #             when = day
            # cachedStopWords = stopwords.words("english")
            # attendees_list = re.split("\s?,\s?| ", query)
            # attendees_list = [word for word in attendees_list if word not in cachedStopWords]
            # attendees_list = [word for word in attendees_list if len(word) >= 3]
            #
            # # attendees_list = re.split(',| and | & ', attendees_list)
            # attendees_list = [x.strip().lower() for x in attendees_list if
            #                   x is not None and x.strip() != '']  # keeping valid syntactic namees only
            names = get_names(org_id=get_org_id_from_alt_id(alt_user_id, config))
            attendees_list = find_users(query, names)
            print "Attendees List" + str(attendees_list)
            # attendee_details = find_attendee_information(alt_user_id,
            #                                              attendees_list,
            #                                              config)
            # for attendee in attendee_details['attendees']:
            #     attendee_name = list(attendee.keys())[0]
            #     attendee_value = list(attendee.values())[0]
            #     if attendee_value['name_count'] == 0:
            #         del attendee[attendee_name]
            # attendee_details = [i for i in attendee_details['attendees'] if len(i)]

            response_msg, errormessage = find_meeting_times_handler(alt_user_id, attendees_list, duration, when,
                                                                    title)
            # print "Inside handler"
            # print response_msg
            # print type(response_msg)
        except Exception:
            errormessage = traceback.format_exc()

        return response_msg, errormessage
    elif intent == "ML_SCHEDULE_MEETING":
        errormessage = ""
        response_msg = ""
        try:
            meeting_attendees = additional_parameters['meeting_attendees']
            if not additional_parameters["meeting_info"]:
                additional_parameters["meeting_info"] = "no"
            org_id = get_org_id_from_alt_id(alt_user_id, config)
            # names = get_names(org_id=get_org_id_from_alt_id(alt_user_id, config))
            attendees_list = find_users(meeting_attendees, org_id=org_id)
            response_msg = schedule_meeting(alt_id=alt_user_id,
                                            attendees_list=attendees_list,
                                            subject=additional_parameters['meeting_title'],
                                            location=additional_parameters['meeting_location'],
                                            meeting_date=additional_parameters['meeting_date'],
                                            meeting_start_time=additional_parameters['meeting_start_time'],
                                            meeting_duration_min=additional_parameters['meeting_duration'],
                                            mail_body=additional_parameters['mail_body'],
                                            create_meeting_link=additional_parameters["meeting_info"],
                                            config=config)
            if isinstance(response_msg, dict):
                body = response_msg["body"]  # body of mail
                title = response_msg["title"]  # subject of mail
                start_time = response_msg["start_time"]  # datetime string
                end_time = response_msg["end_time"]  # dateetime string
                attendees_list = response_msg["attendees"]  # [{"user":"nishant","id":"ng@peoplstrong.com,{.....}]"
                location = response_msg["location"]  # string
                outlook_link = response_msg["outlook_link"]
                cancel_link = response_msg["cancel_link"]
                has_errors = response_msg["has_errors"]
                date = re.findall("\d{4}-\d{2}-\d{2}", start_time)[0]
                date = datetime.strptime(date, "%Y-%m-%d")
                new_date = date.strftime("%d-%b")
                s_time = re.findall("\d{2}:\d{2}", start_time)[0]
                s_time = datetime.strptime(s_time, "%H:%M") + timedelta(hours=5, minutes=30)
                s_time = s_time.strftime("%I:%M %p")
                e_time = re.findall("\d{2}:\d{2}", end_time)[0]
                e_time = datetime.strptime(e_time, "%H:%M") + timedelta(hours=5, minutes=30)
                e_time = e_time.strftime("%I:%M %p")

                # meeting_time = new_date + " " + s_time + " - " + e_time
                meeting_time = new_date + " " + s_time
                # if has_errors == True:
                #     body = "Meeting created without some attendees due to ambiguity. You can always add these user to the meeting manually"
                response_msg = get_calendar_api_response(body, title, meeting_time, e_time, attendees_list, location,
                                                         outlook_link, cancel_link)

                # attendees = list()
                # for al in attendees_list:
                #     attendees.append({"name": al["name"], "id": al["email"]})
                # greeting = "Here are your Meeting Details"
                #
                #
                # meeting_actions = [{"label": "Cancel Meeting", "val": cancel_link},
                #                    {"label": "Open in Outlook", "val": outlook_link}]
                # # print "open meeting: ", open_meeting_url
                # send_meeting_reminder_card(str(alt_user_id), greeting, title, meeting_time, location, attendees,
                #                            meeting_actions,
                #                            verbose=False)


        except Exception:
            errormessage = traceback.format_exc()
        return response_msg, errormessage

    else:
        return []


def find_meeting_times_handler(alt_user_id, attendees, duration, when, title):
    errormessage = ""
    response_msg = ""
    try:
        # attendees = [a.keys()[0] for a in attendees]
        response = find_meeting_times(attendees, duration, when, alt_user_id, config)

        if response["success"]:
            in_limbo_message = ""
            # for result in response["results"]:
            l1_l2_amb_error = response["results"]['err_message']
            result = response["results"]['data'][0]
            if title is None:
                title = "{} Minute meeting created using Jinie | {} Participants".format(str(duration),
                                                                                         str(len(result["attendees"])))
            # in_limbo_message = in_limbo_message + \
            #                    "<br/><b>{0}</b><br/>".format(
            #                        result["dow"])
            list_tuples = list()
            for slot in result["slots"]:
                # hrdt_hdr = slot[0].strftime("%A, %d %b %Y")
                day = slot[0].strftime("%A")
                hrdt_hdr = slot[0].strftime("%d-%b-%Y")
                hrdt_start = slot[0].strftime("%I:%M %p")
                hrdt_end = slot[1].strftime("%I:%M %p")
                params = dict()
                params["t"] = title
                params["oe"] = result["organizer_email"]
                params["oai"] = result["organizer_alt_id"]
                params["ooi"] = result["organizer_org_id"]
                params["at"] = result["attendees"]
                params["rt"] = result["refresh_token"]
                params['atok'] = result["access_token"]
                params['st'] = str(slot[0])
                params['et'] = str(slot[1])
                params_get = base64.urlsafe_b64encode(json_dumps_minify(params))

                cipher_params_get = ps_encrypt(params_get)

                # a_href_wrap = "<a href='{0}/actions/calendar/create?params={1}'>{2}</a>"
                # in_limbo_message = in_limbo_message + a_href_wrap.format(config.SUBDOMAIN, cipher_params_get,
                #                                                          " > {0}: {1} - {2}<br/><br/>".format(
                #                                                              hrdt_hdr, hrdt_start,
                #                                                              hrdt_end))\
                list_tuples.append((cipher_params_get, hrdt_hdr, hrdt_start, hrdt_end))

            index_of_sorted_list = sort_calendar_link_on_time(list_tuples)
            meeting_list = list()
            for index in index_of_sorted_list:
                cipher_params_get = list_tuples[index][0]
                hrdt_hdr = list_tuples[index][1]
                hrdt_start = list_tuples[index][2]
                hrdt_end = list_tuples[index][3]
                # a_href_wrap = "<a href='{0}/actions/calendar/create?params={1}'>{2}</a>"
                # in_limbo_message = in_limbo_message + a_href_wrap.format(config.SUBDOMAIN, cipher_params_get,
                #                                                          " > {0}: {1} - {2}<br/><br/>".format(
                #                                                              hrdt_hdr, hrdt_start,
                #                                                              hrdt_end))
                # s_time = hrdt_start + " - " + hrdt_end
                link = config.SUBDOMAIN + "/actions/calendar/create?params=" + cipher_params_get

                meeting_list.append(
                    {"link": link, "day": day, "date": hrdt_hdr, "s_time": hrdt_start, "e_time": hrdt_end})
            card = get_calender_card_layout(meeting_list)
            in_limbo_message = get_cards_api_response("You can choose from the slots below<br/> " + l1_l2_amb_error,
                                                      card)
            if in_limbo_message:
                # response_msg = "You can schedule from the below mentioned slots {0}: <br/><br/>".format(
                #     result["dow"].lower())
                # response_msg = response_msg + in_limbo_message
                response_msg = in_limbo_message
            else:
                response_msg = "I'm sorry, looks like everyone is busy. There are no slots available :("
        else:
            response_msg = response["results"]["message"]
    except Exception:
        errormessage = traceback.format_exc()
    return response_msg, errormessage


# Hartaran
def get_calendar_api_response(body, title, start_time, end_time, attendees, location, outlook_link, cancel_link):
    print "Body: ", body
    attendees_list = []
    for attendee in attendees:
        attendees_list.append({'email': attendee['email'], 'name': attendee['name']})

    final_layout = {"data": {
        "FAQ": {"status": "Success", "message": body, "code": "EC200", "data": [
            {
                "design": {
                    "selectionOptions": [],
                    "selectedValue": [],
                    "type": "TEXT",
                    "label": body}
            },
            {
                "design": {
                    "cellTrackerID": None,
                    "sequence": None,
                    "displaySeq": None,
                    "subType": "UPCOMING_MEETING",
                    "displayDataType": None,
                    "questionID": None,
                    "dataArray": [
                        {
                            "metgTime": start_time + " - " + end_time,
                            "metgTitle": title,
                            "metgLoc": location,
                            "metgActs": [
                                {
                                    "val": outlook_link,
                                    "label": "Open in Outlook"},
                                {
                                    "val": cancel_link,
                                    "label": "Cancel Meeting"}],
                            "type": "CAL",
                            "membs": attendees_list}],
                    "selectionType": None,
                    "flow": None,
                    "label": "Here are your meeting details",
                    "selectionOptions": None,
                    "type": "OUTLOOK",
                    "selectedValue": None}}]}},
        "messageCodeTO": {"status": "Success",
                          "message": "Request accepted successfully.", "code": "EC200",
                          "data": None}}

    return final_layout


def get_calender_card_layout(skill_list):
    """
    Function to find course from MOOC's
    :param skill_list: List of skills
    :return: List of courses from Udemy API
    """

    # {"link": cipher_params_get, "day": day, "date": hrdt_hdr, "time": time}
    function_output = list()
    for calendar in skill_list:
        icon = config.SUBDOMAIN + "/static/images/calendar.jpg"
        function_output.append({
            "course_id": None,
            "course_name": calendar["link"],
            "source": "",
            "icon": icon,
            "heading_1": calendar["day"],
            "heading_2": calendar["date"],
            "heading_3": "S: " + calendar["s_time"],
            "footer": "E: " + calendar["e_time"],
            "user_click_actions": [
                {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                 "url": calendar["link"]}]
        })
    return function_output


if __name__ == "__main__":
    # print json.dumps(o365_meeting_handler("ML_FIND_MEETING_TIMES", 2653216, ["Amber and Hartaran"]),
    #                  indent=4, default=unicode)

    # print(o365_meeting_handler())

    # print json.dumps(o365_meeting_handler("ML_FIND_MEETING_TIMES", 2653216, {
    #     "attendees": "nishant.garg@peoplestrong.com",
    #     "duration": 45,
    #     "when": "05/sep/2019",
    #     "title": "Test meeting"
    #
    # }),
    #                  indent=4, default=unicode)

    pprint.pprint(get_calendar_api_response("body", "title", "2 pm", "3 pm",
                                            [{"name": "x", "id": "asdasd"}, {"name": "yy", "id": "asdzcd"}],
                                            "location"))
