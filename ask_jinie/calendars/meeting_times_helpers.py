# -*- coding: utf-8 -*-
import base64
import json
import re
import string
import traceback
from datetime import datetime, timedelta

import pytz
from dateutil import parser as date_util_parser
from tzlocal import get_localzone

import config
from ask_jinie.msteams_meetings.msteams_meetings import create_msteams_meeting
from common import sql_helpers
from common.generic_functions import is_single_word, verify_email, parse_meeting_timings, convert_list_to_string, \
    get_office_365_integration_embed_link
from common.mongo_helpers import get_user_details_by_alt_id, \
    get_user_details_from_first_name, get_user_details_from_full_name, get_user_id_from_emp_id
from common.o365_helpers import get_user_data, find_meeting_times_o365, refresh_access_token, o365_meeting_handler, \
    o365_cancel_meeting_handler
from common.optimizations import json_dumps_minify
from common.security import ps_encrypt


# from sample_to_del import send_custom_notification


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def clean_string(inputstr):
    """
    :param inputstr: Clean String
    :return: string
    """
    outputstr = inputstr.lower()
    email_regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'

    if isinstance(outputstr, unicode):
        if not re.search(email_regex, inputstr):
            outputstr = translate_punctuation_to_empty(outputstr)
    else:
        if not re.search(email_regex, inputstr):
            outputstr = outputstr.translate(None, string.punctuation)
    outputstr = re.sub(r'\s+', '', outputstr)
    return outputstr.strip()


def convert_to_utc(datetimeobj):
    utc = datetimeobj.astimezone(pytz.timezone("UTC"))
    return utc


def convert_from_anytz_to_local(datetimestr, timezonestr):
    timezone = pytz.timezone(timezonestr)
    dtm_musecs = datetimestr.split(".")
    tz_aware_dt = timezone.localize(datetime.strptime(dtm_musecs[0], "%Y-%m-%dT%H:%M:%S"))
    return tz_aware_dt.astimezone(get_localzone())


def get_time_constraint(timeslots):
    timeslots_ms_format = list()
    for timeslot in timeslots:
        start_t = convert_to_utc(timeslot[0])
        start = start_t.strftime('%Y-%m-%dT%H:%M:%S')
        end_t = convert_to_utc(timeslot[1])
        end = end_t.strftime('%Y-%m-%dT%H:%M:%S')
        timeslots_ms_format.append({
            "start": {
                "dateTime": start,
                "timeZone": str(start_t.tzinfo)
            },
            "end": {
                "dateTime": end,
                "timeZone": str(end_t.tzinfo)
            }
        })
    return {
        "timeConstraint": {
            "activityDomain": "work",
            "timeslots": timeslots_ms_format
        }
    }


def check_date(when):
    local_tz = get_localzone()

    today = local_tz.localize(datetime.now())
    for fmt in ('%Y-%m-%d', '%Y.%m.%d', '%Y/%m/%d', '%d.%m.%Y', '%d/%m/%Y', '%d-%m-%Y',
                '%d %B, %Y', '%d-%B-%Y', '%d/%B/%Y', '%d %b, %Y', '%d-%b-%Y', '%d/%b/%Y'):
        try:

            date = datetime.strptime(when, fmt)
            local_tz = get_localzone()
            new_date = local_tz.localize(date)
            start = local_tz.localize(datetime.strptime(
                "{0}-{1}-{2} 00:00:00".format(new_date.year, new_date.month,
                                              new_date.day),
                "%Y-%m-%d %H:%M:%S"
            ))

            end = local_tz.localize(datetime.strptime(
                "{0}-{1}-{2} 23:59:59".format(new_date.year, new_date.month,
                                              new_date.day),
                "%Y-%m-%d %H:%M:%S"
            ))
            return (start, end)
        except:
            pass


def generate_timeslots(when):
    timeslots = list()
    local_tz = get_localzone()

    today = local_tz.localize(datetime.now())

    tomorrow = today + timedelta(days=1)

    later_this_week_start = today + timedelta(days=2)

    later_this_week_end = later_this_week_start + timedelta(days=4 - later_this_week_start.weekday())

    later_next_week_start = today + timedelta(days=0 - today.weekday() + 7)

    later_next_week_end = later_next_week_start + timedelta(days=4)

    # oob_today = local_tz.localize(datetime.strptime(
    #     "{0}-{1}-{2} 00:00:00".format(today.year, today.month, today.date),
    #     "%Y-%m-%d %H:%M:%S"
    # ))

    cob_today = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(today.year, today.month, today.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_tomorrow = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(tomorrow.year, tomorrow.month, tomorrow.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_tomorrow = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(tomorrow.year, tomorrow.month, tomorrow.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_later_this_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(later_this_week_start.year, later_this_week_start.month,
                                      later_this_week_start.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_later_this_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(later_this_week_end.year, later_this_week_end.month, later_this_week_end.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    oob_later_next_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 00:00:00".format(later_next_week_start.year, later_next_week_start.month,
                                      later_next_week_start.day),
        "%Y-%m-%d %H:%M:%S"
    ))

    cob_later_next_week = local_tz.localize(datetime.strptime(
        "{0}-{1}-{2} 23:59:59".format(later_next_week_end.year, later_next_week_end.month, later_next_week_end.day),
        "%Y-%m-%d %H:%M:%S"
    ))
    if when == "today":
        if today.weekday() < 5:
            timeslots.append({
                "type": "Today",
                "start": today,
                "end": cob_today,
                "avlabl": True
            })
        else:
            timeslots.append({
                "type": "Today",
                "avlabl": False
            })
    if when == "tomorrow":
        if tomorrow.weekday() < 5:
            timeslots.append({
                "type": "Tomorrow",
                "start": oob_tomorrow,
                "end": cob_tomorrow,
                "avlabl": True
            })
        else:
            timeslots.append({
                "type": "Tomorrow",
                "avlabl": False
            })
    if when == "later_this_week":
        print "later this week: ", later_this_week_start.weekday()
        if later_this_week_start.weekday() < 5:
            timeslots.append({
                "type": "Later this week",
                "start": oob_later_this_week,
                "end": cob_later_this_week,
                "avlabl": True
            })
        else:
            timeslots.append({
                "type": "Later this week",
                "avlabl": False
            })
    if when == "next_week":
        timeslots.append({
            "type": "Next week",
            "start": oob_later_next_week,
            "end": cob_later_next_week,
            "avlabl": True
        })

    return timeslots


def get_ms_formatted_attendee_list(m_attendees):
    attendees = {'attendees': list()}
    for m_attendee in m_attendees:
        attendees['attendees'].append({
            "type": "required",
            "emailAddress": {
                "address": m_attendee["email"],
            }
        })
    return attendees


def get_message_from_ecode(ecode):
    messages = [
        "",
        "I failed to lookup one or more of the users you mentioned. Please contact support.",
        "Looks like there is either a typo in a name or the user does not exist. Please fix and ask me again.",
        "Looks like one of the names you specified is ambiguous. Please add more characters to the name and try again.",
        "I failed to lookup one or more of the users you mentioned. Please contact support.",
        "I failed to lookup one or more of the users you mentioned. Please contact support.",
        "Looks like more than 1 of your colleagues have the same full name {0}. Kindly provide email addresses"
        " also for disambiguation",
        "Looks like more than 1 of your colleagues have the same first name {0}. Kindly provide full names"
        " also for disambiguation",
        "Looks like there is either a typo in the email address or the user does not exist. Please fix and ask me again.",
        "Looks like more than 1 of your colleagues have the same email address {0}. Kindly provide full names"
        " also for disambiguation",
        "Please Enter Valid Date",
        'Looks like the meeting time is in the past. Please provide a future date and try again',
        "Sorry there are no available time slots"
    ]
    return messages[ecode]


def find_meeting_times(names, duration, when, alt_id, config):
    results = list()
    ecode = 0
    cleaned_names = list()
    timeslots = list()
    error_message = ''
    meeting_date_tmp = ''
    # if 'today' in names:
    #     names = names.replace('today', '')
    #     meeting_date_tmp = 'today'
    # if 'tommorow' in names:
    #     names = names.replace('tommorow', '')
    #     meeting_date_tmp = 'tommorow'
    # if 'next week' in names:
    #     names = names.replace('next week', '')
    #     meeting_date_tmp = 'next week'
    # if not when:
    #     if meeting_date_tmp:
    #         when = meeting_date_tmp
    #     else:
    #         when = 'today'
    #
    # if not isinstance(names, list):
    #     names = names.strip()
    #     names = names.strip("[]")
    #     names = names.split(",")
    #     names = [x.strip() for x in names]

    for name in names:
        name = name.lower()
        names = re.split(" and | & ", name)
        for name in names:
            cleaned_names.append(name)
    if not names:
        return {'success': False, 'results': {'message': "I was unable to recognize attendees from your query :("}}
    attendee_details = find_attendee_information(alt_id,
                                                 cleaned_names,
                                                 config)

    attendee_details = check_attendee_is_manager(attendee_details, alt_id)
    if attendee_details['error_code'] != 0:
        name_resolved, error_message, is_resolved = generate_error_message(attendee_details)
        # send_custom_notification(alt_id, error_message)
        if not is_resolved:
            return {'success': False, 'results': {'message': error_message}}
        attendee_email_list = name_resolved
    else:
        attendee_email_list = []  # [{'name':hs,'email':hs@gmail.com},{'name':ng,'email':ng@gmail.com}]
        for attendee in attendee_details['attendees']:
            for k, v in attendee.items():
                attendee_email_list.append({'email': v['name_list'][0]['email']})

    # users_in_meeting = list()
    # for name in cleaned_names:
    #     try:
    #         org_id = get_org_id_from_alt_id(alt_id, config)
    #         if org_id > 0:
    #             m_results = get_user_details_from_name(org_id, name, config)
    #             if not m_results:
    #                 ecode = 2
    #                 break
    #             elif len(m_results) != 1:
    #                 # ecode = 3
    #                 for name in m_results:
    #                     error_names.append(name['search_email'])
    #                 ecode = 7
    #
    #                 email_freq = Counter(error_names)
    #                 for email, freq in email_freq.items():
    #                     if freq > 1:
    #                         ecode = 9
    #                 break
    #             users_in_meeting.extend(m_results)
    #         else:
    #             ecode = 1
    #     except Exception:
    #         ecode = 1
    #         traceback.print_exc()
    #         break

    # if ecode == 0:

    when = when.lower().strip()
    when = re.sub("\s+", "_", when)
    if when not in ["today", "tomorrow", "later_this_week", "next_week"]:
        start_end_dt = check_date(when)
        if start_end_dt is not None:
            local_tz = get_localzone()
            today = local_tz.localize(datetime.now())
            if today <= start_end_dt[1]:
                timeslots.append({
                    "type": "DATE",
                    "start": start_end_dt[0],
                    "end": start_end_dt[1],
                    "avlabl": True
                })
            else:
                timeslots.append({
                    "type": "DATE",
                    "avlabl": False
                })
                ecode = 11
        else:
            ecode = 10

    # if ecode == 0:
    #     if my_details:
    #         users_in_meeting.append(my_details)
    #
    #     else:
    #         ecode = 4

    if ecode == 0:
        try:
            my_details = get_user_data(alt_id, config)
            if not timeslots:
                timeslots = generate_timeslots(when)
            attendees = get_ms_formatted_attendee_list(attendee_email_list)
            for timeslot in timeslots:
                result = dict()
                result["dow"] = timeslot["type"]
                result["slots"] = list()
                if timeslot["avlabl"]:
                    o365_res = find_meeting_times_o365(
                        my_details,
                        config,
                        attendees,
                        get_time_constraint([(timeslot["start"], timeslot["end"])]),
                        "PT" + str(duration) + "M"
                    )
                    print json.dumps(o365_res, indent=4)
                    if o365_res:
                        if o365_res["meetingTimeSuggestions"]:
                            o365_res["meetingTimeSuggestions"] = sorted(
                                o365_res["meetingTimeSuggestions"],
                                key=lambda mtng_obj: date_util_parser.parse(
                                    mtng_obj["meetingTimeSlot"]["start"]["dateTime"]
                                ))
                            for mtgtms in o365_res["meetingTimeSuggestions"]:
                                localstart = convert_from_anytz_to_local(mtgtms["meetingTimeSlot"]["start"]["dateTime"],
                                                                         mtgtms["meetingTimeSlot"]["start"]["timeZone"])
                                localend = convert_from_anytz_to_local(mtgtms["meetingTimeSlot"]["end"]["dateTime"],
                                                                       mtgtms["meetingTimeSlot"]["end"]["timeZone"])
                                result["slots"].append((localstart, localend))
                            result["organizer_email"] = my_details["email"]
                            result["organizer_alt_id"] = my_details["alt_id"]
                            result["organizer_org_id"] = my_details["org_id"]
                            result["attendees"] = [x['emailAddress']['address'] for x in attendees["attendees"]]
                            result["refresh_token"] = my_details["refresh_token"]
                            result["access_token"] = my_details["access_token"]

                results.append(result)
        except:
            traceback.print_exc()
            ecode = 5

    if not result.get('slots'):
        ecode = 12
    if ecode == 0:
        return {'success': True, 'results': {'data': results, 'err_message': error_message}}
    else:
        message = get_message_from_ecode(ecode)
        return {'success': False, 'results': {'ecode': ecode, 'message': message}}


def create_full_name(employee_details):
    '''
    :param employee_details: dictionary of employee details
    :return: full name and email
    '''

    try:
        first_name = employee_details['first_name']
        middle_name = employee_details['middle_name']
        last_name = employee_details['last_name']
        full_name = re.sub(r'\s+', ' ', ' '.join([first_name, middle_name, last_name])).strip()
        return full_name
    except KeyError as ex:
        return ''


def find_attendee_information(alt_id,
                              attendees_list,
                              config):
    '''
    :param alt_id: user alt_id
    :param attendees_list: list of attendees names
    :param config: production config
    :return:
    '''

    users_in_meeting = {'organizer': {}, 'attendees': []}
    attendees_present = False

    try:
        # trying to get user details
        my_details = get_user_data(alt_id, config)
        full_name = create_full_name(my_details)
        my_org_id = my_details['org_id']  # getting user org
        my_email = my_details['email']  # getting user email
        if 'access_token' in my_details and 'refresh_token' in my_details:
            print('User with alt_id {0} found as {1}'.format(alt_id, full_name))
            # get info on attendees
            organizer_ecode = 0
            attendees_present = attendees_list is not None and len(attendees_list) > 0
            for attendee in attendees_list:
                print('attendee is {}'.format(attendee))
                try:
                    if is_single_word(attendee):
                        if '@' not in attendee:  # only first name is given
                            response = get_user_details_from_first_name(clean_string(attendee),
                                                                        config,
                                                                        my_org_id)
                            if not response:  # no candidate was found
                                attendee_ecode = 2
                            elif len(response) != 1:  # multiple candidates found from first name
                                attendee_ecode = 3
                            else:  # ony one guy found with that name
                                attendee_ecode = 0

                            users_in_meeting['attendees'].append({attendee: {
                                'name_list': [{'name': create_full_name(x), 'email': x['email'], 'alt_id': x['alt_id']}
                                              for x in response],
                                'name_count': len(response),
                                'error_message': name_ambiguation_error(attendee_ecode, alt_id),
                                'error_code': attendee_ecode}})
                        else:  # this is an email address, using it directly
                            # print(attendee)
                            # response = get_user_details_from_email(attendee,
                            #                                        config)
                            # if not response:  # no candidate was found
                            #     ecode = 8
                            # elif len(response) != 1:  # multiple candidates found from same email, should not happen
                            #     ecode = 9
                            # else:  # ony one guy found with that name
                            email_verified = verify_email(attendee)
                            if not email_verified:
                                attendee_ecode = 6
                            else:
                                attendee_ecode = 0
                            users_in_meeting['attendees'].append(
                                {attendee: {'name_list': [{'name': attendee, 'email': attendee}],
                                            'name_count': 1,
                                            'error_message': name_ambiguation_error(attendee_ecode, alt_id),
                                            'error_code': attendee_ecode}})
                    else:  # full name given
                        response = get_user_details_from_full_name(clean_string(attendee),
                                                                   config,
                                                                   my_org_id)
                        if not response:  # no candidate was found
                            attendee_ecode = 2
                        elif len(response) != 1:  # multiple candidates found
                            attendee_ecode = 4
                        else:  # ony one guy found with that name
                            attendee_ecode = 0
                        users_in_meeting['attendees'].append({attendee: {
                            'name_list': [{'name': create_full_name(x), 'email': x['email'], 'alt_id': x['alt_id']}
                                          for x in response],
                            'name_count': len(response),
                            'error_message': name_ambiguation_error(attendee_ecode, alt_id),
                            'error_code': attendee_ecode}})
                except Exception:
                    attendee_ecode = 1
                    users_in_meeting['attendees'].append({attendee: {'name_list': [],
                                                                     'name_count': 0,
                                                                     'error_message': name_ambiguation_error(
                                                                         attendee_ecode, alt_id),
                                                                     'error_code': attendee_ecode}})
                    print(traceback.print_exc())
        else:
            organizer_ecode = 7  # user has not registered
            print('non registered users', my_details['email'])
        users_in_meeting['organizer'] = {'name': full_name,
                                         'email': my_email,
                                         'error_message': name_ambiguation_error(organizer_ecode, alt_id),
                                         'error_code': organizer_ecode}

        overall_error = 0  # assuming all went well till here
        # add code for overall error
        if users_in_meeting['organizer']['error_code'] == 7:  # issue with organizer
            overall_error = 7
        if not users_in_meeting['attendees']:
            overall_error = 1
            # if attendees_present:
            #     overall_error = 1
            # else:
            #     overall_error = 13
        for attendee in users_in_meeting['attendees']:
            break_all = False
            for k, v in attendee.items():
                response_count = v['name_count']
                if response_count != 1:  # either a candidate was not found or found multiple times
                    overall_error = 8
                    break_all = True
                    break
            if break_all:
                break

    except:
        overall_error = 1

    users_in_meeting['error_code'] = overall_error
    users_in_meeting['error_message'] = name_ambiguation_error(overall_error, alt_id)
    return users_in_meeting


def name_ambiguation_error(ecode, user_id=None):
    '''
    :param ecode: error code
    :return: corresponding error message
    '''
    error_list = ['Attendee identified correctly',  # all good - 0

                  "Oops! This service is currently unavailable at the moment due to some internal error or"
                  " because we are unable to reach the Office 365 servers."
                  " Please try again later or contact support. ",  # some internal issue - 1

                  "Oops! Looks like this user does not exist. Please remove this user and try again.",

                  # user does not exist- 2
                  "Please use one of these full names and try again.",  # abmiguation on first name - 3

                  "Please use one of these email address and try again.",  # ambiguation on full names - 4

                  "I cannot proceed with the information since more than one user exists with the same email ({0})",

                  # ambiguous mail - 5
                  "Looks like there is either a typo in the email address or the user does not exist. Please fix and ask me again.",

                  # wrong email - 6
                  "I'm glad you would want to use this feature. "
                  "However, you’ll have to integrate me with your Outlook account first and make me your productivity assistant. "
                  "{}".format(get_office_365_integration_embed_link(user_id, "Click here to link Outlook.")),  # register first - 7

                  "I could not resolve the following attendee information:<br/>",  # error message - 8

                  'I could not understand the meeting timings. Please try again',  # meeting time incorrct - 9

                  'Looks like the meeting time is in the past. Please provide a future date and try again',  # 10

                  'L1 manager located',  # 11

                  'L2 Manager located',  # 12

                  'I could not identify any attendees from your query :('  # 13
                  # future meeting time - 10
                  ]

    return error_list[ecode]


def generate_error_message(attendee_detail):
    '''
    :param attendee_detail: response of find_attendee_information function
    :return: name_resolved_list : list of dicts of emailids which are resolved,
    error/message string,
    is_resolved : True if all attendees are resolved else false
    '''

    if attendee_detail['error_code'] in [1, 7]:  # not a disambiguation message, return as is
        return [], attendee_detail['error_message'], False

    is_resolved = False
    name_resolved_list = []
    # is_l1_l2 = False
    for attendee in attendee_detail['attendees']:
        attendee_name = list(attendee.keys())[0]  ## supplied raw name of attendee
        attendee_value = list(attendee.values())[0]

        # if only  a single candidate with this name
        if attendee_value['name_count'] == 1:
            name_resolved_list.append({'email': attendee_value['name_list'][0]['email']})
            continue
        # no candidate with this name
        elif attendee_value['name_count'] == 0:
            # is_resolved = False
            pass
        else:
            # iterating over all probable people of this name
            for probable_attendee in attendee_value['name_list']:
                if probable_attendee.get('is_l1', False):
                    name_resolved_list.append({'email': probable_attendee['email']})
                    # is_l1_l2 = True
                    break  # we do not need to check further records - we assume that this is our guy
                elif probable_attendee.get('is_l2', False):
                    # is_l1_l2 = True
                    name_resolved_list.append({'email': probable_attendee['email']})
                    break  # we do not need to check further records - we assume that this is our guy

    total_attendees = len(attendee_detail['attendees'])
    if len(name_resolved_list) == total_attendees:
        is_resolved = True
    elif len(name_resolved_list) < total_attendees:
        is_resolved = False
    elif len(name_resolved_list) > total_attendees:
        raise Exception('length of resolved list greater than number of intended attendees')

    # constructing error message
    # for attendee in attendee_detail['attendees']:
    #     attendee_name = list(attendee.keys())[0]
    #     attendee_value = list(attendee.values())[0]
    #     if attendee_value['name_count']:
    #         for attendee in attendee_value['name_list']:
    #             name_resolved_list.append(attendee_value['name_list'][0])

    error_message = attendee_detail['error_message'] + '<br/>'

    for attendee in attendee_detail['attendees']:
        attendee_name = list(attendee.keys())[0]
        attendee_value = list(attendee.values())[0]
        if attendee_value['error_code'] == 2:  # if user is not found
            error_message += '> '
            error_message += attendee_name + ' - ' + attendee_value['error_message'] + '<br/>'

        elif attendee_value['error_code'] == 3:  # if first name is ambiguous
            error_message += '> '
            for ambiguity in attendee_value['name_list']:
                error_message += ambiguity['name'] + ', '
            error_message = error_message[:-2]  # removing last ', '
            error_message += ' - ' + attendee_value['error_message'] + '<br/>'

        elif attendee_value['error_code'] == 4:  # if full name is ambiguous
            error_message += '> '
            for ambiguity in attendee_value['name_list']:
                error_message += ambiguity['name'] + ', '
            error_message = error_message[:-2]  # removing last ', '
            error_message += ' - ' + attendee_value['error_message'] + '<br/>'

        elif attendee_value['error_code'] == 5:  # email ambiguous
            error_message += '> '
            for ambiguity in attendee_value['name_list']:
                error_message += ambiguity['email'] + ', '
            error_message = error_message[:-2]  # removing last ', '
            error_message += ' - ' + attendee_value['error_message'] + '<br/>'

        elif attendee_value['error_code'] == 6:  # email not good
            error_message += '> '
            error_message += attendee_name + ' - ' + attendee_value['error_message'] + '<br/>'

        elif attendee_value['error_code'] == 11:  # disambiguation resolved on l1
            for ambiguity in attendee_value['name_list']:
                if ambiguity.get('is_l1', False):
                    l1_manager = ambiguity
                    break
            error_message += '> '
            # error_message += attendee_name + ' - ' + attendee_value['error_message'] + '<br/>'
            error_message += 'There were multiple attendees with name ' + attendee_name + '. However I have chosen ' + \
                             l1_manager['email'] + ' for the meeting since it is your L1 manager. <br/>'

        elif attendee_value['error_code'] == 12:  # disambiguation resolved on l1
            for ambiguity in attendee_value['name_list']:
                if ambiguity.get('is_l2', False):
                    l2_manager = ambiguity
                    break
            error_message += '> '
            # error_message += attendee_name + ' - ' + attendee_value['error_message'] + '<br/>'
            error_message += 'There were multiple attendees with name ' + attendee_name + '. However I have chosen ' + \
                             l2_manager['email'] + ' for the meeting since it is your L1 manager. <br/>'

    return name_resolved_list, error_message, is_resolved


def generate_error_message_2(attendee_detail):
    '''
    :param attendee_detail: response of find_attendee_information function. Using this new function as per changed specs
    :return: error message
    '''
    orig_attendee_detail = attendee_detail
    attendee_detail = attendee_detail["attendees"]
    full_name_ambiguatiton_list = []
    email_incorrect_list = []
    first_name_ambiguation_list = []
    name_not_found_list = []
    internal_error_list = []
    name_resolved_list = []
    is_l1_l2_list = []
    for attendee in attendee_detail:
        attendee_name = list(attendee.keys())[0]
        attendee_value = list(attendee.values())[0]
        for attendee in attendee_value['name_list']:
            if attendee.get('is_l1'):
                name_resolved_list.append(attendee_value['name_list'][0])
                is_l1_l2_list.append(attendee_value['name_list'][0])
                break
            elif attendee.get('is_l2'):
                name_resolved_list.append(attendee_value['name_list'][0])
                is_l1_l2_list.append(attendee_value['name_list'][0])
        if attendee_value['error_code'] == 2:  # if user is not found
            name_not_found_list.append(attendee_name)
        elif attendee_value['error_code'] == 3:
            first_name_ambiguation_list.append(attendee_name)
        elif attendee_value['error_code'] == 6:
            email_incorrect_list.append(attendee_name)
        elif attendee_value['error_code'] == 4:
            full_name_ambiguatiton_list.append(attendee_name)
        elif attendee_value['error_code'] == 1:
            internal_error_list.append(attendee_name)
        elif attendee_value['error_code'] == 0:
            name_resolved_list.append(attendee_value['name_list'][0])

    error_message = ""

    # if len(full_name_ambiguatiton_list+email_incorrect_list+first_name_ambiguation_list\
    #        +name_not_found_list+internal_error_list) > 0:
    # error_message += ' However, '  #problem with at least one user
    if name_not_found_list:
        error_message += ' User(s) ' + convert_list_to_string(name_not_found_list) + ' do not exist.'
    if full_name_ambiguatiton_list:
        error_message += ' Multiple users exist with the name(s) ' + convert_list_to_string(
            full_name_ambiguatiton_list) + '.'
    if first_name_ambiguation_list:
        error_message += ' Multiple users exist with the name(s) ' + convert_list_to_string(
            first_name_ambiguation_list) + '.'
    if email_incorrect_list:
        error_message += ' The email(s) ' + convert_list_to_string(email_incorrect_list) + ' is invalid.'
    if internal_error_list:
        error_message += ' Some internal error occured while processing user(s) ' + convert_list_to_string(
            internal_error_list) + '.'
    if is_l1_l2_list:
        l1_error, l2_error, err = '', '', ''
        l1, l2 = False, False
        for attendee in is_l1_l2_list:
            if attendee.get('is_l1'):
                l1_error += attendee.get('name') + " (L1 Manager)"
                l1 = True
            if attendee.get('is_l2'):
                l2_error += attendee.get('name') + " (L2 Manager)"
                l2 = True
            if l1 and l2:
                err = l1_error + " and " + l2_error
            else:
                if l1:
                    err = l1_error
                else:
                    err = l2_error
        error_message += err + ' is choosen as attendee(s).'

    if len(name_resolved_list) == 0 and error_message == "":
        error_message = "I could not identify any attendees from your query :("

    return name_resolved_list, error_message


def create_meeting_title(attendee_list, meeting_duration_min):
    '''
    :param attendee_list: list of attendee names
    :param meeting_duration_min: meeting duration in mins
    :return: meeting title string
    '''

    if meeting_duration_min >= 60:
        meeting_duration_hour = meeting_duration_min / 60
        meeting_duration_min %= 60
    else:
        meeting_duration_hour = 0

    if meeting_duration_hour > 0 and meeting_duration_min > 0:
        duration_string = str(meeting_duration_hour) + ' Hr and ' + str(meeting_duration_min) + ' Minutes '
    elif meeting_duration_hour > 0:
        duration_string = str(meeting_duration_hour) + ' Hr '
    elif meeting_duration_min > 0:
        duration_string = str(meeting_duration_min) + ' Minutes '

    return duration_string + 'meeting created using Jinie | ' + str(len(attendee_list)) + ' Participant(s)'


def check_attendee_is_manager(attendee_details, user_alt_id):
    l1_id, l2_id = fetch_l1_l2_id(user_alt_id)
    # found_ordered_ids=[]                #List will be in order l1 will come first
    for attendee_dict in attendee_details['attendees']:
        attendee_info = attendee_dict.values()
        count = attendee_info[0]['name_count']
        if count > 1:
            for attendee in attendee_info[0]['name_list']:
                if l1_id == attendee['alt_id']:
                    attendee.update({'is_l1': True})
                    attendee.update({'is_l2': False})
                    attendee_info[0]['is_l1_found'] = True
                    attendee_info[0]['is_l2_found'] = False
                    attendee_info[0]['error_code'] = 11
                    attendee_info[0]['error_message'] = name_ambiguation_error(11, user_alt_id)
                elif l2_id == attendee['alt_id']:
                    attendee.update({'is_l2': True})
                    attendee.update({'is_l1': False})
                    attendee_info[0]['is_l2_found'] = True
                    attendee_info[0]['is_l1_found'] = False
                    attendee_info[0]['error_code'] = 12
                    attendee_info[0]['error_message'] = name_ambiguation_error(12, user_alt_id)
                else:
                    attendee.update({'is_l1': False})
                    attendee.update({'is_l2': False})
                    attendee_info[0]['is_l1_found'] = False
                    attendee_info[0]['is_l2_found'] = False

    return attendee_details


def schedule_meeting(alt_id,
                     attendees_list,
                     meeting_start_time,
                     config,
                     meeting_date,
                     meeting_duration_min=30,
                     subject=None,
                     location='Office',
                     mail_body='Blocking your calender for a meeting invite through Jinie',
                     create_meeting_link="no"
                     ):
    meeting_date_tmp = ''
    # if 'today' in attendees_list:
    #     attendees_list = attendees_list.replace('today', '')
    #     meeting_date_tmp = 'today'
    # if 'tommorow' in attendees_list:
    #     attendees_list = attendees_list.replace('tommorow', '')
    #     meeting_date_tmp = 'tommorow'
    # if not meeting_date:
    #     if meeting_date_tmp:
    #         meeting_date = meeting_date_tmp
    #     else:
    #         meeting_date = 'today'

    # cachedStopWords = stopwords.words("english")
    # attendees_list = re.split(",| ", query)
    # attendees_list = [word for word in attendees_list if word not in cachedStopWords]
    # attendees_list = [word for word in attendees_list if len(word) >= 3]

    # attendees_list = re.split(',| and | & ', attendees_list)
    # attendees_list = [x.strip().lower() for x in attendees_list if
    #                   x is not None and x.strip() != '']  # keeping valid syntactic namees only

    attendee_details = find_attendee_information(alt_id,
                                                 attendees_list,
                                                 config)
    # attendee_details = check_attendee_is_manager(attendee_details, alt_id)
    for attendee in attendee_details['attendees']:
        attendee_name = list(attendee.keys())[0]
        attendee_value = list(attendee.values())[0]
        if attendee_value['name_count'] == 0:
            del attendee[attendee_name]
    attendee_details["attendees"] = [i for i in attendee_details['attendees'] if len(i)]
    # if attendee_details['error_code'] != 0:
    attendee_dict_list, error_message = generate_error_message_2(attendee_details)
    # send_custom_notification(alt_id, error_message)
    # if not attendee_dict_list:  # all are errorneous
    #     return error_message
    # else:
    if True:  # Very bad hack for now for testing purposes. TODO: REMOVE!!!
        # attendee_dict_list = []
        # for attendee in attendee_details['attendees']:
        #     for k, v in attendee.items():
        #         attendee_dict_list += v['name_list']
        user_details = get_user_details_by_alt_id(alt_id, config)
        if not subject:
            try:
                meeting_duration_min = int(meeting_duration_min)
            except Exception:
                meeting_duration_min = 60
            name = create_full_name(user_details)
            # subject = create_meeting_title(attendee_dict_list, meeting_duration_min)
            subject = "Meeting with {}".format(name)
            mail_body = "Meeting with {}##MSTEAMS_LINK##<br/><br/><a href='https://zippi.co/'>Powered by Zippi</a>".format(
                name)
        try:
            # TODO: Test it
            # meeting_start_time = dateutil.parser.parse(meeting_start_time)
            # meeting_start_time = meeting_start_time.time()
            # meeting_start_time = meeting_start_time.strftime("%I:%M %p")
            #
            # meeting_date=dateutil.parser.parse(meeting_date)
            # meeting_date=meeting_date.date()
            # meeting_date=meeting_date.strftime("%d-%m-%Y")
            meeting_start_time_utc, meeting_end_time_utc = parse_meeting_timings(meeting_date,
                                                                                 meeting_start_time,
                                                                                 meeting_duration_min,
                                                                                 user_details["timezone"])
            if meeting_end_time_utc is None:
                ecode = 10
                return name_ambiguation_error(ecode, alt_id)

        except Exception as ex:
            ecode = 9
            # send_custom_notification(alt_id, name_ambiguation_error(ecode))
            return name_ambiguation_error(ecode, alt_id)

        meeting_link = None

        if create_meeting_link.lower().strip() == "yes":
            meeting_link = create_msteams_meeting(alt_id, subject, meeting_start_time_utc, meeting_end_time_utc, True)
            if meeting_link:
                mail_body = mail_body.replace("##MSTEAMS_LINK##",
                                              "<br/><br/>Meeting Link: <a href='{}'>{}</a>".format(meeting_link,
                                                                                                   meeting_link))

        mail_body = mail_body.replace("##MSTEAMS_LINK##", "")

        user_details = get_user_details_by_alt_id(alt_id, config)
        token = refresh_access_token(user_details['refresh_token'], config, user_details['scopes'])
        token_str = token['access_token']

        response = o365_meeting_handler(config=config,
                                        attendees_list=attendee_dict_list,
                                        start_time=meeting_start_time_utc,
                                        end_time=meeting_end_time_utc,
                                        token=token_str,
                                        subject=subject,
                                        location=location,
                                        mail_body=mail_body,
                                        onlineMeetingProvider=meeting_link)

        # print(response)
        if response is None:
            error_message = name_ambiguation_error(1, alt_id)
            # send_custom_notification(alt_id, error_message)
            return error_message
        else:
            meeting_id = response['id']
            # print(meeting_id)
            # send_custom_notification(alt_id, 'Meeting scheduled successfully <br/>'
            #                                  'To cancel meeting <a href="https://www.google.com"> Click here </a>')
            param_dict = {'meeting_id': str(meeting_id),
                          'altid': str(alt_id)}
            param_encode = base64.urlsafe_b64encode(json_dumps_minify(param_dict))  # give it a dictionary
            cipher_params_get = ps_encrypt(param_encode)

            # cancel_meeting_url = config.SUBDOMAIN + '/meeting/cancel?meetingid=' + str(meeting_id) + '&altid='\
            #                      + str(alt_id)
            cancel_meeting_url = config.SUBDOMAIN + '/meeting/cancel?params={}'.format(cipher_params_get)
            open_meeting_url = response['webLink']
            response_message = 'Meeting creation was sucessful.<br/>'
            has_errors = False
            is_error_for_manager = False
            if error_message:
                for attendees in attendee_dict_list:
                    if attendees.get('is_l1') or attendees.get('is_l2'):
                        is_error_for_manager = True
                if is_error_for_manager:
                    has_errors = True
                    if len(attendee_dict_list) < len(attendees_list):
                        response_message += "<br/> " + error_message + 'You can always add remaining users to the meeting manually.<br/>'
                    else:
                        response_message += error_message
                else:
                    has_errors = True
                    response_message += 'However, ' + error_message + ' The meeting has been created without them.' \
                                                                      ' You can always add these users to the meeting manually.<br/>'

            # response_message += '<a href="{}">Open in Outlook</a>'.format(open_meeting_url)  #Todo : remove when sendong JSON
            # response_message += '<a href="{}">Cancel Meeting</a>'.format(cancel_meeting_url) #Todo : remove when sendong JSON
            return {'body': response_message,
                    'title': subject,
                    'start_time': meeting_start_time_utc,
                    'end_time': meeting_end_time_utc,
                    'location': location,
                    'attendees': attendee_dict_list,
                    'outlook_link': open_meeting_url,
                    'cancel_link': cancel_meeting_url, "has_errors": has_errors}  # response for jinie
            # return response_message
        # return None


def cancel_meeting(alt_id,
                   meeting_id,
                   config):
    '''
    :param alt_id: alt_id of user
    :param meeting_id: meeting_id to be cancelled
    :param config:
    :return:
    '''
    user_details = get_user_details_by_alt_id(alt_id, config)
    token = refresh_access_token(user_details['refresh_token'], config, user_details['scopes'])
    token_str = token['access_token']

    response = o365_cancel_meeting_handler(config,
                                           token_str,
                                           meeting_id)
    return response
    # if response:
    #     send_custom_notification(alt_id, 'Meeting Cancelled successfully')
    # else:
    #     send_custom_notification(alt_id, 'Meeting could not be cancelled. Please try again or use Outlook to cancel.')


def fetch_l1_l2_id(alt_user_id):
    crsr = sql_helpers.connect_to_mssql_server('TalentPact',
                                               config.MSSQL_CREDENTIALS)
    l1_usr_id = -1
    l2_usr_id = -1
    if crsr is not None:
        # start = time.time()
        query = "select OrganizationID, L1ManagerID,L2ManagerID from dbo.HrEmployee where UserID=%s"
        crsr.execute(query, (alt_user_id))
        org_id, l1_emp_id, l2_emp_id = crsr.fetchall()[0]
        l1_usr_id = get_user_id_from_emp_id(l1_emp_id, org_id, config)
        l2_usr_id = get_user_id_from_emp_id(l2_emp_id, org_id, config)
    return l1_usr_id, l2_usr_id


if __name__ == "__main__":
    #     from simplex_communications import config
    #     import json
    #
    #     print json.dumps(process_find_meeting_times(["Aakash roY and Pankaj Bansal", "Amber Nigam"], 2205395, config),
    #                      indent=4, default=unicode)
    #     print(clean_string('aakash.roy@peoplestrong.com'))
    #     x = find_attendee_information(2205395,['aakash.roy@peoplestrong.com','nishant garg'],config)
    #     print(x)
    #     print get_user_details_from_email('aakash.roy@peoplestrong.com',config)
    # x = schedule_meeting(2653216,  # 2012224
    #                      'aabhas.tonwer@peoplestrong.com and aakash roy',
    #                      '5 pm',
    #                      config)
    # print fetch_l1_l2_id(2653216)[0]
    # x = find_meeting_times('nishant.garg@peoplestrong.com and aakash roy', 30,
    #                        '15/9/2019', 2205395, config)
    # x = schedule_meeting(2653216,  # 2012224
    #                      'aabhas.tonwer@peoplestrong.com and aakash',
    #                      '5 pm',
    #                      config)

    print get_message_from_ecode(12)
