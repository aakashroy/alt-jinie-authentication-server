import re
import string
from collections import OrderedDict

import requests


def handle_performance_query(intent, alt_user_id, additional_parameters):
    in_limbo_response = {}
    if intent == "QILO_REPORTEE_LIST":
        name_dict = OrderedDict({})

        is_filtered = False
        org_id = additional_parameters.get("org_id", -1)
        email_id = additional_parameters.get("email_id", "")

        user_query = str(additional_parameters.get("query", "").lower())
        nosp_user_query = re.sub(r"\s+", "", user_query).lower()

        if "myokr" in nosp_user_query or "mykr" in nosp_user_query:
            in_limbo_response = requests.post(
                "https://messenger.peoplestrong.com/XmppJinie/getNotificationPayloadFromDataSources",
                headers={
                    "content-type": "application/json",
                    "authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
                },
                json={
                    "user_id": alt_user_id,
                    "params": {
                        "org_id": org_id,
                        "email": email_id
                    },
                    "intent": "JINIE_QILO_MILESTONE_LIST_BY_MAIL"
                }).json()
            print(in_limbo_response)
        else:
            user_query = user_query.replace("'s", "")
            user_query = user_query.translate(None, string.punctuation)
            user_query = re.sub(r"\b(?:me|my|okrs|okr|krs|kr|team|show|and)\b", "", user_query)
            user_query = re.sub(r"(?:\s+)", " ", user_query).strip()
            possible_names = user_query.split()
            quilo_response = requests.post(
                "https://messenger.peoplestrong.com/XmppJinie/getAPIPayloadFromDataSources",
                headers={
                    "content-type": "application/json",
                    "authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
                },
                json={
                    "user_id": alt_user_id,
                    "params": {"payload": {"org_id": org_id,
                                           "email": email_id},
                               "context": "QILO_MILESTONE_LIST_BY_MAIL"},
                    "intent": "JINIE_QILO_REPORTEE_LIST"
                }).json()
            if quilo_response:
                if quilo_response["result"][0]["design"]["dataArray"]:
                    names_from_qilo_response = [(x["title"].lower(), x) for x in
                                                quilo_response["result"][0]["design"]["dataArray"]]
                    result_dict = {}
                    for possible_name in possible_names:
                        for name_obj in names_from_qilo_response:
                            if possible_name in name_obj[0]:
                                result_dict[name_obj[1]["entity1"]] = name_obj[1]
                                name_dict[name_obj[1]["title"]] = None

                    if result_dict:
                        is_filtered = True
                        if len(result_dict.keys()) == 1:
                            in_limbo_response = requests.post(
                                "https://messenger.peoplestrong.com/XmppJinie/getNotificationPayloadFromDataSources",
                                headers={
                                    "content-type": "application/json",
                                    "authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
                                },
                                json={
                                    "user_id": alt_user_id,
                                    "params": {
                                        "org_id": org_id,
                                        "email": email_id
                                    },
                                    "intent": "JINIE_QILO_MILESTONE_LIST_BY_MAIL"
                                }).json()
                        else:
                            modified_response = {
                                "result": [
                                    {
                                        "design": quilo_response["result"][0]["design"]
                                    }
                                ]
                            }
                            modified_response["result"][0]["design"]["dataArray"] = list()
                            for _, result in result_dict.items():
                                modified_response["result"][0]["design"]["dataArray"].append(result)
                            in_limbo_response = modified_response
                    else:
                        in_limbo_response = quilo_response
                else:
                    in_limbo_response[
                        "result"] = "Sorry, there are no reportees under you, you can try typing show my okrs"
        if not in_limbo_response:
            in_limbo_response["result"] = {
                "design": {
                    "selectionOptions": [],
                    "label": "OOPs Failed",
                    "type": "TEXT",
                    "selectedValue": [],
                }
            }
        elif is_filtered:
            name = name_dict.keys()[0]
            tot_names = len(name_dict.keys())
            label = "Showing OKRs for {}{}"
            if tot_names == 1:
                label = label.format(name, ":")
            else:
                label = label.format(name, ", and {} more members".format(tot_names))
            in_limbo_response['result'][0]['design']['label'] = label
            # return {
    #            "messageCodeTO": {
    #                "status": "SUCCESS",
    #                "code": "EC200",
    #                "displayMsg": in_limbo_response["result"]
    #            }
    #        }, ""
    return in_limbo_response["result"], ""
