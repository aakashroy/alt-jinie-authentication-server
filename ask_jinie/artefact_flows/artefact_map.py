ARTEFACT_PROPERTIES = {

    ## PEOPLESTRONG

    ("sales", "sales pitches", "zippi"): {
        "file_path": "artefacts/19/sales_salespitch_zippi.pdf",
        "document_friendly_name": "Zippi sales pitch",
        "file_screen_name": "zippi_sales_pitch.pdf",
        "intent_mask": [],
        "org_id_mask": [19, 3, 398],
        "zippi_group_mask": []
    },
    ("sales", "sales pitches", "recruit"): {
        "file_path": "artefacts/19/alt_recruit_pitch.pdf",
        "document_friendly_name": "Alt-Recruit sales pitch",
        "file_screen_name": "alt_recruit_pitch.pdf",
        "intent_mask": [],
        "org_id_mask": [19, 3, 398],
        "zippi_group_mask": []
    },
    ("document", "alt-recruit", "case studies"): {
        "file_path": "artefacts/19/alt_recruit_case_study.pdf",
        "document_friendly_name": "Alt Recruit Case Studies",
        "file_screen_name": "alt_recruit_case_study.pdf",
        "intent_mask": [],
        "org_id_mask": [19, 3, 398],
        "zippi_group_mask": []
    },

    ("document", "hcm", "case studies"): {
        "file_path": "artefacts/19/hcm_case_study.pdf",
        "document_friendly_name": "HCM Case Studies",
        "file_screen_name": "hcm_case_study.pdf",
        "intent_mask": [],
        "org_id_mask": [19, 3, 398],
        "zippi_group_mask": []
    },

    ("incentive", "policy"): {
        'file_path': "artefacts/19/incentive_policy.pdf",
        "document_friendly_name": "Incentive Policy",
        "file_screen_name": "incentive_policy.pdf",
        "intent_mask": [],
        "org_id_mask": [19, 3, 398],
        "zippi_group_mask": []
    },

    ("rate card", "alt-analytics"): {
        "file_path": "artefacts/19/alt_analytics_rate_card.pdf",
        "document_friendly_name": "Alt-Analytics Rate Card",
        "file_screen_name": "alt_analytics_rate_card.pdf",
        "intent_mask": [],
        "org_id_mask": [19],
        "zippi_group_mask": ["672941547111118881"]
    },

    ("rate", "all the product"): {
        "file_path": "artefacts/19/alt_all_rate_card.pdf",
        "document_friendly_name": "All Alt Products Rate Card",
        "file_screen_name": "alt_all_rate_card.pdf",
        "intent_mask": [],
        "org_id_mask": [19],
        "zippi_group_mask": ["672941547111118881"]
    },

    ##VMART

    ("it policy & procedures",): {
        "file_path": "artefacts/313/IT_Policy_Procedures.pdf",
        "document_friendly_name": "IT Policy Procedures",
        "file_screen_name": "IT_Policy_Procedures.pdf",
        "intent_mask": [],
        "org_id_mask": [313],
        "zippi_group_mask": []
    },

    ("it training",): {
        "file_path": "artefacts/313/IT_Training.pdf",
        "document_friendly_name": "IT Training",
        "file_screen_name": "IT_Training.pdf",
        "intent_mask": [],
        "org_id_mask": [313],
        "zippi_group_mask": []
    },

    ("spring & holi store communication brief",): {
        "file_path": "artefacts/313/Spring_Summer_Holi_STORE_COMMUNICATION_BRIEF.pdf",
        "document_friendly_name": "Spring Summer Holi store communication breif",
        "file_screen_name": "Spring_Summer_Holi_STORE_COMMUNICATION_BRIEF.pdf",
        "intent_mask": [],
        "org_id_mask": [313],
        "zippi_group_mask": []
    },

    ("ss 20 launch mpm plan",): {
        "file_path": "artefacts/313/SS_20_Launch_MPM_Plan.pdf",
        "document_friendly_name": "SS 20 Launch MPM Plan",
        "file_screen_name": "SS_20_Launch_MPM_Plan.pdf",
        "intent_mask": [],
        "org_id_mask": [313],
        "zippi_group_mask": []
    },

    ##MAHINDRA

    ("out reach",): {
        "file_path": "artefacts/398/outreach.pdf",
        "document_friendly_name": "outreach program document",
        "file_screen_name": "outreach.pdf",
        "intent_mask": [],
        "org_id_mask": [398],
        "zippi_group_mask": []
    },
}
