import base64
import json

import requests

from ask_jinie.artefact_flows import intent_mapping, artefact_map


def artefact_flow_handler(intent, alt_user_id, additional_parameters):
    print("Entered: artefact_flow_handler")
    ERROR_MESSAGE = "Sorry, I was unable to retrieve your document. Possible reasons could be that the document is not available or you do not have the permissions to view it."
    if intent in intent_mapping.INTENT_MAP.keys():
        if "org_id" in additional_parameters and "query" in additional_parameters and additional_parameters["query"]:
            print("Present org id and query")
            org_id = int(additional_parameters["org_id"])
            query = additional_parameters["query"].lower()
            print("Received query: {}".format(query))

            for keywords, artefact_properties in artefact_map.ARTEFACT_PROPERTIES.items():
                print("Checking: ", keywords)
                print(json.dumps(artefact_properties))
                intent_mask = artefact_properties["intent_mask"]
                org_id_mask = artefact_properties["org_id_mask"]
                zippi_group_mask = set(artefact_properties["zippi_group_mask"])

                if (len(intent_mask) == 0 or intent in intent_mask) \
                        and (len(org_id_mask) == 0 or org_id in org_id_mask):
                    kw_list = list(keywords)
                    present = len(kw_list) > 0
                    for kw in kw_list:
                        present = present & (kw in query)
                        if not present:
                            break
                    if present:
                        group_allowed = len(zippi_group_mask) == 0
                        if not group_allowed:
                            response = requests.post(
                                "https://messenger.peoplestrong.com/XmppJinie/internal/getUserGroups", json={
                                    "userId": alt_user_id
                                }).json()
                            if zippi_group_mask.intersection(set(response["responseData"].keys())):
                                group_allowed = True
                        if group_allowed:
                            file_path = artefact_properties["file_path"]
                            file_screen_name = artefact_properties["file_screen_name"]
                            document_friendly_name = artefact_properties["document_friendly_name"]

                            print("Sending: {}, {}, {}".format(file_path, file_screen_name, document_friendly_name))
                            with open(file_path, "rb") as generic_file:
                                file_byte_array = base64.b64encode(generic_file.read())
                                print("Sending {} bytes . . .".format(len(file_byte_array)))
                                return {

                                    "messageCode": {
                                        "code": "EC200",
                                        "message": "Hi, ##_USER_FULL_NAME_## ! Here's the {} you requested:".format(
                                            document_friendly_name),
                                        "description": "Hi, ##_USER_FULL_NAME_##! Here's the {} you requested:".format(
                                            document_friendly_name),
                                        "validationKeys": None,
                                        "forcedAppUpdate": False
                                    },
                                    "responseData": [
                                        {
                                            "message": "Hi, ##_USER_FULL_NAME_##! Here's the {} you requested:".format(
                                                document_friendly_name),
                                            "fileByteArray": file_byte_array,
                                            "fileName": file_screen_name
                                        }
                                    ],
                                    "response": None,
                                    "appUpdateResponseTO": None,
                                    "totalRecords": 0,
                                    "responseMap": None
                                }
                        break
        else:
            print("Absent org id and query")
    else:
        print("artefact: not my intent")
    print("Falling back")
    return {

        "messageCode": {
            "code": "EC200",
            "message": "Hi, ##_USER_FULL_NAME_##! {}".format(
                ERROR_MESSAGE),
            "description": "Hi, ##_USER_FULL_NAME_##! {}".format(
                ERROR_MESSAGE),
            "validationKeys": None,
            "forcedAppUpdate": False
        },
        "responseData": [
            {
                "message": "Hi, ##_USER_FULL_NAME_##! {}".format(
                    ERROR_MESSAGE),
                "fileByteArray": "",
                "fileName": ""
            }
        ],
        "response": None,
        "appUpdateResponseTO": None,
        "totalRecords": 0,
        "responseMap": None
    }
