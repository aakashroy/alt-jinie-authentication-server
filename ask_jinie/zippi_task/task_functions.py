import base64
import traceback
import urllib

import requests
import tinyurl

import config
from common.mongo_helpers import update_approved_rejected_email_details, \
    update_task_email_details
from common.o365_helpers import get_user_data, reply_all


def handle_approve_reject_createTask(intent, additional_parameters):
    if intent == "ML_CREATE_TASK":
        return create_task(additional_parameters)
    elif intent == "ML_APPROVE":
        return approve_email(additional_parameters)
    elif intent == "ML_REJECT":
        return reject_email(additional_parameters)


def create_task(additional_parameters):
    creator_name = additional_parameters["task_creator_name"],
    expiryTime = additional_parameters["expiryTime"],
    assignee_name = additional_parameters["assignee_name"],
    creator_userid = additional_parameters["task_creator_alt_id"],
    title = additional_parameters["title_task"],
    assignee_userid = additional_parameters["assignee_userid"],
    org_id = additional_parameters["org_id"]
    remark = additional_parameters["remark"]
    creator_name = ''.join(creator_name)
    creator_userid = ''.join(creator_userid)
    expiryTime = ''.join(expiryTime)
    title = ''.join(title)
    assignee_userid = ''.join(assignee_userid)
    assignee_name = ''.join(assignee_name)
    remark = ''.join(remark)
    user_data = get_user_data(assignee_userid, config)
    o365_email_resource_id = additional_parameters["mail_id"]
    display_msg = "You have already taken an action on this task"
    #title = title.decode("utf-8")
    #remark = remark.decode("utf-8")
    if user_data and o365_email_resource_id not in user_data.get("approved_rejected_task_mail_ids", []):

        approver_link = config.o365_EMAIL_APPROVE_ACTION_ENDPOINT.format(o365_email_resource_id, assignee_userid,
                                                                         additional_parameters["approver_email"],
                                                                         title)
        reject_link = config.o365_EMAIL_REJECT_ACTION_ENDPOINT.format(o365_email_resource_id, assignee_userid,
                                                                      additional_parameters["approver_email"],
                                                                      title)
        read_link = "https://outlook.office.com/mail/deeplink/read/{}?version=2020052401.07&popoutv2=1&leanbootstrap=1".format(
            urllib.quote(o365_email_resource_id))
        try:
            task_descrption = base64.urlsafe_b64decode(str(remark)) \
                              + "\n\nREAD EMAIL: " + tinyurl.create_one(read_link) \
                              + "\n\nAPPROVE HERE: " + tinyurl.create_one(approver_link) \
                              + "\n\nREJECT HERE: " + tinyurl.create_one(reject_link)

        except Exception:
            task_descrption = base64.urlsafe_b64decode(str(remark))

        display_msg = "<b>Sorry, I was unable to create a task for you.</b>"
        try:
            response = requests.post("https://hrms.peoplestrong.com/XmppJinie/taskCreation/{}".format(org_id),
                                     json={
                                         "cn": creator_name,
                                         "fid": creator_userid,
                                         "tn": base64.urlsafe_b64decode(str(title)),
                                         "tai": assignee_userid,
                                         "ta": assignee_name,
                                         "expiryTime": expiryTime,
                                         "rm": task_descrption
                                     }).json()
            if "message" in response and "code" in response["message"]:
                display_msg = creator_name + " has assigned you a task."

                task_email_list = user_data.get("task_mail_ids", [])
                task_email_list.append(o365_email_resource_id)
                task_email_list = list(set(task_email_list))
                approved_rejected_task_email_list = user_data.get("approved_rejected_task_mail_ids", [])
                approved_rejected_task_email_list.append(o365_email_resource_id)
                approved_rejected_task_email_list = list(set(approved_rejected_task_email_list))
                update_task_email_details(int(assignee_userid), approved_rejected_task_email_list,
                                          task_email_list, config)


        except Exception:
            print("Exception in Task.create_task\n{}".format(traceback.format_exc()))

    final_response = {"messageCodeTO": {
        "status": "SUCCESS",
        "code": "S100",
        "displayMsg": display_msg
    }}

    return final_response


def reject_email(additional_parameters):
    alt_id = additional_parameters["approvee_alt_id"]
    o365_email_resource_id = additional_parameters["mail_id"]
    user_data = get_user_data(alt_id, config)
    approver_email = additional_parameters["approver_email"]
    email_sub = additional_parameters["email_sub"]
    display_msg = "You have already taken an action on this task"
    #email_sub = email_sub.decode("utf-8")
    if user_data and o365_email_resource_id not in user_data.get("approved_rejected_mail_ids", []):
        approved_rejected_email_list = user_data.get("approved_rejected_mail_ids", [])
        approved_rejected_email_list.append(o365_email_resource_id)
        approved_rejected_email_list = list(set(approved_rejected_email_list))
        rejected_email_list = user_data.get("rejected_mail_ids", [])
        rejected_email_list.append(o365_email_resource_id)
        rejected_email_list = list(set(rejected_email_list))
        approved_rejected_task_email_list = user_data.get("approved_rejected_task_mail_ids", [])
        approved_rejected_task_email_list.append(o365_email_resource_id)
        approved_rejected_task_email_list = list(set(approved_rejected_task_email_list))
        update_approved_rejected_email_details(int(alt_id), approved_rejected_task_email_list,
                                               approved_rejected_email_list, rejected_email_list, config,
                                               False)
        reply_all(o365_email_resource_id, user_data, config, False)

        display_msg = "You have rejected the email <b>from</b> {} with <b>subject</b>, {}".format(
            base64.urlsafe_b64decode(str(approver_email)),
            base64.urlsafe_b64decode(str(email_sub))
        )

    final_response = {"messageCodeTO": {
        "status": "SUCCESS",
        "code": "S100",
        "displayMsg": display_msg
    }}
    return final_response


def approve_email(additional_parameters):
    alt_id = additional_parameters["approvee_alt_id"]
    o365_email_resource_id = additional_parameters["mail_id"]
    user_data = get_user_data(alt_id, config)
    approver_email = additional_parameters["approver_email"]
    email_sub = additional_parameters["email_sub"]
    display_msg = "You have already taken an action on this task"
    #email_sub = email_sub.decode("utf-8")
    if user_data and o365_email_resource_id not in user_data.get("approved_rejected_mail_ids", []):
        approved_rejected_email_list = user_data.get("approved_rejected_mail_ids", [])
        approved_rejected_email_list.append(o365_email_resource_id)
        approved_rejected_email_list = list(set(approved_rejected_email_list))
        approved_email_list = user_data.get("approved_mail_ids", [])
        approved_email_list.append(o365_email_resource_id)
        approved_email_list = list(set(approved_email_list))
        approved_rejected_task_email_list = user_data.get("approved_rejected_task_mail_ids", [])
        approved_rejected_task_email_list.append(o365_email_resource_id)
        approved_rejected_task_email_list = list(set(approved_rejected_task_email_list))
        update_approved_rejected_email_details(int(alt_id), approved_rejected_task_email_list,
                                               approved_rejected_email_list, approved_email_list, config,
                                               True)
        reply_all(o365_email_resource_id, user_data, config, True)

        display_msg = "You have approved the email <b>from</b> {} with <b>subject</b>, {}".format(
            base64.urlsafe_b64decode(str(approver_email)),
            base64.urlsafe_b64decode(str(email_sub))
        )

    final_response = {"messageCodeTO": {
        "status": "SUCCESS",
        "code": "S100",
        "displayMsg": display_msg
    }}
    return final_response
