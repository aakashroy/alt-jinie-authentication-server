import json
import traceback
from datetime import datetime, timedelta

import pytz
import requests
import tinyurl

import config
from common.generic_functions import get_office_365_integration_embed_link
from common.o365_helpers import get_user_data, msteams_create_online_meeting


def get_tz_aware_meeting_start_end_time(df_start_date_time, user_time_zone):
    if df_start_date_time:
        if not user_time_zone:
            user_time_zone = "Asia/Kolkata"
        date_arr = df_start_date_time.split("T")
        date_parts = date_arr[0].split("-")
        dd = int(date_parts[2])
        mm_mon = int(date_parts[1])
        yy = int(date_parts[0])
        time_parts = date_arr[1].split(":")
        hh = int(time_parts[0])
        mm = int(time_parts[1])
        usertz = pytz.timezone(user_time_zone)
        # local_mtg_start_date = mtg_start_date.astimezone(pytz.timezone(user_time_zone))
        local_mtg_start_date = usertz.localize(datetime(yy, mm_mon, dd, hh, mm))
        utc_mtg_start_date = local_mtg_start_date.astimezone(pytz.utc)
    else:
        utc_mtg_start_date = datetime.utcnow().replace(tzinfo=pytz.utc)

    utc_mtg_end_date = utc_mtg_start_date + timedelta(hours=1)
    return utc_mtg_start_date, utc_mtg_end_date


def get_bitlink(longurl):
    try:
        bitly_headers = {
            "Authorization": "Bearer 277160f21fd1818608ace60ae5e803edc47e8b0b"
        }
        response = requests.post("https://api-ssl.bitly.com/v4/shorten",
                                 headers=bitly_headers,
                                 json={

                                     "group_guid": "Bk3q31WMuTW",
                                     "domain": "bit.ly",
                                     "long_url": longurl

                                 })
        shorturl = response.json()["link"]
    except Exception:
        try:
            shorturl = tinyurl.create_one(longurl)
        except Exception:
            shorturl = longurl

    return shorturl


def create_msteams_meeting(user_id, subject, meeting_start_time, meeting_end_time=None, is_internal_call=False):
    try:
        user_details = get_user_data(user_id, config)
    except Exception:
        user_details = None
        pass
    if user_details:
        if not subject:
            subject = "{}'s Meeting Room".format(user_details["full_name"])
        print(subject)
        print(meeting_start_time)
        if meeting_start_time and meeting_end_time:
            start_date_time = meeting_start_time + "Z"
            end_date_time = meeting_end_time + "Z"
        else:
            utc_meeting_start_time, utc_meeting_end_time = get_tz_aware_meeting_start_end_time(meeting_start_time,
                                                                                               user_details.get(
                                                                                                   "timezone",
                                                                                                   ""))
            start_date_time = utc_meeting_start_time.strftime("%Y-%m-%dT%H:%M:00Z")
            end_date_time = utc_meeting_end_time.strftime("%Y-%m-%dT%H:%M:00Z")
            # start_date_time = "2020-03-26T05:30:00Z"
            # end_date_time = "2020-03-26T06:30:00Z"

        result = msteams_create_online_meeting(subject,
                                               start_date_time,
                                               end_date_time,
                                               user_details,
                                               config)
        if result.status_code == 201:
            result = result.json()
            print(json.dumps(result, indent=4))
            shorturl = get_bitlink(result["joinWebUrl"])
            if is_internal_call:
                return shorturl
            else:
                return "Here's your meeting link. This link is like a bridge, whoever you share it with will be able to join the meeting. " \
                       "You can go ahead share it with the meeting participants :) <br/><br/><a href='{}'>{}</a>".format(
                    shorturl, shorturl)
        else:
            if is_internal_call:
                return None
            else:
                if result.status_code == 400:
                    return "I'm sorry, you need to sign up with Microsoft Teams for your organization for this to work. " \
                           "<a href='https://products.office.com/en-in/microsoft-teams/free?rtc=1'>See Here.</a>"
                else:
                    return "Oops! Looks like you haven't linked your account with Office 365 yet.<br/><br/>" \
                           "{}".format(get_office_365_integration_embed_link(user_id, "Click Here to Office 365"))
    else:
        if is_internal_call:
            return None
        else:
            return "Oops! Looks like you haven't linked your account with Office 365 yet.<br/><br/>" \
                   "{}".format(get_office_365_integration_embed_link(user_id, "Click Here to Link Office 365"))


def handle_msteams_meeting_query(intent, alt_user_id, additional_parameters):
    response = ""
    if intent == "ML_TEAMS_MEETING":
        try:
            response = create_msteams_meeting(alt_user_id, "", "")
        except Exception:
            traceback.print_exc()
            response = None
        if not response:
            response = "I'm having trouble communicating with Microsoft Teams servers at the moment. Kindly try again later."
    return response, ""


if __name__ == "__main__":
    handle_msteams_meeting_query("ML_TEAMS_MEETING", "4384885", None)
