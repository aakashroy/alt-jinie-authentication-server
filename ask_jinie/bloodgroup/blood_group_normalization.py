"""It will normalize the user blood group"""

import re
import string


def cleaning(blood_group):
    """
    Cleaning Blood group and normalizing it
    :param blood_group: List of Blood Group
    :return: Clean and normalized list of blood groups
    """
    result = list()
    remove = string.punctuation
    remove = remove.replace("+", "")
    remove = remove.replace("-", "")
    pattern = r"[{}]".format(remove)
    for i in blood_group:
        striped_str = i.strip()
        space_removed_str = striped_str.replace(" ", "")
        punctuation_removed_str = re.sub(pattern, "", space_removed_str)
        clean_str = punctuation_removed_str.lower()
        result.append(clean_str)
    return result


def blood_group_normalization(given_blood_group):
    """
    User input Blood Group Mapping with Database stored Blood Group
    :param blood_group: Blood Group
    :return: List of blood group
    """
    req_blood_group = [given_blood_group]
    A_positive = ['A +ve', 'A Positive', 'A+', 'A+ve', 'A +', 'APositive']
    A_negative = ['A -ve', 'A Negative', 'A-', 'A-ve', 'A -', 'ANegative']
    B_positive = ['B +ve', 'B Positive', 'B+', 'B+ve', 'B +', 'BPositive']
    B_negative = ['B -ve', 'B Negative', 'B-', 'B-ve', 'B -', 'BNegative']
    A_RH_positive = ['A(RH)Positive', 'A(RH)+ve', 'A(RH)+', 'A(RH) Positive', 'A(RH) +ve', 'A(RH) +']
    A1_positive = ['A1 Positive', 'A1 +', 'A1+ve', 'A1Positive', 'A1+', 'A1 +ve']
    A1_negative = ['A1 Negative', 'A1 -', 'A1-ve', 'A1Negative', 'A1-', 'A1 -ve']
    A1B_negative = ['A1B Negative', 'A1B-', 'A1B-ve', 'A1BNegative', 'A1B -', 'A1B -ve']
    A1B_positive = ['A1B Positive', 'A1B+', 'A1B+ve', 'A1BPositive', 'A1B +', 'A1B +ve']
    A2_negative = ['A2 Negative', 'A2-', 'A2-ve', 'A2Negative', 'A2 -', 'A2 -ve']
    A2_positive = ['A2 Positive', 'A2+', 'A2+ve', 'A2Positive', 'A2 +', 'A2 +ve']
    A2B_positive = ['A2B +', 'A2B Positive', 'A2B+ve', 'A2B+', 'A2BPositive', 'A2B +ve']
    A2rh_positive = ['A2rh Positive', 'A2rh+', 'A2rh+ve', 'A2rhPositive', 'A2rh +', 'A2rh +ve']
    AB_positive = ['AB +ve', 'AB Positive', 'AB+', 'AB+ve', 'ABPositive', 'AB +']
    AB_negative = ['AB -ve', 'AB Negative', 'AB-', 'AB-ve', 'ABNegative', 'AB -']
    O_positive = ['O +ve', 'O Positive', 'O+ve', 'O+', 'OPositive', 'O +']
    O_negative = ['O -ve', 'O Negative', 'O-ve', 'O-', 'ONegative', 'O -']

    all_blood_groups = [A_positive,
                        A_negative, B_negative, B_positive, A_RH_positive, A1_positive, A1_negative, A1B_positive,
                        A1B_negative,
                        A2_negative, A2_positive, A2B_positive, A2B_positive, A2rh_positive, AB_positive, AB_negative,
                        O_positive, O_negative]
    list_of_blood = list()
    for blood_group in all_blood_groups:
        blood_category = cleaning(blood_group)
        req_blood = cleaning(req_blood_group)
        if req_blood[0] in blood_category:
            list_of_blood.extend(blood_group)
            break
    return list_of_blood


if __name__ == "__main__":
    print blood_group_normalization('Test')
