"""Contains all entry point and helper functions for course recommendation"""
from datetime import datetime, timedelta

import config
from ask_jinie.course_recommendation.db_helpers import fetch_empcode_tentantid, fetch_userid_enterpriseid, \
    enterprise_top_courses, get_course_names, find_course_date, find_enrolled_courses, \
    list_peer_empcodes, find_peers_userid, find_courses_of_peers, find_designation
from ask_jinie.course_recommendation.recommender.course_recommender_model import course_rec_ml
from ask_jinie.course_recommendation.recommender.get_courses_from_external_sources import get_courses_from_moocs, \
    get_course_from_mooc_conversational
from common.sql_helpers import connect_to_mysql_server, connect_to_mssql_server


def fetch_basic_details(altid, mysql_conn, mssql_conn):
    """
    Fetches basic details of employees
    :param altid: Altid
    :param mysql_conn: Mysql connection object
    :param mssql_conn: Mssql connection object
    :return: employee_code, user_id, enterprise_id, tenant_id
    """
    employee_code, user_id, enterprise_id, tenant_id = -1, -1, -1, -1
    empcode_tent_id = fetch_empcode_tentantid(altid, mssql_conn)
    if empcode_tent_id['success']:
        employee_code = empcode_tent_id['emp_code']
        tenant_id = empcode_tent_id['tenant_id']
        # print employee_code,tenant_id

        userid_enterp_id = fetch_userid_enterpriseid(tenant_id, employee_code, mysql_conn)
        if userid_enterp_id['success'] is True:
            user_id = userid_enterp_id['user_id']
            enterprise_id = userid_enterp_id['enterprise_id']

    return employee_code, user_id, enterprise_id, tenant_id


def top_courses(alt_id, count=3):
    """
    Find top courses in the organization
    :param alt_id: Altid of the user
    :param count: Max courses to fetch
    :return: Top Courses in organization
    """
    final_list = list()
    mysql_conn = None
    mssql_conn = None
    ex = None
    error_message = ""
    try:
        mysql_conn = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
        mssql_conn = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
        if employee_code > 0 and user_id > 0 and enterprise_id > 0 and tenant_id > -1:
            enterprise_top_course = enterprise_top_courses(enterprise_id, mysql_conn, count)
            top_n_courses = [i[0] for i in enterprise_top_course]

            # active_courses = find_active_training_course(enterprise_id, mysql_conn)
            # active_courses_list = [i[0] for i in active_courses]
            # top_active_courses = [i for i in top_courses if i in active_courses_list]
            # top_n_courses = top_active_courses[:n]  # list of  top  10 active courses in an Enterprise

            string_top_n_courseid = ','.join([str(i) for i in top_n_courses])
            if string_top_n_courseid:
                final_list = get_course_names(mysql_conn, string_top_n_courseid)
            # top_courses_dict = dict()
            # final_list = list()
            # # cnt = 0
            # for i in top_n_courses:
            #     top_courses_dict["course_id"] = i
            #     top_courses_dict["course_name"] = top_course_name_dict[i]
            #     # top_courses_dict["rank"] = cnt
            #     # print top_courses_dict
            #     final_list.append(top_courses_dict.copy())
            #     # cnt += 1
        else:
            error_message = "User or organisation not found"
    except Exception as exp:
        ex = exp

    if mysql_conn is not None:
        mysql_conn.close()
    if mssql_conn is not None:
        mssql_conn.close()

    if ex is not None:
        raise ex

    return final_list, error_message


def trending_courses(alt_id, count=3):
    """
    Find trending courses in an organization
    :param alt_id: Altid of the user
    :param count: Max courses to fetch
    :return: Returns Trending courses in organization
    """
    final_list = list()
    mysql_conn = None
    mssql_conn = None
    ex = None
    error_message = ""
    try:
        mysql_conn = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
        mssql_conn = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
        if employee_code > 0 and user_id > 0 and enterprise_id > -1 and tenant_id > -1:
            date = datetime.now().date()
            todaydatetime = str(date) + ' ' + "00:00:00"
            since_days = 7
            while True:
                date = datetime.now() - timedelta(days=since_days)
                prevdate = str(date.date()) + ' ' + "23:59:00"
                courses = find_course_date(enterprise_id, mysql_conn, todaydatetime, prevdate, count)
                trending_active_courses = [int(i[0]) for i in courses]
                # active_courses = find_active_training_course(enterprise_id, mysql_conn)
                # active_courses_list = [int(i[0]) for i in active_courses]

                # trending_active_courses = [i for i in course_trend if i in active_courses_list]

                if len(trending_active_courses) >= 3:
                    top_n_trending = trending_active_courses[:count]
                    string_top_3_courseid = ','.join([str(i) for i in top_n_trending])
                    # print string_top_10_courseid
                    final_list = get_course_names(mysql_conn, string_top_3_courseid)
                    break

                if since_days == 21:
                    break  # "No trending course to show"
                since_days += 7
        else:
            error_message = "User or organisation not found"
    except Exception as exp:
        ex = exp

    if mysql_conn is not None:
        mysql_conn.close()
    if mssql_conn is not None:
        mssql_conn.close()

    if ex is not None:
        raise ex

    return final_list, error_message


def already_enrolled_courses(alt_id):
    """
    Show list of already enrolled courses
    :param alt_id: Altid of the user
    :return: List of Enrolled Courses
    """
    final_list = list()
    mysql_conn = None
    mssql_conn = None
    ex = None
    try:
        mysql_conn = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
        mssql_conn = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
        if employee_code > 0 and user_id > -1 and enterprise_id > 0 and tenant_id > 0:
            enrolled_courses = find_enrolled_courses(user_id, mysql_conn)
            string_courseid = ','.join([str(i) for i in enrolled_courses])
            final_list = get_course_names(mysql_conn, string_courseid)
    except Exception as exp:
        ex = exp

    if mysql_conn is not None:
        mysql_conn.close()
    if mssql_conn is not None:
        mssql_conn.close()

    if ex is not None:
        raise ex

    return final_list


def recommend_courses_for_me(alt_id):
    """
    Function to recommend courses basis of enrolled courses
    :param alt_id: Altid of the user
    :return: List of Recommended Courses
    """
    final_list = list()
    mysql_conn = None
    mssql_conn = None
    ex = None
    error_message = ""
    try:
        mysql_conn = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
        mssql_conn = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
        if enterprise_id > 0 and user_id > 0 and config.DEFAULT_COURSE_RECOMMENDER_SOURCE_OVERRIDE != "MOOC":  # When user is present in both talentpact and qtrain
            enrolled_courses = already_enrolled_courses(alt_id)
            temp_course_list = [i['course_id'] for i in enrolled_courses]
            final_list = list()
            if temp_course_list:
                rec_course = course_rec_ml(temp_course_list)
                if rec_course:
                    peers_employee_code = list_peer_empcodes(employee_code, tenant_id,
                                                             mssql_conn)  # Employee code of peers on same designationn
                    peers_employee_code.remove(int(employee_code))
                    string_peer_empcode = ','.join(["'" + str(i) + "'" for i in peers_employee_code])
                    peer_user_id = find_peers_userid(string_peer_empcode, enterprise_id, mysql_conn)
                    list_peer_userid = ','.join(["'" + str(i[0]) + "'" for i in peer_user_id])
                    course_enrol_by_peer = find_courses_of_peers(list_peer_userid, mysql_conn)
                    peers_course_list = [int(i[0]) for i in course_enrol_by_peer]
                    course_to_recommend = [i for i in rec_course if i in peers_course_list]
                    if course_to_recommend:
                        string_courseid = ','.join([str(i) for i in course_to_recommend])
                        final_list = get_course_names(mysql_conn, string_courseid)
        elif employee_code > 0 and tenant_id > 0:  # when user have only access to jinie but not altLearning.
            resp = find_designation(employee_code, tenant_id, mssql_conn)
            if resp:
                # final_list = get_courses_from_moocs([resp[0][1]])
                final_list = get_courses_from_moocs([resp[0][1]])
        else:
            error_message = "User or organisation not found"
    except Exception as exp:
        ex = exp

    if mysql_conn is not None:
        mysql_conn.close()
    if mssql_conn is not None:
        mssql_conn.close()

    if ex is not None:
        raise ex

    return final_list, error_message


def recommend_courses(alt_id, skills, level, duration, price, count=3):
    """
    Function to recommend courses on the basis of user skills
    :param alt_id: Altid of the user
    :param level: Level of course
    :param duration: duration of course
    :param price: paid/free
    :param skills: List of skills
    :param count: Max courses to fetch
    :return: Courses on basis of skills
    """

    final_list = list()
    mysql_conn = None
    mssql_conn = None
    ex = None
    error_message = ""
    try:
        mysql_conn = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
        mssql_conn = connect_to_mssql_server(config.TALENTPACT_DB, config.MSSQL_CREDENTIALS)
        employee_code, user_id, enterprise_id, tenant_id = fetch_basic_details(alt_id, mysql_conn, mssql_conn)
        if employee_code == -1 and tenant_id == -1:  # when userid it invalid or not present in Talentpact
            error_message = "User or organisation not found"
        else:

            if not isinstance(skills, list):
                skills = skills.strip()
                skills = skills.strip("[]")
                skills = skills.split(",")
                skills = [x.strip() for x in skills]

            # skill_lower=skills.lower()
            # skill_striped=skill_lower.strip()
            # skill_splitted=skill_striped.split(',')
            # clean_skill_list=map(str.strip, skill_splitted)

            if enterprise_id > 0 and user_id > 0 and config.DEFAULT_COURSE_RECOMMENDER_SOURCE_OVERRIDE != "MOOC":  # When user is present in both talentpact and qtrain
                if skills:
                    for i in skills:
                        final_list.extend(
                            get_course_names(mysql_conn, ids=None, coursename_like=i.strip(), count=count))
            else:  # when user have only access to jinie but not altLearning.
                # final_list = get_courses_from_moocs(skills)
                final_list = get_course_from_mooc_conversational(skills, level, duration, price)
    except Exception as exp:
        ex = exp

    if mysql_conn is not None:
        mysql_conn.close()
    if mssql_conn is not None:
        mssql_conn.close()

    if ex is not None:
        raise ex

    return final_list, error_message


def course_recommendation_handler(intent, alt_id, skills, level, duration, price, count):
    """
    Check the intent of the message and decides which function to invoke
    :param intent: Intent
    :param alt_id: Altid of the user
    :param level: level of course
    :param duration: duration of course
    :param price: paid/free
    :param skills: List of skills
    :param count: Count
    :return: Calls desired function on the basis of Intent
    """
    # print intent, alt_id, skills, level, duration, price, count
    if intent == "ML_TOP_COURSES":
        return top_courses(alt_id)
    elif intent == "ML_TRENDING_COURSES":
        return trending_courses(alt_id)
    elif intent == "ML_RECOMMEND_COURSES_ME":
        return recommend_courses_for_me(alt_id)
    elif intent == "ML_RECOMMEND_COURSES":
        return recommend_courses(alt_id, skills, level, duration, price, count)
    else:
        return []


if __name__ == "__main__":
    # print
    # "Top Course:"
    # print
    # top_courses(2205395)
    # print
    # "\nTrending Course:"
    # print
    # trending_courses(2205395)
    # print
    # "\nAlredy Enrolled Course:"
    # print
    # already_enrolled_courses(2205395)
    # print
    # "\nRecommended Courses for me:"
    # print
    # recommend_courses_for_me(2205395)1736034
    print "\nRecommended Courses:"
    # print recommend_courses(2653216, ['python', 'php'],'all','long','paid')
    print recommend_courses(1736034, ['java'], 'all', 'long', 'paid')
