"""Fetching courses from Udemy using API"""
import json

import urllib2

from ask_jinie.course_recommendation.recommender.calling_udemy_api import _get_suggested_courses_from_skill

URL = 'https://jqodrwa9u4.execute-api.ap-south-1.amazonaws.com/test/suggest-courses-from-skills'
DATA = {
    "s": "1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98",
}
URL_Conversation = ''


def get_courses_from_moocs(skill_list):
    """
    Function to find course from MOOC's
    :param skill_list: List of skills
    :return: List of courses from Udemy API
    """
    DATA["skill_list"] = skill_list
    req = urllib2.Request(URL)
    req.add_header('Content-Type', 'application/json')
    response = urllib2.urlopen(req, json.dumps(DATA))
    json_response = json.loads(response.read())
    function_output = list()
    if json_response['status'] is True:
        for skc in json_response["response"]["skills_will_finish"]:
            skill = skc["skill"]
            for course in skc["courses"]:
                icon = "http://daas.peoplestrong.com/static/images/udemy.png"
                if "course_icon" in course:
                    icon = course["course_icon"]
                function_output.append({
                    "course_id": course["course_id"],
                    "course_name": course["title"],
                    "source": "MOOC::" + course["source"],
                    "icon": icon,
                    "heading_1": course["title"],
                    "heading_2": "Level: " + str(course["instructional_level"]),
                    "heading_3": "Rating: " + str(round(course["rating"], 1)),
                    "footer": skill.capitalize() + " | Udemy",
                    "user_click_actions": [
                        {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                         "url": course["url"]}]
                })
    return function_output


def get_course_from_mooc_conversational(skills, level, duration, price):
    """

    :param skills: list of skills
    :param level: level of course
    :param duration: course duration
    :param price: free/paid
    :return: Courses on Udemy
    """
    #   Extract skills,duration,level and price from slots
    skills = [str(skill) for skill in skills]
    api_data_p = {'skill_list': skills,
                  'duration': str(duration),
                  'instructional_level': str(level),
                  'paid': str(price)
                 }
    data = {'s': '1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98'}
    data.update(api_data_p)
    json_response = _get_suggested_courses_from_skill(data,
                                                      None)  ##       Hits Udemy API       ##

    # DATA["skill_list"] = skill_list
    # req = urllib2.Request(URL_Conversation)
    # req.add_header('Content-Type', 'application/json')
    # response = urllib2.urlopen(req, json.dumps(data))
    # json_response = json.loads(response.read())
    function_output = list()
    if json_response['status'] is True:
        for skc in json_response["response"]["skills_will_finish"]:
            skill = skc["skill"]
            for course in skc["courses"]:
                icon = "http://daas.peoplestrong.com/static/images/udemy.png"
                if "course_icon" in course:
                    icon = course["course_icon"]
                function_output.append({
                    "course_id": course["course_id"],
                    "course_name": course["title"],
                    "source": "MOOC::" + course["source"],
                    "icon": icon,
                    "heading_1": course["title"],
                    "heading_2": "Level: " + str(course["instructional_level"]),
                    "heading_3": "Rating: " + str(round(course["rating"], 1)),
                    "footer": skill.capitalize() + " | Udemy",
                    "user_click_actions": [
                        {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                         "url": course["url"]}]
                })
    return function_output


if __name__ == "__main__":
    print
    get_courses_from_moocs(["java"])
    print
    get_courses_from_moocs(["java", "csharp"])
    print
    get_courses_from_moocs(["qwertyui1234"])
