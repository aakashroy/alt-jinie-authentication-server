# -*- coding: utf-8 -*-
"""This module contains the _get_suggested_courses_from_skill method which is used by the
   get_suggested_courses_from_skill lambda to fetch courses given a list of skills. Courses
   are fetched from Udemy and Coursera based on the source parameter as on date
"""
import json
import math
import re
import string
import traceback
from datetime import datetime, timedelta
from pprint import pprint
from urllib import urlencode

import lxml.html
import requests

# from builtins import str

TESTING_SKILLS = ['machine learning', 'python']

UDEMY_REFERRER = 'https://www.udemy.com/courses/search/?'
UDEMY_API = 'https://www.udemy.com/api-2.0/search-courses/?'

COURSERA_URL = "https://www.coursera.org/courses"

HEADERS = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "accept-language": "en-US,en;q=0.9",
    "upgrade-insecure-requests": "1",
    "user-agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
}

E002 = {"E002": "Exception occurred while fetching skill"}
E003 = {"E003": "Exception occurred while processing course"}
E004 = {"E004": "Check max_courses argument: 0 < max_courses <= 10"}
E005 = {"E005": "Check end_date argument: end_date should be valid date / time"}
E006 = {
    "E006": "Check skill_list argument: should be a non-empty list with each skill length 0 < len(skill) < 100 and len(skill_list) <= 100"}
E007 = {"E007": "end_date parameter is not supported with source Coursera"}
E008 = {"E008": "source parameter can be either Udemy (default) or Coursera"}
E009 = {"E009": "Paid / Free classification is only available for Udemy"}
E010 = {"E010": "Exception occurred while fetching Rating"}
E011 = {"E011": "Exception occurred while fetching Ordering"}
E012 = {"E012": "Exception occurred while fetching Level"}
E013 = {"E013": "Exception occurred while fetching Duration"}


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def get_business_finish_date(days_to_add, include_weekends=True):
    """This method computes the end date of a course given the number of days to finish the course"""
    _now = datetime.now()
    new_now = _now.replace(hour=0, minute=0, second=0, microsecond=0)
    new_now = new_now + timedelta(days=1)
    int_days_to_add = int(math.ceil(days_to_add))

    if include_weekends:
        return new_now + timedelta(days=(int_days_to_add - 1))

    i = 1
    next_business_day = new_now
    while i < int_days_to_add:
        next_business_day = new_now + timedelta(days=1)
        wkday = next_business_day.weekday()
        # 0 - 6, starting from Monday 5 - Sat, 6 - Sun
        wknd = (6 - wkday) + 1
        if wknd < 3:
            int_days_to_add = int_days_to_add + wknd
            i = i + wknd
        else:
            i = i + 1

    add_days = (6 - next_business_day.weekday()) + 1
    if add_days < 2:
        next_business_day = next_business_day + timedelta(days=add_days)

    return next_business_day


def get_datetime_from_str(date_str):
    """Tries to parse a python date from a given string using commonly used formats"""
    if not isinstance(date_str, datetime):
        try:
            _datetime = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%fZ")
        except:
            try:
                _datetime = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S.%f")
            except:
                try:
                    _datetime = datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S")
                except:
                    try:
                        _datetime = datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")
                    except:
                        try:
                            _datetime = datetime.strptime(date_str, "%Y-%m-%d")
                        except:
                            try:
                                _datetime = datetime.strptime(date_str, "%y-%m-%d")
                            except:
                                return None
        return _datetime
    else:
        return date_str


def get_courses_from_udemy(url, referer_url):
    """Fetches courses from Udemy"""
    udemy_api_response = {}
    fetched = False
    exception_string = ""
    udemy_http_response = None
    try:
        HEADERS['referer'] = referer_url
        udemy_http_response = requests.get(url=url, headers=HEADERS)
        udemy_api_response = udemy_http_response.json()
        if "courses" not in udemy_api_response:
            # raise IOError("Wrong data structure received from UDEMY: " + unicode(json.dumps(udemy_api_response)))
            raise IOError("Wrong data structure received from UDEMY: " + str(json.dumps(udemy_api_response)))
        fetched = True
        # print(json.dumps(udemy_api_response)
    except:
        exception_string = traceback.format_exc()
        exception_string = exception_string + " | Server Response: " + str(udemy_http_response)
        print(exception_string)
    return {'fetched': fetched, 'courses': udemy_api_response, 'errmsg': exception_string}


def are_equal_considering_plural(word1, word2):
    if word1 == word2:
        return True
    elif (word1 + "s") == word2:
        return True
    elif (word1 + "es") == word2:
        return True

    elif (word2 + "s") == word1:
        return True
    elif (word2 + "es") == word1:
        return True

    return False


def process_udemy_course(course, keyword_arr=None):
    """Converts Udemy course data structure into a normalized data structure"""
    # Status 1 means Success
    # Status 0 means Ignore
    # Status -1 means Error
    converted_course = dict()
    course_exception_string = ""
    try:
        if keyword_arr is not None and len(keyword_arr) > 0:

            # replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))

            lowercase_title = str(course["title"].strip().lower())
            # if type(lowercase_title) is unicode:
            if type(lowercase_title) is str:
                lowercase_title = lowercase_title.encode('ascii', 'ignore')
            # lowercase_title = lowercase_title.translate(replace_punctuation)
            lowercase_title = lowercase_title.translate(None, string.punctuation)

            title_arr = lowercase_title.split(" ")

            lowercase_headline = str(course["headline"].strip().lower())
            # if type(lowercase_headline) is unicode:
            if type(lowercase_headline) is str:
                lowercase_headline = lowercase_headline.encode('ascii', 'ignore')
            # lowercase_headline = lowercase_headline.translate(replace_punctuation)
            lowercase_headline = lowercase_headline.translate(None, string.punctuation)

            headline_arr = lowercase_headline.split(" ")

            keywords_in_title = len(title_arr) > 0
            keywords_in_headline = len(headline_arr) > 0

            for keyword in keyword_arr:
                token_match_title = False
                token_match_headline = False

                for title in title_arr:
                    token_match_title = token_match_title | are_equal_considering_plural(keyword, title)
                    if token_match_title:
                        break
                keywords_in_title = keywords_in_title & token_match_title

                for headline in headline_arr:
                    token_match_headline = token_match_headline & are_equal_considering_plural(keyword, headline)
                    if token_match_headline:
                        break
                keywords_in_headline = keywords_in_headline & token_match_headline

            if keywords_in_title is False and keywords_in_headline is False:
                return {'processed_course': converted_course, 'status': 0}

        converted_course["title"] = course["title"].strip()
        converted_course["rating"] = course["rating"]
        converted_course["num_reviews"] = course["num_reviews"]
        try:
            content_info = re.sub(r"(?:hours|hour|total|\s)", "", course["content_info"])
            converted_course["duration"] = float(content_info)
        except:
            try:
                content_info = re.sub(r"(?:mins|min|total|\s)", "", course["content_info"])
                converted_course["duration"] = float(content_info) / (60.0)
            except:
                return {'processed_course': converted_course, 'status': 0}

        converted_course["url"] = "https://www.udemy.com" + course["url"].strip()
        converted_course["is_paid"] = course["is_paid"]
        converted_course["instructional_level"] = course["instructional_level"]
        if converted_course["is_paid"] is True:
            if "discount" in course and course["discount"] is not None \
                    and "price" in course["discount"] \
                    and course["discount"]["price"] is not None:
                converted_course["price"] = course["discount"]["price"]["amount"]
                converted_course["currency_symbol"] = course["discount"]["price"]["currency"]
            else:
                converted_course["price"] = course.get("price", "N/A")
        converted_course["source"] = "Udemy"
        converted_course["course_id"] = course["id"]

        probable_img_keys = ['image_480x270', 'image_304x171', 'image_240x135', 'image_125_H', 'image_100x100']
        course_icon = ""
        for img_key in probable_img_keys:
            if img_key in course and course[img_key]:
                course_icon = course[img_key]
                break

        if course_icon:
            converted_course["course_icon"] = course_icon

    except:
        course_exception_string = traceback.format_exc()
    status = 1 if len(course_exception_string) == 0 else -1
    return {'processed_course': converted_course, 'status': status, 'error': course_exception_string}


def process_udemy_courses(skill_to_process, end_date, max_courses, skill_keywords):
    """Filters Udemy courses once fetched from the website"""
    # 1. Will Finish
    # 2. Wont Finish
    # 3. Error
    if skill_to_process["status"] is False:
        skill_to_process.pop("status")
        return {'processed_skill': skill_to_process, 'status': 3}

    courses = skill_to_process["courses"]
    if len(courses) == 0:
        if "status" in skill_to_process:
            skill_to_process.pop("status")
        return {'processed_skill': skill_to_process, 'status': 1 if end_date is None else 2}
    else:
        error_message = ""
        course_list = list()
        processed_course_list = list()
        if end_date is not None:

            _now = datetime.now()
            new_now = _now.replace(hour=0, minute=0, second=0, microsecond=0)
            new_now = new_now + timedelta(days=1)

            end_date = end_date.replace(hour=23, minute=59, second=59)
            end_date = end_date - timedelta(days=1)

            diff = end_date - new_now
            knapsack_hours = diff.days * 7.0
            seconds_in_days = diff.seconds / (60.0 * 60.0 * 24.0)
            knapsack_hours = math.ceil(knapsack_hours + seconds_in_days * 7.0)

            for course in courses:
                processed_course = process_udemy_course(course, skill_keywords)
                if processed_course["status"] == 0:  # Ignore the course as it contains some other unit of time
                    continue
                elif processed_course["status"] == 1:
                    processed_course_list.append(processed_course["processed_course"])
                    # course_finish_date = get_business_finish_date(processed_course["processed_course"]["duration"])
                    if processed_course["processed_course"]["duration"] <= knapsack_hours:
                        course_list.append(processed_course["processed_course"])
                        break
                else:
                    error_message = processed_course["error"]
                    break

            if "status" in skill_to_process:
                skill_to_process.pop("status")
            if len(error_message) > 0:
                if "courses" in skill_to_process:
                    skill_to_process.pop("courses")
                skill_to_process["error"] = error_message
                return {'processed_skill': skill_to_process, 'status': 3}
            else:
                if len(course_list) > 0:
                    skill_to_process["courses"] = course_list
                    return {'processed_skill': skill_to_process, 'status': 1}
                else:
                    tmp_course_list = [processed_course_list[0]] if len(processed_course_list) > 0 else []
                    skill_to_process["courses"] = tmp_course_list
                    return {'processed_skill': skill_to_process, 'status': 2}
        else:
            max_courses = min(max_courses, len(courses))
            for i in range(0, max_courses):
                processed_course = process_udemy_course(courses[i], skill_keywords)
                if processed_course["status"] == 0:
                    continue
                elif processed_course["status"] == 1:
                    course_list.append(processed_course["processed_course"])
                else:
                    error_message = processed_course["error"]
                    break
            if "status" in skill_to_process:
                skill_to_process.pop("status")
            if len(error_message) > 0:
                if "courses" in skill_to_process:
                    skill_to_process.pop("courses")
                skill_to_process["error"] = error_message
                return {'processed_skill': skill_to_process, 'status': 3}
            else:
                skill_to_process["courses"] = course_list
                return {'processed_skill': skill_to_process, 'status': 1}


def get_coursera_courses(courses_xpath):
    """Fetches courses from Coursera website"""
    course_list = list()
    course_elements = courses_xpath.xpath(
        '//div[contains(@class,"rc-SearchResults")]/div[@data-reactid]/div[not(@class)]')
    for course_element in course_elements:
        course_dict = dict()
        # course_dict["title"] = unicode(course_element.xpath('.//h2[contains(@class,"headline")]')[0].text_content())
        # course_dict["url"] = u"https://www.coursera.org" + unicode(course_element.xpath('.//a/@href')[0])
        course_dict["title"] = str(course_element.xpath('.//h2[contains(@class,"headline")]')[0].text_content())
        course_dict["url"] = u"https://www.coursera.org" + str(course_element.xpath('.//a/@href')[0])
        course_dict["source"] = "Coursera"
        course_list.append(course_dict)
    return course_list


def _get_suggested_courses_from_skill(event, context):
    """EPF for lambda to get courses against a given list of skills"""
    if 's' not in event or event['s'] != '1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98':
        return dict()

    # print("paid? :", event)
    if 'paid' in event:
        if "source" in event and event["source"] != "Udemy":
            return {'response': dict(), 'status': False, 'error': E009}

    paid_or_free_course_filter = ""
    if event.get("paid") in ["paid", "free"]:
        paid_or_free_course_filter = "price-paid" if event.get("paid") == "paid" else "price-free"
    # print("paid-or-free: ", paid_or_free_course_filter)
    source = "Udemy"

    if 'source' in event:
        if event["source"] == "Coursera" and 'end_date' in event:
            return {'response': dict(), 'status': False, 'error': E007}
        if event["source"] != "Udemy" and event["source"] != "Coursera":
            return {'response': dict(), 'status': False, 'error': E008}
        source = event["source"]

    max_courses = 6

    if 'max_courses' in event:
        try:
            max_courses = int(event['max_courses'])
            if max_courses <= 0 or max_courses > 10:
                return {'response': dict(), 'status': False, 'error': E004}
        except:
            return {'response': dict(), 'status': False, 'error': E004}

    end_date = None
    if 'end_date' in event:
        end_date = event['end_date']
        end_date = get_datetime_from_str(end_date)
        if not isinstance(end_date, datetime):
            return {'response': {}, 'status': False, 'error': E005}

    skill_list = list()
    if 'skill_list' in event:
        skill_list = event['skill_list']

    if not isinstance(skill_list, list):
        return {'response': dict(), 'status': False, 'error': E006}
    rating_filter = ''
    if 'rating' in event:
        rating_filter = event['rating']
        if not isinstance(rating_filter, str):
            return {'response': dict(), 'status': False, 'error': E010}

    ordering_filter = ''
    if 'ordering' in event:
        ordering_filter = event['ordering']
        if not isinstance(ordering_filter, str):
            return {'response': dict(), 'status': False, 'error': E011}
    duration_filter = ''
    if 'duration' in event:
        # duration_filter = int(event['duration'])
        # if not isinstance(duration_filter, int):
        #     return {'response': dict(), 'status': False, 'error': E013}
        duration_filter = str(event['duration'])
        if not isinstance(duration_filter, str):
            return {'response': dict(), 'status': False, 'error': E013}
        # if duration_filter <= 5:
        #     duration_filter = 'short'
        # elif duration_filter >= 6 and duration_filter <= 15:
        #     duration_filter = 'medium'
        # elif duration_filter >= 16 and duration_filter<= 40:
        #     duration_filter = 'long'
        # else:
        #     duration_filter = 'extraLong'
    if 'instructional_level' in event:
        level_filter = event['instructional_level']
        if not isinstance(level_filter, str):
            return {'response': dict(), 'status': False, 'error': E012}

    # print "Duration Filer ", duration_filter
    cleaned_skill_list = list()
    for skill in skill_list:
        skill_name = skill.strip()
        if len(skill_name) > 100 or len(skill_name) == 0:
            return {'response': dict(), 'status': False, 'error': E006}
        cleaned_skill_list.append(skill_name)
        if len(cleaned_skill_list) > 100:
            return {'response': dict(), 'status': False, 'error': E006}

    skill_list = cleaned_skill_list
    # print("clean skills: ",skill_list)
    if len(skill_list) == 0:
        return {'response': dict(), 'status': False, 'error': E006}

    skills_will_finish_list = list()
    skills_wont_finish_list = list()
    skills_error_list = list()

    skill_processing_list = list()

    if source == "Udemy":

        for skill in skill_list:

            tokenize_skill = False
            skill_keywords = skill.strip().lower().split(" ")
            for skill_keyword in skill_keywords:
                if len(skill_keyword) < 4:
                    tokenize_skill = True
                    break

            if tokenize_skill is False:
                skill_keywords = None

            num_trials_to_fill = 0
            fetched = False
            final_course_list = list()
            unfiltered_courses = list()
            exception_string = ""
            is_relevant = False

            param_dict = {'q': skill, 'lang': 'en', 'ratings': '3.0'}

            if len(rating_filter) > 0:
                param_dict['ratings'] = rating_filter
            # print("Params Dict: ", param_dict)
            if len(paid_or_free_course_filter) > 0:
                param_dict['price'] = paid_or_free_course_filter
            if len(ordering_filter) > 0:
                param_dict['ordering'] = ordering_filter
            if len(duration_filter) > 0:
                param_dict['duration'] = duration_filter
            if len(level_filter) > 0:
                param_dict['instructional_level'] = level_filter

                while num_trials_to_fill < 2:

                    # print("Final Param DIct: ", param_dict)
                    skill_encoded = urlencode(param_dict)
                    # print(skill_encoded)

                    referer = UDEMY_REFERRER + skill_encoded
                    url = UDEMY_API + skill_encoded
                    # pprint("Final URL: ", url)
                    udemy_api_response = get_courses_from_udemy(url=url, referer_url=referer)
                    # print("api response",type(udemy_api_response),"~~~",udemy_api_response)
                    # break
                    fetched_itr = udemy_api_response["fetched"]
                    fetched = fetched_itr | fetched
                    exception_string = exception_string + udemy_api_response["errmsg"]

                    if fetched_itr:
                        print("Fetched courses for skill: " + str(skill) + " | Iter: " + str(num_trials_to_fill + 1))
                        unfiltered_courses_in_this_itr = udemy_api_response["courses"]["courses"]
                        unfiltered_courses.extend(unfiltered_courses_in_this_itr)
                        for unfiltered_course in unfiltered_courses_in_this_itr:
                            strip_lower_skill = skill.lower().strip()
                            title_desc = cleanhtml(
                                "{} {}".format(unfiltered_course["title"].encode('utf-8'),
                                               unfiltered_course["headline"].encode('utf-8')))
                            title_desc = title_desc.lower()
                            if len(re.findall(r"\b{}\b".format(strip_lower_skill), title_desc)) > 1:
                                is_relevant = True
                                if unfiltered_course["rating"] >= 4.0 and unfiltered_course["num_reviews"] >= 2000:
                                    final_course_list.append(unfiltered_course)
                                    if len(final_course_list) == 10:
                                        break
                    else:
                        break

                    if udemy_api_response["courses"]["count"] <= 10:
                        break

                    num_trials_to_fill = num_trials_to_fill + 1

                    param_dict['p'] = '2'

            if fetched and is_relevant:
                if len(final_course_list) <= 3:
                    final_course_list = unfiltered_courses
                skill_processing_list.append({'skill': skill, 'courses': final_course_list, 'status': True})
            else:
                skill_processing_list.append({'skill': skill, 'status': False, 'error': exception_string})

        # Now we have the skills to process in skill_processing_list

        # print(str(len(skill_processing_list)) + ", " + str(skill_processing_list))

        for skill_to_process in skill_processing_list:
            processed_skill = process_udemy_courses(skill_to_process, end_date, max_courses, skill_keywords)
            status = processed_skill["status"]
            if status == 1:
                skills_will_finish_list.append(processed_skill["processed_skill"])
            elif status == 2:
                skills_wont_finish_list.append(processed_skill["processed_skill"])
            else:
                skills_error_list.append(processed_skill["processed_skill"])

    elif source == "Coursera":

        for skill in skill_list:
            params = urlencode({'languages': 'en',
                                'primaryLanguages': 'en',
                                'query': skill,
                                'userQuery': skill})
            exception_string = ""
            coursera_courses = list()
            try:
                coursera_raw_response = requests.get("https://www.coursera.org/courses", params=params)
                if coursera_raw_response.status_code == 200:
                    coursera_courses = get_coursera_courses(lxml.html.document_fromstring(coursera_raw_response.text))
                    coursera_courses = coursera_courses[0:min(max_courses, len(coursera_courses))]
            except:
                exception_string = traceback.format_exc()

            if len(exception_string) == 0:
                skills_will_finish_list.append({'skill': skill, 'courses': coursera_courses})
            else:
                skills_error_list.append({'skill': skill, 'error': exception_string})

    return {'response': {'skills_will_finish': skills_will_finish_list, 'skills_wont_finish': skills_wont_finish_list,
                         'skills_met_errors': skills_error_list}, 'status': True, 'v': 1.11}


if __name__ == "__main__":
    # x = _get_suggested_courses_from_skill({'s': '1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98',
    #                                        'skill_list': ['java', 'python']}, None)
    # pprint(x)
    # x = _get_suggested_courses_from_skill(
    #     {'s': '1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98', 'skill_list': ['java', 'mongodb'],
    #      'duration': 'long', 'paid': 'price-free'}
    #     , None)
    x = _get_suggested_courses_from_skill(
        {'s': '1CBDC7C55180CC1A0240F6551AC3FA75E764B3C1A814908B9FB2747645766B98',
         'skill_list': ['Java'], 'paid': 'free', 'instructional_level': 'beginner'}
        , None)

    pprint(x)
