"""Loading course recommendation model"""
import os
import pickle

from ask_jinie.course_recommendation import models


def course_rec_ml(course_list):
    """
    Predicting listing of courses using pre trained wor2vec model
    :param course_list: List of courses
    :return: Predicted list of courses
    """
    model_file = os.path.dirname(models.__file__)
    model_file = model_file + "/w2v_model_new.pkl"
    # print (os.curdir)
    with open(model_file, 'rb') as model_file_handle:
        model = pickle.load(model_file_handle)

    vocab = list(model.wv.vocab)
    # Only those courses will go to model which are present in vocab..
    course_list = [value for value in course_list if value in vocab]
    res = []
    if course_list:
        result = model.most_similar(positive=course_list, topn=100)
        res = [int(i[0]) for i in result if i[1] >= 0.80]
    return res
