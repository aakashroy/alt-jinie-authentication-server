"""Names of intents for Course Recommendation"""
INTENT_MAP = {
    'ML_TOP_COURSES': {
    },
    'ML_TRENDING_COURSES': {
    },
    'ML_RECOMMEND_COURSES_ME': {
    },
    'ML_RECOMMEND_COURSES': {
    }
}
