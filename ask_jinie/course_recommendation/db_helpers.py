"""Database Query Helper Functions"""


def fetch_empcode_tentantid(userid, tpconn):  # talentpact
    """
    Fetches employee code and tenant id
    :param userid:Userid
    :param tpconn:TalentPact connection object
    :return:EmployeeCode,Tenantid
    """

    output = {"success": False}
    query = "Select EmployeeCode,TenantId from HrEmployee where userid=%s"
    tpconn.execute(query, userid)
    result = list(tpconn.fetchall())
    if result:
        output = {"success": True, "emp_code": result[0][0], "tenant_id": result[0][1]}
    return output


def fetch_userid_enterpriseid(tenantid, employeecode, qtconn):  # qtrain
    """
    Fetches userid and enterprise id
    :param tenantid:
    :param employeecode:
    :param qtconn:
    :return:Userid and Enterpriseid
    """

    output = {"success": False}
    query = "SELECT autoid,enterpriseID from tbl_users usr inner join enterprise e on usr.enterpriseID=e.eid " \
            "where ps_tenant_id=%s and client_userid=%s"
    qtconn.execute(query, (tenantid, employeecode))
    result = list(qtconn.fetchall())
    if result:
        output = {"success": True, "user_id": result[0][0], "enterprise_id": result[0][1]}
    return output


def enterprise_top_courses(enterpriseid, qtconn, count):  # qtrain
    """
    Finds Top Course in Enterprise w.r.t Count
    :param enterpriseid: Enterpriseid
    :param qtconn: Qtrain connection object
    :param count: Count
    :return: Enterprise Top Courses
    """

    query = "select courseid, count(courseid) as course_count from tbl_usersubscription \
        inner join tbl_courselist on tbl_usersubscription.courseid = tbl_courselist.id \
        where tbl_usersubscription.enterpriseid=%s and tbl_usersubscription.milestone in (1, 2, 3, 4, 5) \
        and tbl_courselist.courselabel='1' and tbl_courselist.status=1 \
        group by tbl_usersubscription.courseid order by course_count desc limit %s"
    qtconn.execute(query, (str(enterpriseid), count))
    result = list(qtconn.fetchall())
    return result


def find_active_training_course(enterpriseid, qtconn):  # qtrain
    """
    Finds list of active courses in an enterprise
    :param enterpriseid: Enterpriseid
    :param qtconn: Qtrain connection object
    :return: Courseid of all active training courses
    """
    query = "Select id from tbl_courselist where courseLabel='1' and status=1 and enterpriseID=%s"
    qtconn.execute(query, (enterpriseid,))
    result = list(qtconn.fetchall())
    return result


def get_course_names(qtconn, ids=None, coursename_like="", count=0):  # qtrain
    """
    Finds course name using Employee ids
    :param qtconn: Qtrain connection object
    :param ids: CourseIds
    :param coursename_like: CourseName
    :param count: Count
    :return: CourseName,Number of videos,No of assesments
    """
    query = "SELECT tbl.id, tbl.courseName,tbl_lbases.no_of_videos,tbl_lbases.no_of_assesment " + \
            " from (SELECT tbl_courselist.id,tbl_courselist.courseName,tbl_courselbases.lbaseID " + \
            " from tbl_courselist inner join tbl_courselbases on tbl_courselist.id=tbl_courselbases.courseID " + \
            " and tbl_courselist.courselabel='1' and tbl_courselist.status=1 %s)" + \
            " as tbl inner join tbl_lbases on tbl.lbaseID=tbl_lbases.lbaseid %s %s"

    query = query % ("" if not coursename_like else " and tbl_courselist.courseName like '%" + coursename_like + "%'",
                     "" if not ids else " where tbl.id in (%s) " % ids,
                     " limit " + str(count) if count > 0 else "")

    # qtconn.execute(query,(ids, ))
    qtconn.execute(query)
    results = list(qtconn.fetchall())
    output_results = list()
    for result in results:
        footer = ""
        if coursename_like:
            footer = "Tests: " + coursename_like.capitalize()
        else:
            if result[2] is not None and result[2] > 0:
                footer = "Videos: " + str(result[2])
            if result[3] is not None and result[3] > 0:
                if footer:
                    footer = footer + " | "
                footer = footer + "Tests: " + str(result[3])

        output_results.append({
            "course_name": result[1],
            "course_id": result[0],
            "source": "internal",
            "icon": "http://daas.peoplestrong.com/static/images/ps-capabiliti-learning-logo_v6.jpg",
            "heading_1": result[1],
            "heading_2": "Level: All Levels",
            "heading_3": "Rating: 5",
            "footer": footer,
            "user_click_actions": [
                {"logo": "http://daas.peoplestrong.com/static/images/open-link.png",
                 "url": "https://mylearning.peoplestrong.com"}]
        })
    # for result in results:
    return output_results


def find_course_date(enterpriseid, qtconn, todaydate, prevdate, count):  # qtrain
    """
    Finds trending courses filter by date
    :param enterpriseid: Enterpriseid
    :param qtconn: Qtrain Connection Object
    :param todaydate: Todays Date
    :param prevdate: Previous date
    :param count: Count
    :return: Courseid ad Course count
    """
    query = "select courseid, count(courseid) as course_count from tbl_usersubscription \
            inner join tbl_courselist on tbl_usersubscription.courseid = tbl_courselist.id \
            where tbl_usersubscription.enterpriseid=%s and tbl_usersubscription.milestone in (1, 2, 3, 4, 5) \
            and tbl_courselist.courselabel='1' and tbl_courselist.status=1 \
            and tbl_usersubscription.createdon>=%s and tbl_usersubscription.createdon<=%s \
            group by tbl_usersubscription.courseid order by course_count desc limit %s"
    qtconn.execute(query, (str(enterpriseid), prevdate, todaydate, count))
    coursedate = list(qtconn.fetchall())
    return coursedate


def find_enrolled_courses(userid, qtconn):
    """
    Finds Course List enrolled by Userid'
    :param userid: Userid
    :param qtconn: Qtrain Connection object
    :return: List of Enrolled Courses
    """
    query = "SELECT tbl_usersubscription.userid, tbl_usersubscription.courseid from tbl_usersubscription " \
            "inner join tbl_courselist on tbl_usersubscription.courseid = tbl_courselist.id " \
            "and tbl_courselist.courselabel = '1' and tbl_courselist.status = 1 " \
            "where tbl_usersubscription.milestone in (1,2,3,4,5) and tbl_usersubscription.userid=%s"
    qtconn.execute(query, (str(userid),))
    result = list(qtconn.fetchall())
    result_list = [str(i[1]) for i in result]
    return result_list


def find_designation(empcode, tenantid, tpconn):  # TalentPact
    """
    This function finds employee designation'
    :param empcode: Employee Code
    :param tenantid: Tenant id
    :param tpconn: Talentpact Connection object
    :return: Employee Designation
    """
    query = "SELECT hre.DesignationID,hrd.DesignationName from HrEmployee hre inner join HrDesignation hrd " \
            "on hre.DesignationID=hrd.DesignationID where hre.EmployeeCode=%s and hre.TenantID=%s"
    tpconn.execute(query, (str(empcode), tenantid))
    designationid = list(tpconn.fetchall())
    return designationid


def get_empcode_with_same_designation(designationid, tpconn):  # Talentpact
    """
    This fuction is to find peer with same designation'
    :param designationid: Designation ID
    :param tpconn: TalentPact Connection Object
    :return: Peers with same designation
    """
    query = "SELECT EmployeeCode from HrEmployee where DesignationID=%s and EmployeeCode is not NULL"
    tpconn.execute(query, designationid)
    employeecode = list(tpconn.fetchall())
    return employeecode


def list_peer_empcodes(empcode, tenantid, tpconn):  # TalentPact
    """
    This function is to find employee code of peers
    :param empcode: EmployeeCode
    :param tenantid: TenantId
    :param tpconn: Talentpact Connection Object
    :return: list of peers Employeecode
    """
    emplist = list()
    des = find_designation(int(empcode), tenantid, tpconn)
    empcodes = get_empcode_with_same_designation(des[0][0], tpconn)
    for i in empcodes:
        emplist.append(int(i[0]))
    return emplist


def find_peers_userid(employeecode, enterpriseid, qtconn):  # qtrain
    """
    This function is to find userids of peers
    :param employeecode: EmployeeCode
    :param enterpriseid: EnterpriseId
    :param qtconn: Qtrain Connection Object
    :return: Peers userid
    """
    query = "SELECT autoid from tbl_users where client_userid in (%s) and enterpriseID=%s" % (
        employeecode, enterpriseid)
    qtconn.execute(query)
    userids = list(qtconn.fetchall())
    return userids


def find_courses_by_skills(enterpriseid, skill, qtconn, count):
    """
    Find list of courses on the basis of skills'
    :param enterpriseid: EnterpriseId
    :param skill: Skills
    :param qtconn: Qtrain Connection Object
    :param count: Count
    :return: Courses by Skills
    """
    query = "SELECT id as courseid,courseName from tbl_courselist where enterpriseID=%s " \
            "and courseLabel='1' and status=1 and courseName like '%s' LIMIT %s" % (enterpriseid, skill, count)
    qtconn.execute(query)
    return qtconn.fetchall()


def find_courses_of_peers(peer_user_ids, qtconn):  # qtrain
    """
    Finds list of courses enrolled by peers
    :param peer_user_ids: Peer user id
    :param qtconn: Qtrain Connection object
    :return: Enrolled courses by peers
    """
    query = "SELECT tbl_usersubscription.courseid from tbl_usersubscription " \
            "inner join tbl_courselist on tbl_usersubscription.courseid = tbl_courselist.id " \
            "where tbl_courselist.courselabel = '1' and tbl_courselist.status = 1 " \
            "and tbl_usersubscription.milestone in (1,2,3,4,5) and tbl_usersubscription.userid in (%s)" % str(
        peer_user_ids)
    qtconn.execute(query)
    courselist = list(qtconn.fetchall())
    return courselist


if __name__ == "__main__":
    import config
    from common.sql_helpers import connect_to_mysql_server

    MYSQL_CONN = connect_to_mysql_server(config.QTRAIN_DB, config.MYSQL_CREDENTIALS)
    get_course_names(MYSQL_CONN, "4, 615714, 615715")
