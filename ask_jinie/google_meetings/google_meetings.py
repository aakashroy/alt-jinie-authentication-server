import uuid

import config
import requests
from urllib import urlencode
import json
import config
import logging
import urllib
import traceback
from common import mongo_helpers, generic_functions
from datetime import datetime, timedelta
import pytz


def refresh_access_token(user_data):
    print("refresh_access_token")
    print(user_data)
    refresh_token = user_data['google_meetings']['refresh_token']
    scope = user_data.get("scope", "")
    client_id = config.GOOGLE_MEET_CLIENT_ID
    client_secret = config.GOOGLE_MEET_CLIENT_SECRET
    base_endpoint = config.GOOGLE_MEET_TOKEN_URL
    query_data = {'refresh_token': refresh_token,
                  'client_id': client_id,
                  'client_secret': client_secret,
                  'scope': scope,
                  'access_type': 'offline',
                  "grant_type": "refresh_token"
                  }
    url_enpoint = base_endpoint + "?" + urlencode(query_data)
    print(url_enpoint)
    oauth_response = requests.post(base_endpoint, data=json.dumps(query_data)).json()
    print(oauth_response)
    if 'access_token' in oauth_response:
        print("Oauth Response Success")
        oauth_response['refresh_token'] = refresh_token
        oauth_response['token_create_at'] = datetime.now()
        return oauth_response
    else:
        return None


def get_user_data(user_id):
    user_details = mongo_helpers.get_user_details_by_alt_id(user_id, config)
    google_auth_data = None
    if user_details is not None:
        google_auth_data = user_details.get("google_meetings", None)
    if google_auth_data is not None:
        access_token_expires_on = google_auth_data["token_create_at"] + timedelta(
            seconds=int(google_auth_data["expires_in"]))
        access_token_threshold = access_token_expires_on - timedelta(seconds=config.ACCESS_TOKEN_TIMEOUT_THRESHOLD)
        print(access_token_threshold)
        if datetime.now() > access_token_threshold:
            print("Refreshing new token")
            new_access_details = refresh_access_token(user_details)
            if new_access_details is not None:
                update_token_data = mongo_helpers.update_google_meeting_oauth(user_details.get("alt_id"),
                                                                              new_access_details, config)
                if update_token_data is not None:
                    user_details["google_meetings"] = new_access_details
            else:
                user_details = None

    return user_details


def get_tz_aware_meeting_start_end_time(df_start_date_time, user_time_zone):
    if df_start_date_time:
        if not user_time_zone:
            user_time_zone = "Asia/Kolkata"
        date_arr = df_start_date_time.split("T")
        date_parts = date_arr[0].split("-")
        dd = int(date_parts[2])
        mm_mon = int(date_parts[1])
        yy = int(date_parts[0])
        time_parts = date_arr[1].split(":")
        hh = int(time_parts[0])
        mm = int(time_parts[1])
        usertz = pytz.timezone(user_time_zone)
        # local_mtg_start_date = mtg_start_date.astimezone(pytz.timezone(user_time_zone))
        local_mtg_start_date = usertz.localize(datetime(yy, mm_mon, dd, hh, mm))
        utc_mtg_start_date = local_mtg_start_date.astimezone(pytz.utc)
    else:
        utc_mtg_start_date = datetime.utcnow().replace(tzinfo=pytz.utc)

    utc_mtg_end_date = utc_mtg_start_date + timedelta(hours=1)
    return utc_mtg_start_date, utc_mtg_end_date


def get_google_integration_embed_link(user_id, text):
    return ""


def create_google_meeting(user_id, subject, meeting_start_time, meeting_end_time=None, is_internal_call=False):
    try:
        user_details = get_user_data(user_id)
        print(user_details)
    except Exception as e:
        print(e)
        user_details = None
        pass
    if user_details and "google_meetings" in user_details:
        print(user_details)
        if not subject:
            subject = "{}'s Meeting Room".format(user_details["first_name"])
        print(subject)
        print(meeting_start_time)
        if meeting_start_time and meeting_end_time:
            start_date_time = meeting_start_time + "Z"
            end_date_time = meeting_end_time + "Z"
        else:
            utc_meeting_start_time, utc_meeting_end_time = get_tz_aware_meeting_start_end_time(meeting_start_time,
                                                                                               user_details.get(
                                                                                                   "timezone",
                                                                                                   ""))
            start_date_time = utc_meeting_start_time.strftime("%Y-%m-%dT%H:%M:%S%z")
            end_date_time = utc_meeting_end_time.strftime("%Y-%m-%dT%H:%M:%S%z")
            print("START TIME")
            print(start_date_time)
            print(end_date_time)
            # start_date_time = "2020-04-26T17:1:03+05:30"
            # end_date_time = "2020-04-26T18:1:03+05:30"

        result = create_meeting(subject, start_date_time, end_date_time, user_details)
        if result is not None and result.status_code == 200:
            result = result.json()
            print(json.dumps(result, indent=4))
            shorturl = generic_functions.get_bitlink(result["hangoutLink"])
            if is_internal_call:
                return shorturl
            else:
                return "Here's your meeting link. This link is like a bridge, whoever you share it with will be able to join the meeting. " \
                       "You can go ahead share it with the meeting participants :) <br/><br/><a href='{}'>{}</a>".format(
                    shorturl, shorturl)
        else:
            if is_internal_call:
                return None
            else:
                if result.status_code == 401:
                    return "I'm sorry, you need to sign up with Google Meet for this to work. <br/><br/>" \
                           "{}".format(generic_functions.get_google_integration_embed_link(user_id,
                                                                                           "Click Here to Link Google Meetings"))
                else:
                    return "Oops! Looks like you haven't linked your account with Google yet.<br/><br/>" \
                           "{}".format(generic_functions.get_google_integration_embed_link(user_id,
                                                                                           "Click Here to Link Google Meetings"))
    else:
        if is_internal_call:
            return None
        else:
            return "Oops! Looks like you haven't linked your account with Google yet.<br/><br/>" \
                   "{}".format(
                generic_functions.get_google_integration_embed_link(user_id, "Click Here to Link Google Meetings"))


def link_user_account(user_data):
    response = {
        "is_authorized": False,
        "user_name": "User"
    }
    user_id = user_data.get('state')
    auth_code = user_data.get('code')
    print("AuthCOde " + str(auth_code))
    print("State" + str(user_id))
    user_details = None
    if user_id and user_id != "-1":
        user_details = mongo_helpers.get_user_details_by_alt_id(user_id, config)
    if user_details is not None:
        user_name = user_details.get("first_name", "User")
        print("user_data " + user_name)
        client_id = config.GOOGLE_MEET_CLIENT_ID
        client_secret = config.GOOGLE_MEET_CLIENT_SECRET
        redirect_url = config.GOOGLE_MEET_REDIRECT_URL
        base_endpoint = config.GOOGLE_MEET_TOKEN_URL
        query_data = {'code': auth_code,
                      'client_id': client_id,
                      'client_secret': client_secret,
                      'redirect_uri': redirect_url,
                      "grant_type": "authorization_code"
                      }
        url_enpoint = base_endpoint + "?" + urlencode(query_data)
        print(url_enpoint)
        oauth_response = requests.post(base_endpoint, data=json.dumps(query_data)).json()
        print oauth_response
        if "access_token" in oauth_response:
            token_create_at = datetime.now()
            update_data = {
                "access_token": oauth_response["access_token"],
                "expires_in": oauth_response["expires_in"],
                "token_type": oauth_response["token_type"],
                "scope": oauth_response["scope"],
                "token_create_at": token_create_at,
                "refresh_token": oauth_response["refresh_token"]
            }
            update_res = mongo_helpers.update_google_meeting_oauth(user_details.get("alt_id"), update_data, config)
            print(update_res)
            if update_res:
                response["is_authorized"] = True
                response["user_name"] = user_name
    return response


def create_meeting(meeting_subject, start_datetime, end_datetime, user_details):
    """
    Get Event
    :param meeting_subject: Meeting Subject
    :param start_datetime: Meeting start_datetime
    :param user_details: UserDetails
    :return: Result
    """
    result = None
    uuId = str(uuid.uuid1())
    meeting_payload = {
        "conferenceData": {
            "createRequest": {
                "requestId": uuId,
                "conferenceSolutionKey": {
                    "type": "hangoutsMeet"
                }
            }
        },
        "end": {
            "date": None,
            "dateTime": None
        },
        "start": {
            "date": None,
            "dateTime": None
        },
        "attendees": [],
        "summary": meeting_subject,
        "description": meeting_subject
    }
    try:
        user_timezone = user_details.get("timezone", "Asia/Kolkata")
        access_token = user_details['google_meetings']['access_token']
        if access_token is not None:
            access_token = "Bearer " + access_token
            url_endpoint = "https://www.googleapis.com/calendar/v3/calendars/primary/events?conferenceDataVersion=1"
            print(url_endpoint)
            headers = {'User-Agent': 'python_agent/1.0',
                       'Accept': 'application/json',
                       'Content-Type': 'application/json',
                       'Authorization': access_token
                       }
            meeting_payload['start']['dateTime'] = start_datetime
            meeting_payload['end']['dateTime'] = end_datetime
            body = json.dumps(meeting_payload)
            print(body)
            result = requests.post(url_endpoint,
                                   headers=headers,
                                   data=body)
            print(result)

    except Exception as e:
        print(e)
        result = None

    return result


def handle_google_meeting_query(intent, alt_user_id, additional_parameters):
    response = ""
    if intent == "ML_GOOGLE_MEETING":
        try:
            response = create_google_meeting(alt_user_id, "", "")
            print(response)
        except Exception:
            traceback.print_exc()
            response = None
        if not response:
            response = "I'm having trouble communicating with Google meetings servers at the moment. Kindly try again later."
    return response, ""


if __name__ == "__main__":
    # print(get_user_data(2721489))
    print(uuid.uuid1())
    create_google_meeting(2721489, "ML_GOOGLE_MEETING", "")
    # create_google_meeting(2721489, "", None, meeting_end_time=None, is_internal_call=False)
    # print(get_user_details_by_alt_id(2721489, config))
