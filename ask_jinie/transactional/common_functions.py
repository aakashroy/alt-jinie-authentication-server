"""
Contains functions for all queries required by entrypoint
"""
from string import join


def getnamebyid(crsr, employeeid):
    """
    Returns username by its employeeid
    :param crsr: TalentPact Connection object
    :param employeeid: Employeeid
    :return: Name of Employee
    """
    # crsr = sql_helpers.connectToSQLServer('TalentPact')
    query = "SELECT concat(IsNULL(hrper.GivenName,''),' ',IsNULL(hrper.FamilyName,'')) from" \
            " (hremployee hre inner join HrProfile hrp on (hre.ProfileID=hrp.ProfileID)) " \
            "inner join HrPerson as hrper on hrper.PersonID=hrp.PersonID " \
            "where hre.employeeid=%s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def getempsalary(crsr, employeeid):
    """
    Returns salary account number of employee
    :param crsr: Connection to TalentPact Database
    :param employeeid: EmployeeId
    :return: Salary Account Number
    """
    query = "SELECT AccountNo from HrEmployeeBankDetail where isSalaryAccount=1 and EmployeeID=%s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def getworksite(crsr, worksiteid):
    """
    Finds worksite hierarchy
    :param crsr: Connection to TalentPact Database
    :param worksiteid: Worksiteid
    :return: Worksite hierarchy
    """
    query = "SELECT WorksiteHierarchy from HrWorkSite where worksiteid=%s"
    crsr.execute(query, (worksiteid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def my_blood_group(crsr, employeeid):
    """
    Return Employee Blood Group
    :param crsr: Connection to TalentPact Database
    :param employeeid: Employeeid
    :return: BloodGroup
    """
    query = "Select ct.Type from dbo.HrEmployee as hre " \
            "inner join dbo.HrContentType as ct on hre.BloodGroupTypeID=ct.TypeID inner join HrProfile " \
            "as hrp on hrp.ProfileId=hre.ProfileID where EmployeeID=%s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_confirm_status(crsr, typeId):
    """
    Finds getConfirmStatus by typeId
    :param crsr: Connection to TalentPact Database
    :param typeId: TypeId
    :return: ConfirmStatus
    """
    query = "SELECT Type from HrContentType where TypeID=%s"
    crsr.execute(query, (typeId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_grade(crsr, gradeId):
    """
    Finds Grade by gradeId
    :param crsr: Connection to TalentPact Database
    :param gradeId: GradeId
    :return: Grade
    """
    query = "select Grade from TalentPact.dbo.HrGrade where GradeID=%s"
    crsr.execute(query, (gradeId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_joining_date(crsr, timeDimensionId):
    """
    Finds Joining date Joining timeDimensionId
    :param crsr: Connection to TalentPact Database
    :param timeDimensionId: TimeDimensionId
    :return: JoiningDate
    """
    query = "select concat( DayOfMonth,' ',TheMonth, ' ', TheYear) from TimeDimension where TimeDimensionId=%s"
    crsr.execute(query, (timeDimensionId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_confirmation_dueDate(crsr, timeDimensionId):
    """
    Finds Confirmation due date Joining timeDimensionId
    :param crsr: Connection to TalentPact Database
    :param timeDimensionId: TimeDimensionId
    :return: Confirmation Due date
    """
    query = "select concat( DayOfMonth,' ',TheMonth, ' ', TheYear) from TimeDimension where TimeDimensionId=%s"
    crsr.execute(query, (timeDimensionId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_marital_status(crsr, profileId):
    """
    Finds Maritalstatus using personId
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileId
    :return: JoiningDate
    """
    query = "select t1.Type from TalentPact.dbo.HrContentType t1 inner join TalentPact.dbo.HrPerson t2 " \
            "on t1.TypeID = t2.MaritalStatusID inner join TalentPact.dbo.HrProfile t3" \
            " on t3.PersonID=t2.PersonID and t3.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_dateof_mariage(crsr, profileId):
    """
    Finds DateOfMarriage using profileId
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileID
    :return: DateOfMarriage
    """
    query = "select CONVERT(varchar, DateOfMarriage, 107) as DateOfMarriage from  TalentPact.dbo.HrPerson t1" \
            " inner join TalentPact.dbo.HrProfile t2 on t2.PersonID=t1.PersonID and t2.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_gender(crsr, profileId):
    """
    Finds Gender data using profileId
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileId
    :return: Gender details
    """
    query = "select t1.Type from TalentPact.dbo.HrContentType t1 inner join TalentPact.dbo.HrPerson t2 " \
            "on t1.TypeID = t2.GenderID inner join TalentPact.dbo.HrProfile t3" \
            " on t3.PersonID=t2.PersonID and t3.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_birth_place(crsr, profileId):
    """
    Finds Birthplace using profileId
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileID
    :return: BirthPlace
    """
    query = "select BirthPlace from  TalentPact.dbo.HrPerson t1" \
            " inner join TalentPact.dbo.HrProfile t2 on t2.PersonID=t1.PersonID and t2.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_father_name(crsr, profileId):
    """
    Finds FatherName using profileId
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileID
    :return: FatherName
    """
    query = "select FatherName from  TalentPact.dbo.HrPerson t1" \
            " inner join TalentPact.dbo.HrProfile t2 on t2.PersonID=t1.PersonID and t2.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_age(crsr, profileId):
    """
    Finds Age using personId profileID
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileId
    :return: age
    """
    query = "select FLOOR(DATEDIFF(day,CONVERT(datetime, " \
            "concat( t1.DayOfMonth,' ',t1.TheMonth, ' ', t1.TheYear), 106),  GETDATE())/365.25)" \
            " AS datetime from TalentPact.dbo.TimeDimension " \
            "t1 inner join TalentPact.dbo.HrPerson t2 on t1.TimeDimensionId = t2.DateOfBirth inner join" \
            " TalentPact.dbo.HrProfile t3 on t3.PersonID=t2.PersonID and t3.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_date_of_birth(crsr, profileId):
    """
    Finds DateOFBirth using personId profileID
    :param crsr: Connection to TalentPact Database
    :param profileId: ProfileId
    :return: date of birth
    """
    query = "select concat( t1.DayOfMonth,' ',t1.TheMonth, ' ', t1.TheYear) from TalentPact.dbo.TimeDimension " \
            "t1 inner join TalentPact.dbo.HrPerson t2 on t1.TimeDimensionId = t2.DateOfBirth inner join" \
            " TalentPact.dbo.HrProfile t3 on t3.PersonID=t2.PersonID and t3.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_country_of_birth(crsr, profileId):
    """
        Finds CountryOfBirth using profileId
        :param crsr: Connection to TalentPact Database
        :param profileId: ProfileId
        :return: Country name
        """
    query = "select t1.Name from TalentPact.dbo.SysCountry " \
            "t1 inner join TalentPact.dbo.HrPerson t2 on" \
            " t1.CountryID = t2.CountryOfBirth inner join TalentPact.dbo.HrProfile t3" \
            " on t3.PersonID=t2.PersonID and t3.ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_personal_emailid(crsr, profileId):
    """
        Finds Personal EmaidId using profileID
        :param crsr: Connection to TalentPact Database
        :param profileId: ProfileId
        :return: Personal Email
        """
    query = "select PersonalEmailAddress from TalentPact.dbo.HrProfile where ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_mobile_number(crsr, profileId):
    """
        Finds Mobile Number using profileID
        :param crsr: Connection to TalentPact Database
        :param profileId: ProfileId
        :return: Mobile Number
        """
    query = "select MobileNumber from TalentPact.dbo.HrProfile where ProfileID=%s"
    crsr.execute(query, (profileId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_designation(crsr, designationId):
    """
        Finds Designation using designationId
        :param crsr: Connection to TalentPact Database
        :param designationId: DesignationId
        :return: Designation
        """
    query = "select DesignationName from TalentPact.dbo.HrDesignation where DesignationID=%s"
    crsr.execute(query, (designationId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_employee_status(crsr, typeId):
    """
        Finds Employee Status using status Typeid
        :param crsr: Connection to TalentPact Database
        :param typeId: Employee Status Type Id
        :return: Employee Status
        """
    query = "select Type from TalentPact.dbo.HrContentType where TypeID=%s"
    crsr.execute(query, (typeId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_employee_category(crsr, categoryTypeId):
    """
        Finds Employee Category using status CategoryTypeid
        :param crsr: Connection to TalentPact Database
        :param categoryTypeId: CategoryT Type Id
        :return: Employee Category
        """
    query = "select EmployeeCategoryType from TalentPact.dbo.HrEmployeeCategoryType" \
            " where EmployeeCategoryTypeID=%s"
    crsr.execute(query, (categoryTypeId))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_dependent_details(crsr, employeeId):
    """
        Finds Dependent details using EmployeeId
        :param crsr: Connection to TalentPact Database
        :param employeeId: EmployeeId
        :return: Dependent details
        """
    query = "select t2.Name, t1.Type from TalentPact.dbo.HrContentType t1 inner join" \
            " TalentPact.dbo.HrEmployeeDependentDetail t2 on t2.RelationshipTypeID = t1.TypeID " \
            "where t2.EmployeeID=%s"
    returnStr = "<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Relationship<br>"
    crsr.execute(query, (employeeId))
    result = crsr.fetchall()
    if result:
        return result
    return None


def get_emergency_contacts(crsr, employeeId):
    """
        Finds Emergency Contact details using EmployeeId
        :param crsr: Connection to TalentPact Database
        :param employeeId: EmployeeId
        :return: Emergency Contact details
        """
    query = "select ContactName, MobileNo from TalentPact.dbo.HrEmployeeEmergencyContact" \
            " where EmployeeID=%s"
    returnStr = "<br>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Contact Number<br>"
    crsr.execute(query, (employeeId))
    result = crsr.fetchall()
    if result:
        return result
    return None


def get_branch_name(crsr, employeeid):
    """
    Return Employee Bank Branch Name
    :param crsr: Connection to TalentPact Database
    :param employeeid: Employeeid
    :return: BranchName
    """
    query = "select t2.Branch from TalentPact.dbo.HrEmployeeBankDetail t1 inner join" \
            " TalentPact.dbo.HrOrganizationBank t2 " \
            "on t1.BankID= t2.OrganizationBankID and t1.IsSalaryAccount=1  and t1.EmployeeID=%s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_ifsc_code(crsr, employeeid):
    """
    Return Employee Bank IFSC Code
    :param crsr: Connection to TalentPact Database
    :param employeeid: Employeeid
    :return: IFSCCode
    """
    query = "select IFSCCode from TalentPact.dbo.HrEmployeeBankDetail " \
            " where isSalaryAccount=1 and EmployeeID=%s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None


def get_esci_Number(crsr, employeeid):
    """
    Return Employee ESCI Number
    :param crsr: Connection to TalentPact Database
    :param employeeid: Employeeid
    :return: ESI No
    """
    query = " select ESINo from TalentPact.dbo.HrEmployee where EmployeeID= %s"
    crsr.execute(query, (employeeid))
    result = crsr.fetchall()
    if result:
        return result[0][0]
    return None