"""Intents for Transactional Jinie queries"""

INTENT_MAP = {
    'ML_ESCI_NUMBER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getESCINumber',
        'onSuccess': u'Your ESI Number as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your ESI Number saved with us, please raise a ticket!'
    },
    'ML_IFSC_CODE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getIFSCCode',
        'onSuccess': u'Your bank branch IFSC code is : <b>##INTENT_RESPONSE_VALUE##</b>. "Never share your account details with anyone"',
        'onError': u'Oops, looks like we do not have your Bank IFSC Code saved with us, please raise a ticket!'
    },
    'ML_BANK_BRANCHNAME': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getBranchName',
        'onSuccess': u'Your bank branch name is : <b>##INTENT_RESPONSE_VALUE##</b>. "Never share your account details with anyone"',
        'onError': u'Oops, looks like we do not have your Branch Name saved with us, please raise a ticket!'
    },
    'ML_EMERGENCY_CONTACT': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getEmergencyContacts',
        'onSuccess': u'As I can see your  Emergency Contact Details are',
        'onError': u'Oops, looks like we do not have your Emergency Contacts saved with us, please raise a ticket!',
        'design': 'table',
        'theader': {"column_0": "<b>Name</b>", "column_1": "<b>Contact Number</b>"}
    },
    'ML_DEPENDENT_DETAILS': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getDependentDetails',
        'onSuccess': u'Below is a list of people who rely on you as a primary source of income:',
        'onError': u'Oops, looks like we do not have your Dependent details saved with us, please raise a ticket!',
        'design': 'table',
        'theader': {"column_0": "<b>Name</b>", "column_1": "<b>Relationship</b>"}
    },
    'ML_EMPLOYEE_CATEGORY': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeCategoryTypeID',
        'transform': 'getEmployeeCategory',
        'onSuccess': u'Your Employee Category as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Employee Category saved with us, please raise a ticket!'
    },
    'ML_EMPLOYEMENT_STATUS': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmploymentStatusID',
        'transform': 'getEmployeeStatus',
        'onSuccess': u'As I can see your your Employment Status is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Employment Status saved with us, please raise a ticket!'
    },
    'ML_OFFICIAL_EMAIL_ID': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'OfficialEmailAddress',
        'onSuccess': u'As I can see your Official EmailId is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Official EmailId saved with us, please raise a ticket!'
    },
    'ML_DESIGNATION': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'DesignationID',
        'transform': 'getDesignation',
        'onSuccess': u'Your current designation is <b>##INTENT_RESPONSE_VALUE##</b>. Becoming is better than being.',
        'onError': u'Oops, looks like we do not have your Designation saved with us, please raise a ticket!'
    },
    'ML_MOBILE_NUMBER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getMobileNumber',
        'onSuccess': u'As I can see your Mobile Number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Mobile Number saved with us, please raise a ticket!'
    },
    'ML_PERSONAL_EMAIL_ID': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getPersonalEmailId',
        'onSuccess': u'As per our records your personal email I\'d is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Personal EmailId saved with us, please raise a ticket!'
    },
    'ML_COUNTRY_OF_BIRTH': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getCountryOfBirth',
        'onSuccess': u'As I can see your your Country is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Country saved with us, please raise a ticket!'
    },
    'ML_AGE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getAge',
        'onSuccess': u'Your Age as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Age saved with us, please raise a ticket!'
    },
    'ML_DATE_OF_BIRTH': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getDateOfBirth',
        'onSuccess': u'Your Date of Birth as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Date Of Birth date saved with us, please raise a ticket!'
    },
    'ML_FATHER_NAME': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getFatherName',
        'onSuccess': u'As per the information provided by you, your father name is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Father Name date saved with us, please raise a ticket!'
    },
    'ML_BIRTH_PLACE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getBirthPlace',
        'onSuccess': u'As I can see your birth place is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Birth Place date saved with us, please raise a ticket!'
    },
    'ML_GENDER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getGender',
        'onSuccess': u'Your Gender as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Gender details saved with us, please raise a ticket!'
    },
    'ML_DATE_OF_MARRIAGE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getDateofMarriage',
        'onSuccess': u'Your Marriage date as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Marriage date saved with us, please raise a ticket!'
    },
    'ML_MARITAL_STATUS': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ProfileID',
        'transform': 'getMaritalStatus',
        'onSuccess': u'As I can see your Marital status is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Marital status saved with us, please raise a ticket!'
    },
    'ML_CONFIRMATION_DUE_DATE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ConfirmationDateID',
        'transform': 'getConfirmationDueDate',
        'onSuccess': u'As I can see your Confirmation due date is <b>##INTENT_RESPONSE_VALUE##</b> days',
        'onError': u'Oops, looks like we do not have your Confirmation due date saved with us, please raise a ticket!'
    },
    'ML_NOTICE_PERIOD': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'NoticePeriod',
        'onSuccess': u'As I can see your your NoticePeriod is <b>##INTENT_RESPONSE_VALUE##</b> days',
        'onError': u'Oops, looks like we do not have your Notifice Period saved with us, please raise a ticket!'
    },
    'ML_CONFIRMATION_STATUS': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'ConfirmationStatusID',
        'transform': 'getConfirmStatus',
        'onSuccess': u'your current employemnet status stand as <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Confirmation Status saved with us, please raise a ticket!'
    },
    'ML_GRADE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'GradeID',
        'transform': 'getGrade',
        'onSuccess': u'Currently, you are at <b>##INTENT_RESPONSE_VALUE##</b> level',
        'onError': u'Oops, looks like we do not have your Grade saved with us, please raise a ticket!'
    },
    'ML_DATE_OF_JOINING': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'JoiningDateID',
        'transform': 'getJoiningDate',
        'onSuccess': u'Hi, As I can see your joining date is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Grade saved with us, please raise a ticket!'
    },
    'ML_PAN_NUMBER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'PANNo',
        'onSuccess': u'As I can see your Pan number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your PAN Number saved with us, please raise a ticket!'
    },
    'ML_EMPLOYEE_ID': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'onSuccess': u'Your Employee ID as per our records is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Employee ID saved with us, please raise a ticket!'
    },
    'ML_EMPLOYEE_CODE': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeCode',
        'onSuccess': u'As I can see your Employee Code is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Employee Code saved with us, please raise a ticket!'
    },
    'ML_PF_ACCOUNT_NO': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'PFAccountNo',
        'onSuccess': u'As I can see your PF number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your PF Account No. saved with us, please raise a ticket!'
    },
    'ML_ADHAR_NO': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'AdharNo',
        'onSuccess': u'As I can see your AADHAAR number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Aadhaar Number saved with us, please raise a ticket!'
    },
    'ML_UAN_NO': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'UANNumber',
        'onSuccess': u'As I can see your UAN number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your UAN Number saved with us, please raise a ticket!'
    },
    'ML_VOTERID_NO': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'VoterID',
        'onSuccess': u'As I can see your VoterID Number is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your VoterID Number saved with us, please raise a ticket!'
    },
    'ML_L1_MANAGER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'L1ManagerID',
        'transform': 'getName',
        'onSuccess': u'<b>##INTENT_RESPONSE_VALUE##</b> is your L1 manager. Say Hi to him on zippi!',
        'onError': u'Oops, looks like we do not have your L1 Manager saved with us, please raise a ticket!'
    },
    'ML_L2_MANAGER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'L2ManagerID',
        'transform': 'getName',
        'onSuccess': u'<b>##INTENT_RESPONSE_VALUE##</b> is your L2 manager. Say Hi to him on zippi!',
        'onError': u'Oops, looks like we do not have your L2 Manager saved with us, please raise a ticket!'
    },
    'ML_HR_MANAGER': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'HrManagerID',
        'transform': 'getName',
        'onSuccess': u'<b>##INTENT_RESPONSE_VALUE##</b> is your HR manager. Say Hi to him on zippi!',
        'onError': u'Oops, looks like we do not have your HR Manager saved with us, please raise a ticket!'
    },
    'ML_BANK_ACCOUNT': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'getSalary',
        'onSuccess': u'Your bank details are : <b>##INTENT_RESPONSE_VALUE##</b> "Never share your account details with anyone"',
        'onError': u'Oops, looks like we do not have your Salary Bank Account Number saved with us, please raise a ticket!'
    },
    'ML_WORKSITE_DETAILS': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'WorksiteID',
        'transform': 'worksite',
        'onSuccess': u'As I can see your worksite details are <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Worksite Details saved with us, please raise a ticket!'
    },
    'ML_MY_BLOOD_GROUP': {
        'DB': 'TalentPact',
        'DB_Table': 'dbo.HrEmployee',
        'DB_Column': 'EmployeeID',
        'transform': 'bloodgroup',
        'onSuccess': u'As I can see your Blood Group is <b>##INTENT_RESPONSE_VALUE##</b>',
        'onError': u'Oops, looks like we do not have your Blood Group Details saved with us, please raise a ticket!'
    },
    'ML_REQUIRED_BLOOD_GROUP': {
        'onSuccess': u"I have shared the required blood group along with your contact details to donors at your worksite. Interested donors (if found) will get in touch with you.<br/><br/>"
                     "Meanwhile, You can also <br/><br/><a href='https://bit.ly/2H0kIyG'>Click Here to search the national blood bank directory</a><br/><br/>"
                     "Take care!",
        'onSuccess_no': u"Thank you!",
        'onError': u"Oops, I don't think that's a valid blood group!<br/><br/>You can try saying: Need [A+/B+/etc] blood<br/><br/>"
                   "Meanwhile, You can also <br/><br/><a href='https://bit.ly/2H0kIyG'>Click Here to search the national blood bank directory</a><br/><br/>"
                   "Take care!",
    }
}
