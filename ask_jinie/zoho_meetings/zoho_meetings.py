import json
import traceback
import urllib
from datetime import datetime, timedelta

import pytz
import requests
import tinyurl

import config
from common import logging, mongo_helpers
from common.generic_functions import get_zoho_integration_embed_link

zoho_domains = ["zoho.com", "zoho.in"]


def convert_datetime_timezone(dt, source_tz, dest_tz):
    tz_src = pytz.timezone(source_tz)
    tz_dest = pytz.timezone(dest_tz)
    dt = dt.replace(tzinfo=None)
    dt = tz_src.localize(dt)
    dt = dt.astimezone(tz_dest)
    return dt.replace(tzinfo=None)


def zoho_get_authtoken(username, password):
    """
    Get Event
    :param domain: ZOHO Domain ID
    :param username: ZOHO Username
    :param password: ZOHO Password
    :return: Event
    """
    try:
        authTokenMap = {}
        result = None
        url_params = urllib.urlencode({'SCOPE': 'ZohoMeeting/meetingapi', 'EMAIL_ID': username, 'PASSWORD': password})
        # url_endpoint = "https://accounts."+domain +"/apiauthtoken/nb/create?SCOPE=ZohoMeeting/meetingapi" + '?' + url_params
        for api_domain in zoho_domains:
            url_endpoint = "https://accounts." + api_domain + "/apiauthtoken/nb/create?SCOPE=ZohoMeeting/meetingapi&FROM_AGENT=true&EMAIL_ID=" + username + "&PASSWORD=" + password
            print(url_endpoint)
            headers = {'User-Agent': 'python_agent/1.0',
                       'Accept': 'application/json',
                       'Content-Type': 'application/json'}
            response = requests.post(url_endpoint,
                                     headers=headers,
                                     data=json.dumps({}))
            print response.text
            for line in response.text.splitlines():
                if "AUTHTOKEN=" in str(line):
                    token_data = str(line).split("=")
                    authTokenMap["AUTHTOKEN"] = token_data[1]
            if "AUTHTOKEN" in authTokenMap:
                authTokenMap["domain"] = api_domain
                result = authTokenMap
                break
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "Zoho-HELPERS",
                    traceback.format_exc())
    return result


def zoho_create_meeting(meeting_subject, start_datetime, user_details):
    """
    Get Event
    :param meeting_subject: Meeting Subject
    :param start_datetime: Meeting start_datetime
    :param user_details: UserDetails
    :return: Result
    """
    result = None
    try:
        user_timezone = user_details.get("timezone", "Asia/Kolkata")
        zoho_details = user_details.get("zoho_meetings", {})
        domain = zoho_details.get('domain', 'zoho.com')
        authtoken = zoho_details.get('authToken', None)
        if domain in zoho_domains and authtoken is not None:
            base_endpoint = "https://meeting." + domain + "/api/private/json/meetings/createMeeting"
            apiKey = config.ZOHO_MEETING_APIKEY
            url_params = urllib.urlencode(
                {'apikey': apiKey, 'authtoken': authtoken, 'topic': meeting_subject, 'date': start_datetime})
            url_endpoint = base_endpoint + '?' + url_params
            print(url_endpoint)
            headers = {'User-Agent': 'python_agent/1.0',
                       'Accept': 'application/json',
                       'Content-Type': 'application/json'}
            result = requests.post(url_endpoint,
                                   headers=headers,
                                   data=json.dumps({}))

    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "Zoho-HELPERS",
                    traceback.format_exc())

    return result


def get_tz_aware_meeting_start_end_time(df_start_date_time, user_time_zone):
    if df_start_date_time:
        if not user_time_zone:
            user_time_zone = "Asia/Kolkata"
        date_arr = df_start_date_time.split("T")
        date_parts = date_arr[0].split("-")
        dd = int(date_parts[2])
        mm_mon = int(date_parts[1])
        yy = int(date_parts[0])
        time_parts = date_arr[1].split(":")
        hh = int(time_parts[0])
        mm = int(time_parts[1])
        usertz = pytz.timezone(user_time_zone)
        # local_mtg_start_date = mtg_start_date.astimezone(pytz.timezone(user_time_zone))
        local_mtg_start_date = usertz.localize(datetime(yy, mm_mon, dd, hh, mm))
        utc_mtg_start_date = local_mtg_start_date.astimezone(pytz.utc)
    else:
        utc_mtg_start_date = datetime.utcnow().replace(tzinfo=pytz.utc)

    utc_mtg_end_date = utc_mtg_start_date + timedelta(hours=1)
    return utc_mtg_start_date, utc_mtg_end_date


def get_bitlink(longurl):
    try:
        bitly_headers = {
            "Authorization": "Bearer 277160f21fd1818608ace60ae5e803edc47e8b0b"
        }
        response = requests.post("https://api-ssl.bitly.com/v4/shorten",
                                 headers=bitly_headers,
                                 json={

                                     "group_guid": "Bk3q31WMuTW",
                                     "domain": "bit.ly",
                                     "long_url": longurl

                                 })
        shorturl = response.json()["link"]
    except Exception:
        try:
            shorturl = tinyurl.create_one(longurl)
        except Exception:
            shorturl = longurl

    return shorturl


def create_zoho_meeting(user_id, subject, meeting_start_time):
    try:
        user_details = mongo_helpers.get_user_details_by_alt_id(user_id, config)
    except Exception:
        user_details = None
        pass
    if user_details and user_details.has_key('zoho_meetings'):
        if not subject:
            subject = "{}'s Meeting Room".format(user_details["search_fl_name"])
        utc_meeting_start_time, utc_meeting_end_time = get_tz_aware_meeting_start_end_time(meeting_start_time,
                                                                                           user_details.get("timezone",
                                                                                                            ""))
        start_date_time = utc_meeting_start_time.strftime("%Y-%m-%d %H:%M")
        # user_timezone = user_details.get('timezone', "Asia/Kolkata")
        # user_timezone_start_time = convert_datetime_timezone(utc_meeting_start_time, 'UTC', user_timezone)
        # start_date_time = user_timezone_start_time.strftime("%Y-%m-%d %H:%M")
        print("Meeting Start Time " + str(start_date_time))
        result = zoho_create_meeting(subject, start_date_time, user_details)
        zoho_details = user_details.get("zoho_meetings", {})
        user_domain = zoho_details.get('domain', 'zoho.com')
        link_url = str(config.SUBDOMAIN) + "/zoho_login?userId=" + str(user_id)
        if result is not None and result.status_code == 200:
            print(result.json())
            result_json = result.json()
            if 'result' in result_json["response"]:
                meetinid = result_json["response"]["result"]["meeting"]["meetingKey"]
                meetingurl = "https://meeting." + user_domain + "/meeting/join?key=" + meetinid
                shorturl = get_bitlink(meetingurl)
                return "Here's your meeting link. This link is like a bridge, whoever you share it with will be able to join the meeting. " \
                       "You can go ahead share it with the meeting participants :) <br/><br/><a href='{}'>{}</a>".format(
                    shorturl, shorturl)
            elif 'error' in result_json["response"]:
                return "Oops! Looks like you haven't linked your account with Zoho Meetings yet. or you've changed your password.<br/><br/>" \
                       "{}".format(get_zoho_integration_embed_link(user_id, "Click Here to Link Zoho Meetings"))

        else:
            return "Oops! Looks like you haven't linked your account with Zoho Meetings yet, or you've changed your password.<br/><br/>" \
                   "{}".format(get_zoho_integration_embed_link(user_id, "Click Here to Link Zoho Meetings"))
    else:
        return "Oops! Looks like you haven't linked your account with Zoho Meetings yet.<br/><br/>" \
               "{}".format(get_zoho_integration_embed_link(user_id, "Click Here to Link Zoho Meetings"))


def handle_zoho_meeting_query(intent, alt_user_id, additional_parameters):
    response = ""
    if intent == "ML_ZOHO_MEETING":
        try:
            response = create_zoho_meeting(alt_user_id, "", "")
        except Exception:
            traceback.print_exc()
            response = None
        if not response:
            response = "I'm having trouble communicating with Zoom meetings servers at the moment. Kindly try again later."
    return response, ""


if __name__ == '__main__':
    # print(create_zoho_meeting(2721489,"",""))
    # print zoho_get_authtoken("zoho.com","edukondal.gorla@gmail.com","zoho@123")
    print handle_zoho_meeting_query("ML_ZOHO_MEETING", 2721489, {})
