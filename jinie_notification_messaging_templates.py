"""Jinie notification message templates"""
ONBOARDING_MESSAGE = {
    'message_type': -1,
    'destination_alt_user_id': -1,
    'me': '',
    'tokens': ''
}

NOTIFICATION_REQUEST = {
    'message_type': -1,
    'destination_alt_user_id': -1,
    'scheduled_time': None
}
