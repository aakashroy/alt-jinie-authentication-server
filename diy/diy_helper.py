"""Connection to TalentPact and Qtrain Database"""
import pymssql
import time
import traceback
import config
import mysql.connector
import requests
from common import logging, sql_helpers
from common import logging
import json

REATTEMPT_COUNT = 10


def connect_to_mysql_server():
    """
    Connect to MS-SQL server
    :return: Connection object
    """
    start = time.time()
    attempts = 1
    connected_boolean = False
    crsr = None
    MYSQL_CONNECTION_EJABBERD = config.MYSQL_CONNECTION_EJABBERD
    sql_connection_client = None

    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = mysql.connector.connect(host = MYSQL_CONNECTION_EJABBERD["host"],
                                                            user = MYSQL_CONNECTION_EJABBERD["user"],
                                                             passwd = MYSQL_CONNECTION_EJABBERD["passwd"],
                                                              database = MYSQL_CONNECTION_EJABBERD["database"])
            #crsr = sql_connection_client
            connected_boolean = True
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Connection Successful")
        except Exception:
            logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", traceback.format_exc())
            attempts += 1
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Reconnect attempt : {0}".format(attempts))
            time.sleep(5)
    if connected_boolean is False:
        logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", "Failed connecting to SQL Server. Check Error Log")
    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Took: " + str(end - start) + " seconds.")
    return sql_connection_client


def add_dialogflow_token_generator(orgId):
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    orgId = str(orgId)
    sel_query = "select * from ejabberd.TokenGenerator where orgId="+str(orgId)+" and tokenGenerator='DialogFlow'"
    cursor.execute(sel_query, ())
    details = cursor.fetchall()
    if len(details) == 0:
        print("Inserting Dialogflow data")
        insert_query = "INSERT INTO ejabberd.TokenGenerator (orgId, tokenGenerator, paramKey, parmValue, generatorType," \
                       " partnerId) select "+str(orgId)+", tokenGenerator, paramKey, parmValue, generatorType,partnerId from" \
                       " ejabberd.TokenGenerator where orgId=19 and tokenGenerator= 'DialogFlow'"
        cursor.execute(insert_query,())
        mysql_conn.commit()
        requests.get('https://messenger.peoplestrong.com/XmppJinie/reloadTokenGenerator/' + str(orgId))


def add_domain_qilo_token_generator(orgId):
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    orgId = str(orgId)
    sel_query = "select * from ejabberd.TokenGenerator where orgId="+str(orgId)+" and tokenGenerator='qilo' and paramKey='domain'"
    cursor.execute(sel_query, ())
    details = cursor.fetchall()
    if len(details) == 0:
        insert_query = "INSERT INTO ejabberd.TokenGenerator (orgId, tokenGenerator, paramKey, parmValue, generatorType," \
                       " partnerId) select orgId, 'qilo', 'domain', domain, 'REST', '5' from" \
                       " ejabberd.domain_orgId_mapping where orgId="+str(orgId)
        cursor.execute(insert_query,())
        mysql_conn.commit()
        requests.get('https://messenger.peoplestrong.com/XmppJinie/reloadTokenGenerator/' + str(orgId))


def insert_zippi_partner_org(orgId, partnerId,authType):
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    sel_query = "select * from ejabberd.ZippiPartnerOrgAuthMapping where orgId=%s and partnerId=%s"
    cursor.execute(sel_query,(orgId,partnerId))
    details = cursor.fetchall()
    if len(details)==0:
        insert_query = "INSERT INTO ejabberd.ZippiPartnerOrgAuthMapping (orgId, partnerId,authType) VALUES (%s, %s,%s)"
        cursor.execute(insert_query,(orgId,partnerId, authType))
        mysql_conn.commit()
        requests.get('https://messenger.peoplestrong.com/XmppJinie/reloadTokenGenerator/' + str(orgId))


def insert_response_generator(intent, orgId,templateOrgId):
    if templateOrgId == None:
        templateOrgId = 19
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    sel_query = "select orgId, partnerId, intent,empgroup,url,payloadTemplate,contentType,apiKey," \
                "responseTemplate,requestType,methodType,headerTemplate from  ejabberd.JinieResponseGenerator " \
                "where intent=%s and orgId=%s"
    cursor.execute(sel_query,(intent, orgId))
    details = cursor.fetchall()
    if len(details) == 0:
        insert_query = "insert into ejabberd.JinieResponseGenerator (orgId, partnerId, intent,empgroup,url," \
                       "payloadTemplate,contentType,apiKey,responseTemplate,requestType,methodType," \
                       "headerTemplate) select "+str(orgId)+", partnerId, intent,empgroup,url,payloadTemplate," \
                        "contentType,apiKey,responseTemplate,requestType,methodType,headerTemplate " \
                        "from ejabberd.JinieResponseGenerator where intent='"+str(intent)+"' and orgId="+str(templateOrgId)
        cursor.execute(insert_query,())
        mysql_conn.commit()
        print(cursor.rowcount, "record inserted into JinieResponseGenerator. ")
    else:
        sel_query = "select url,payloadTemplate from ejabberd.JinieResponseGenerator where intent=%s and orgId=%s"
        cursor.execute(sel_query, (intent, templateOrgId))
        tempRefdetails = cursor.fetchall()
        if len(tempRefdetails) == 1:
            url = tempRefdetails[0][0]
            payloadTemplate = tempRefdetails[0][1]
            update_query = "update ejabberd.JinieResponseGenerator set url=%s ,payloadTemplate=%s where intent=%s " \
                       "and orgId=%s"
            print "URL"+str(url)
            print payloadTemplate
            cursor.execute(update_query, (url, payloadTemplate, intent, orgId))
            mysql_conn.commit()
            print(cursor.rowcount, "record updated  into JinieResponseGenerator. ")



def update_jinie_form_response_table(strParamData, intent, orgId):
    orgId = str(orgId)
    Type = strParamData[2]
    design_type = strParamData[3]
    api_url = strParamData[4]
    input_json_path = strParamData[5]
    input_json_schema_path = strParamData[6]
    output_schema_path = strParamData[7]
    status = strParamData[8]
    lang = strParamData[9]
    label = strParamData[10]
    partnerId = strParamData[11]
    empgroup = strParamData[12]
    contentType = strParamData[13]
    methodType = strParamData[14]
    headerTemplate = strParamData[15]
    urlMapSchemaPath = strParamData[16]
    headerMapSchemaPath = strParamData[17]
    errorMsg = strParamData[18]
    responseCode = strParamData[19]
    print("Intent "+intent)
    print("orgId " + str(orgId))
    print("design " + design_type)
    print("lang " + lang)
    mysql_conn = connect_to_mysql_server()
    cursor = mysql_conn.cursor()

    sel_query = "select * from  ejabberd.jinie_form_response_mapping where intent=%s and orgId=%s and design_type=%s and lang=%s"
    cursor.execute(sel_query,(intent, orgId,design_type,lang))
    details = cursor.fetchall()
    print(details)
    if len(details) == 0:
        insert_query = "insert into ejabberd.jinie_form_response_mapping(intent,orgId,type,design_type,api_url, input_json_path," \
                       "input_json_schema_path,output_schema_path,status,lang, label,partnerId,empgroup,contentType," \
                       "methodType,headerTemplate,urlMapSchemaPath,headerMapSchemaPath,errorMsg,responseCode)" \
                       "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?.?,?,?,?,?)"
        cursor.execute(insert_query,(intent,orgId,Type, design_type,api_url, input_json_path,
                                     input_json_schema_path,output_schema_path,status,lang, label,partnerId,empgroup,
                                     contentType, methodType,headerTemplate,urlMapSchemaPath,headerMapSchemaPath,
                                     errorMsg,responseCode))
        mysql_conn.commit()
        print(cursor.rowcount, "record inserted into JinieRequestGeneratorParams. ")
    else:
        update_query = "update ejabberd.jinie_form_response_mapping set api_url=%s," \
                        "input_json_path=%s, input_json_schema_path=%s,output_schema_path=%s," \
                       "status=%s, methodType=%s, headerTemplate=%s,urlMapSchemaPath=%s,headerMapSchemaPath=%s," \
                       "errorMsg=%s where orgId=%s and intent=%s and design_type=%s  and lang=%s"
        print(update_query)
        cursor.execute(update_query, (api_url,input_json_path,input_json_schema_path,output_schema_path,
                                      status,methodType,headerTemplate,urlMapSchemaPath,headerMapSchemaPath,
                                      errorMsg,orgId,intent,design_type,lang))
        mysql_conn.commit()
        print(cursor.rowcount, "record update into JinieRequestGeneratorParams. ")


def insert_jinie_form_response_table(intent, orgId,templateOrgId,lang="en"):
    if templateOrgId == None:
        templateOrgId = 19
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    sel_query = "select intent,orgId,type,design_type,api_url, input_json_path," \
                       "input_json_schema_path,output_schema_path,status,lang, label,partnerId,empgroup,contentType," \
                       "methodType,headerTemplate,urlMapSchemaPath,headerMapSchemaPath,errorMsg,responseCode from ejabberd.jinie_form_response_mapping where intent=%s and orgId=%s"
    cursor.execute(sel_query,(intent, orgId))
    details = cursor.fetchall()
    if len(details) == 0:
        insert_query = "insert into ejabberd.jinie_form_response_mapping(intent,orgId,type,design_type,api_url, input_json_path," \
                       "input_json_schema_path,output_schema_path,status,lang, label,partnerId,empgroup,contentType," \
                       "methodType,headerTemplate,urlMapSchemaPath,headerMapSchemaPath,errorMsg,responseCode)" \
                       "select intent,"+str(orgId)+",type,design_type,api_url, input_json_path,input_json_schema_path," \
                       "output_schema_path,status,lang, label,partnerId,empgroup,contentType,methodType,headerTemplate," \
                       "urlMapSchemaPath,headerMapSchemaPath,errorMsg,responseCode from ejabberd.jinie_form_response_mapping" \
                       " where intent =%s and orgId=%s and lang=%s"

        cursor.execute(insert_query,(intent,templateOrgId,lang))
        mysql_conn.commit()
        print(cursor.rowcount, "record inserted into JinieRequestGeneratorParams.")
    else:
        cursor.execute(sel_query, (intent, templateOrgId))
        ref_org_details = cursor.fetchall()
        #print(details)
        for paramData in ref_org_details:
            strParamData = []
            for res in paramData:
                if res is not None:
                    strParamData.append(str(res).encode('utf8'))
                else:
                    strParamData.append("")
            print(strParamData)

            update_jinie_form_response_table(strParamData, intent, orgId)


def update_request_generator_params_items(paramData, intent, orgId):
    partnerId = paramData[1]
    empgroup = paramData[3]
    paramType =paramData[4]
    paramName= paramData[5]
    paramValue= paramData[6]
    valueObject= paramData[7]
    mysql_conn = connect_to_mysql_server()
    cursor = mysql_conn.cursor()
    sel_query = "select * from ejabberd.JinieRequestGeneratorParams where intent=%s and orgId=%s and paramName=%s"
    cursor.execute(sel_query,(intent, orgId,paramName))
    details = cursor.fetchall()
    if len(details) == 0:
        insert_query = "insert into ejabberd.JinieRequestGeneratorParams (orgId, partnerId, intent,empgroup,paramType,paramName,paramValue,valueObject) " \
                       " value (%s, %s,%s,%s,%s,%s,%s,%s)"
        cursor.execute(insert_query,(orgId, partnerId, intent,empgroup,paramType,paramName,paramValue,valueObject))
        mysql_conn.commit()
        print(cursor.rowcount, "record inserted into JinieRequestGeneratorParams. ")
    else:
        update_query = "update ejabberd.JinieRequestGeneratorParams set paramType=%s," \
                        "paramValue=%s, valueObject=%s where orgId=%s and intent=%s and paramName=%s"
        cursor.execute(update_query, (paramType,paramValue,valueObject,orgId,intent,paramName))
        mysql_conn.commit()
        print(cursor.rowcount, "record update into JinieRequestGeneratorParams. ")


def insert_request_generator_params(intent, orgId,templateOrgId):
    if templateOrgId == None:
        templateOrgId = 19
    mysql_conn = connect_to_mysql_server()
    cursor= mysql_conn.cursor()
    sel_query = "select orgId, partnerId, intent,empgroup,paramType,paramName,paramValue,valueObject from ejabberd.JinieRequestGeneratorParams where intent=%s and orgId=%s"
    cursor.execute(sel_query,(intent, orgId))
    details = cursor.fetchall()
    if len(details) == 0:
        insert_query = "insert into ejabberd.JinieRequestGeneratorParams (orgId, partnerId, intent,empgroup,paramType,paramName,paramValue,valueObject) " \
                       "select "+str(orgId)+", partnerId, intent,empgroup,paramType,paramName,paramValue,valueObject " \
                                       "from ejabberd.JinieRequestGeneratorParams where intent='"+str(intent)+"' and orgId="+str(templateOrgId)
        cursor.execute(insert_query,())
        mysql_conn.commit()
        print(cursor.rowcount, "record inserted into JinieRequestGeneratorParams. ")
    else:
        cursor.execute(sel_query, (intent, str(templateOrgId)))
        ref_org_details = cursor.fetchall()
        print(ref_org_details)
        for paramData in ref_org_details:
            update_request_generator_params_items(paramData, intent, orgId)


def update_faq(orgId, intent):
    faq_json = {
            "orgId":str(orgId),
            "intent":str(intent),
            "type":"faq",
            "data":""
    }
    faq_url = config.UPDATE_FAQ_DIRECTORY_API
    sync_header = {
        "content-type": "application/json",
        "authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
    }
    requests.post(faq_url, headers=sync_header,json=faq_json).json()
    return True


def sync_organization(orgId, timeZone="Asia/Kolkata"):
    database = "TalentPact"
    result = False
    print timeZone
    crsr = sql_helpers.connect_to_mssql_server(database,
                                               config.MSSQL_CREDENTIALS)
    query = "select TypeID, rtrim(ltrim([Type])) from TalentPact.dbo.HrContentType where " \
                "OrganizationID=%s and rtrim(ltrim(ContentCategory))='EmploymentStatus'" \
                " and rtrim(ltrim([Type])) = 'Active'";
    if crsr is not None:
        crsr.execute(query, (orgId))
        results = crsr.fetchall()
        if results and results[0][0] is not None and unicode(results[0][0]):
            emp_status = results[0][0]
            sync_json = {
                    "org_id": orgId,
                    "disallowed_domains": [],
                    "employment_status_ids": [emp_status],
                    "motd_remind_after_days": 7,
                    "timezone": timeZone,
                    "sync_enabled": True
                    }
            sync_url = "https://daas.peoplestrong.com/jinie/organization_onboarding"
            sync_header = {
                        "content-type": "application/json",
                        "authorization": "45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"
                        }
            requests.post(sync_url, headers=sync_header,json=sync_json).json()
            result = True
    return result

if __name__ == '__main__':
    #insert_zippi_partner_org(409,1,"PS")
    #insert_request_generator_params("ML_RECOMMEND_COURSES", 409, None)
    #insert_response_generator("ML_RECOMMEND_COURSES", 409, None)
    #requests.get('https://messenger.peoplestrong.com/XmppJinie/reloadTokenGenerator/'+str(409))
    result = sync_organization(451)
    print result
