import pymongo
import config

DISALLOWED_ORGS =[]
ML_FAQ_INTENTS = []
ML_MEETINGS_INTENT = []
STORYLINE_INTENT = []
ML_QILO_INTENT = []
STORYLINE_INTENT_JSON={}


def get_diy_config(sl_version, config):
    conn = pymongo.MongoClient(config.MONGO_CONNECTION)
    database = conn["altjinie"]
    coll = database["diy_config"]
    result = coll.find({"version":sl_version}, {"_id": 0}).limit(1)
    result_json = [x for x in result]
    result_json = result_json[0]
    return result_json

if __name__ == '__main__':
    sl_opts = get_diy_config("2", config)
    print(sl_opts)