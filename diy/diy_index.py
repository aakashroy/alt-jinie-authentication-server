import enable_intents, diy_intent_mapping, diy_helper
import config


def run_diy(postdata):

    org_id = int(postdata.get("org_id", 0))
    intents = postdata.get("intents", [])
    fromOrg = postdata.get("fromOrg", 19)
    sl_version = str(postdata.get("sl_version", "2"))
    sync = postdata.get("sync", False)
    if sync in ["false","False"]:
        sync = False
    sync = bool(sync)
    timeZone = postdata.get("timezone", "Asia/Kolkata")
    data = {}

    diy_config = diy_intent_mapping.get_diy_config(sl_version, config)
    diy_intent_mapping.DISALLOWED_ORGS = diy_config["DISALLOWED_ORGS"]
    diy_intent_mapping.ML_FAQ_INTENTS = diy_config["ML_FAQ_INTENTS"]
    diy_intent_mapping.ML_MEETINGS_INTENT = diy_config["ML_MEETINGS_INTENT"]
    diy_intent_mapping.STORYLINE_INTENT = diy_config["STORYLINE_INTENT"]
    diy_intent_mapping.ML_QILO_INTENT = diy_config["ML_QILO_INTENT"]
    diy_intent_mapping.STORYLINE_INTENT_JSON = diy_config["STORYLINE_INTENT_JSON"]

    dis_allowed_orgs = diy_intent_mapping.DISALLOWED_ORGS
    if sync:
        print("sync is true")
    else:
        print("sync is false")
    if org_id == 0:
        return {"status": False, "message": "Organization is mandatory", "data": data}
    elif str(org_id) in dis_allowed_orgs:
        return {"status": False, "message": "Given organization is not allowed to run diy", "data": data}
    else:
        print("continue to diy")
        if sync:
            diy_helper.sync_organization(org_id, timeZone)
        if len(intents) == 0:
            diy_helper.add_dialogflow_token_generator(org_id)
            intents = diy_intent_mapping.ML_FAQ_INTENTS + diy_intent_mapping.ML_QILO_INTENT + \
                      diy_intent_mapping.STORYLINE_INTENT
        for intent in intents:
            if intent in diy_intent_mapping.ML_FAQ_INTENTS:
                enable_intents.ml_faq_intent_diy(org_id, intent, fromOrg)
            elif intent in diy_intent_mapping.ML_QILO_INTENT:
                enable_intents.ml_qilo_intent_diy(org_id, intent, fromOrg)
            elif intent in diy_intent_mapping.STORYLINE_INTENT:
                jsonData = diy_intent_mapping.STORYLINE_INTENT_JSON.get(intent)
                enable_intents.ml_storyline_intent_diy(org_id,jsonData)
            else:
                enable_intents.ml_faq_intent_diy(org_id, intent, fromOrg)

        return {"status": True, "message": "You successfully enabled faqs", "data":data}


if __name__ == '__main__':
    print "test"
