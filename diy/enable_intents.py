# -*- coding: utf-8 -*-
import diy_helper
#import diy_intent_mapping
import requests
import json
import config


def ml_qilo_intent_diy(orgId, intent,fromOrg):
    diy_helper.insert_zippi_partner_org(orgId, 5, "")
    diy_helper.add_domain_qilo_token_generator(orgId)
    diy_helper.insert_jinie_form_response_table(intent, orgId, fromOrg,"en")


def ml_storyline_intent_diy(orgId, jsonData):
    url = config.ADD_STORYLINE_API
    data = jsonData
    if data:
        data["orgId"] = str(orgId)
        headers = {'Content-type': 'application/json', 'Authorization':'admin@123'}
        requests.post(url, data=json.dumps(data), headers=headers)

def ml_faq_intent_diy(orgId, intent,fromOrg=19):
    diy_helper.insert_zippi_partner_org(orgId, 1, "PS")
    diy_helper.insert_request_generator_params(intent, orgId, fromOrg)
    diy_helper.insert_response_generator(intent, orgId, fromOrg)
    diy_helper.update_faq(orgId,intent)



if __name__ == '__main__':
    #ml_faq_intent_diy(409, "ML_RECOMMEND_COURSES")
    #ml_qilo_intent_diy(409, "QUILO_REPORT",19)
    diy_helper.add_dialogflow_token_generator(400)