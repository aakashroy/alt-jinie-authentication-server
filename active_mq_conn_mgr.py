import stomp


class ActiveMQConnectionManager:
    __stomp_conn = None
    __conn_str = None

    @staticmethod
    def __get_connection(conn_str):
        if ActiveMQConnectionManager.__stomp_conn is not None:
            try:
                if ActiveMQConnectionManager.__stomp_conn.is_connected():
                    return ActiveMQConnectionManager.__stomp_conn
            except:
                ActiveMQConnectionManager.__stomp_conn = None

        if ActiveMQConnectionManager.__stomp_conn is None:
            ActiveMQConnectionManager.__stomp_conn = stomp.Connection(conn_str)
            ActiveMQConnectionManager.__stomp_conn.start()
            ActiveMQConnectionManager.__stomp_conn.connect()

        return ActiveMQConnectionManager.__stomp_conn

    @staticmethod
    def initialize(conn_str):
        ActiveMQConnectionManager.__conn_str = conn_str
        ActiveMQConnectionManager.__get_connection(ActiveMQConnectionManager.__conn_str)

    @staticmethod
    def send(destination, body, headers):
        activemq_conn = ActiveMQConnectionManager.__get_connection(ActiveMQConnectionManager.__conn_str)
        activemq_conn.send(destination=destination,
                           body=body,
                           headers=headers)

    @staticmethod
    def disconnect():
        if ActiveMQConnectionManager.__stomp_conn is not None:
            try:
                ActiveMQConnectionManager.__stomp_conn.disconnect()
            except:
                pass
