import base64
import json
import re
import string
import traceback
import urllib
from datetime import datetime
from multiprocessing.process import Process

import ftfy
import requests
from bs4 import BeautifulSoup
from spacy.lang.en import English

import config
from common import logging
from common.mongo_helpers import get_user_details_from_o365_email_created_subscription_creator_id, \
    get_user_details_from_email, get_external_webhooks_for_module, get_beta_users
from common.o365_helpers import get_user_data, get_email, convert_datetime_timezone
from name_disambiguation import NameDisambiguation

WEBHOOK_USERS_FILTER = set(list([2653237, 3593830, 3593680, 3732315]))

nlp = English()
nlp.add_pipe(nlp.create_pipe('sentencizer'))

PRESERVE_MAILTO_REGEX = re.compile(r"<a\s+href\s*?=\s*?\"\s*?(mailto:[^\"]+)\"[^@]+@([^<]+)<\s*?/a\s*?>",
                                   flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)

EMAIL_SPLITTER_REGEX = re.compile(r"From:.*?Sent:.*?(?:To|Cc):.*?Subject:.*?\n",
                                  flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)

MENTION_REMOVAL_REGEX = re.compile(
    r"{{_mailto:[^_]+_}}",
    flags=re.IGNORECASE)

APPROVAL_REGEX_WITH_MENTION = re.compile(
    r"@{{_mailto:([^_]+)_}}(.*?(?:approv|aprov|aproov)(?:e|al|eal)[^\n\r]*)",
    flags=re.IGNORECASE)

APPROVAL_REGEX = re.compile(r"\b(?:approv|aprov|aproov)(?:e|ed|al|eal)\b",
                            flags=re.IGNORECASE)

HTML_COMMENT_REGEX = re.compile(r"<!--.*?-->", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)

SALUTATION_REGEX = re.compile(r"hi|hello|greetings|dear")

NEWLINE_CORRECT_RGX = re.compile(r"[\n\r]+", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)

APPROVAL_CRITERIA_REGEX_RULES = [
    re.compile(r"(kind|please).*?app?roo?v(e|al)", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE),
    re.compile(r"(require|need|provide).*?(you|an)?.*?app?roo?v(e|al)", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE),
]

NEGATIVE_RULES = [
    # re.compile(r"(don't|do not).*?app?roo?v(e|al)", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)
    re.compile(r"\b(don't|dont|not).*?app?roo?v(e|al)", flags=re.IGNORECASE | re.DOTALL | re.MULTILINE)
]

REMOVE_PUNCTUATION_RGX = re.compile('[%s]' % re.escape(string.punctuation))


def generate_ngrams(s, n):
    # Use the zip function to help us generate n-grams
    # Concatentate the tokens into ngrams and return
    ngrams = zip(*[s[i:] for i in range(n)])
    return [" ".join(ngram) for ngram in ngrams]


def get_meeting_reminder_card_layout(alt_id, greeting, members, creater_name, creator_userid, assignee_name,
                                     assignee_userid, remark,
                                     org_id, mail_id, mail_from, mail_sub, meeting_time, meeting_loc, expiryTime='2'):
    greeting = greeting.encode('ascii', 'ignore')
    remark = remark.encode('ascii', 'ignore')
    mail_sub = mail_sub.encode('ascii', 'ignore')
    if len(mail_sub) > 150:
        mail_sub = mail_sub[0:140] + ". . ."
    if len(remark) > 200:
        remark = remark[0:190] + ". . ."
    mail_sub = base64.urlsafe_b64encode(mail_sub)
    remark = base64.urlsafe_b64encode(remark)
    mail_from = base64.urlsafe_b64encode(mail_from)
    encoded_string_reject = base64.urlsafe_b64encode(
        '{"payload": {"mail_id": "' + mail_id + '", "alt_id": "' + assignee_userid + '",  "approver_email": "' + mail_from + '", "email_sub": "' + mail_sub + '"},"context": "ML_REJECT"}')
    encoded_string_approve = base64.urlsafe_b64encode(
        '{"payload": {"mail_id": "' + mail_id + '", "alt_id": "' + assignee_userid + '", "approver_email": "' + mail_from + '", "email_sub": "' + mail_sub + '"},"context": "ML_APPROVE"}')
    encoded_string_createTask = base64.urlsafe_b64encode(
        '{"payload": {"creator_name": "' + creater_name + '", "creator_userid": "' + creator_userid + '", "title": "' + mail_sub + '", "assignee_userid": "' + assignee_userid + '", "assignee_name": "' + assignee_name + '", "remark": "' + remark + '", "expiryTime": "' + expiryTime + '", "org_id": "' + org_id + '","mail_id": "' + mail_id + '", "approver_email": "' + mail_from + '"}, "context": "ML_CREATE_TASK"}')
    final_layout = {
        "input": {
            "usersList": [
                {
                    "text": [
                        {
                            "design": [
                                {
                                    "buttons": [
                                        {
                                            "messageText": "Approve",
                                            "action": 2,
                                            "label": "Approve",
                                            "content": "__base64__" + encoded_string_approve
                                        },
                                        {
                                            "messageText": "Reject",
                                            "action": 2,
                                            "label": "Reject",
                                            "content": "__base64__" + encoded_string_reject
                                        },
                                        {
                                            "messageText": "Create a task",
                                            "action": 2,
                                            "label": "Create a task",
                                            "content": "__base64__" + encoded_string_createTask
                                        },
                                        {
                                            "label": "Read Email",
                                            "action": 3,
                                            "url": "https://outlook.office.com/mail/deeplink/read/{}?version=2020052401.07&popoutv2=1&leanbootstrap=1".format(
                                                urllib.quote(mail_id)
                                            ),
                                        }
                                    ],
                                    "cellTrackerID": None,
                                    "sequence": None,
                                    "displaySeq": None,
                                    "subType": "UPCOMING_MEETING",
                                    "displayDataType": None,
                                    "questionID": None,
                                    "genericArray": [
                                        {
                                            "metgTime": meeting_time,
                                            "metgTitle": "",
                                            "metgLoc": "Importance: {}".format(meeting_loc),
                                            "type": "CAL",
                                            "membs": members
                                        }

                                    ],
                                    "selectionType": None,
                                    "flow": None,
                                    "label": greeting,
                                    "selectionOptions": None,
                                    "type": "OUTLOOK",
                                    "selectedValue": None
                                }
                            ]
                        }
                    ],
                    "userId": alt_id,
                    "messageType": "NOTIFICATION"
                }
            ],
            "messageType": "NOTIFICATION"
        }
    }
    return final_layout


def run_webhook_handler_process(o365_email):
    print("run_webhook_handler_process Starting")
    try:
        webhooks = get_external_webhooks_for_module("incoming_email", config)
        for webhook in webhooks:
            print("Informing webhook: {}".format(webhook["callback_url"]))
            r = requests.post(webhook["callback_url"],
                              json={
                                  "body": o365_email["body"],
                                  "subject": o365_email["subject"],
                                  "toRecipients": o365_email["toRecipients"],
                                  "from": o365_email["from"],
                                  "sender": o365_email["sender"],
                                  "cc": o365_email["ccRecipients"],
                                  "sentDateTime": o365_email["sentDateTime"],
                                  "lastModifiedDateTime": o365_email["lastModifiedDateTime"],
                                  "receivedDateTime": o365_email["receivedDateTime"],
                                  "createdDateTime": o365_email["createdDateTime"]
                              }, timeout=5)
            print(r.status_code)
        print("run_webhook_handler_process Exiting")
    except Exception:
        print("Exception in run_webhook_handler_process")
        traceback.print_exc()


def handle_incoming_email(mail_data, config, debug=False):
    try:
        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "Received packet (incoming email from o365)")
        if "value" in mail_data:
            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "Have emails to parse")
            for mail in mail_data.get("value", []):
                cstate = mail.get("clientState", "")
                if cstate:
                    user_id = cstate.split("|")
                    if len(user_id) > 1:
                        beta_users = get_beta_users("task_from_email", config)
                        if user_id[1] not in beta_users["user_ids"]:
                            print("Not my email, not my problem!")
                            return
                    else:
                        print("Old client state format, will not process further!")
                        return
                else:
                    print("Old client state format, will not process further!")
                    return

                mail_id = mail.get("resourceData", {}).get("id", "")
                f_o365_user_id = mail.get("resourceData", {}).get("@odata.id", "")
                approver_email = ""
                approval_text = ""
                approval_start_location = -1
                actual_mail_body = ""
                if mail_id and f_o365_user_id:
                    t_o365_user_id = f_o365_user_id.split("/")[1]
                    # logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "Prossesing mail with id",
                    #             {"id": mail_id, "Fo365_user_id": f_o365_user_id, "To365_user_id": t_o365_user_id})
                    destination_user = get_user_details_from_o365_email_created_subscription_creator_id(t_o365_user_id,
                                                                                                        config)
                    if destination_user:
                        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "Email recipient details", {
                            "alt_id": destination_user["alt_id"],
                            "email": destination_user.get("mail_ms", "Unregistered for o365"),
                            "org_id": destination_user["org_id"]
                        })
                        if "mail_ms" not in destination_user:
                            destination_user = None

                    from_user_data = {}
                    o365_email = {}

                    if destination_user and destination_user["org_id"] == 19:
                        destination_user_data = get_user_data(destination_user["alt_id"], config)
                        o365_email = get_email(mail_id, destination_user_data, config, "all")

                        if not debug:
                            if destination_user["alt_id"] in WEBHOOK_USERS_FILTER:
                                proc = Process(target=run_webhook_handler_process,
                                               args=(o365_email,))
                                proc.start()
                            # run_webhook_handler_process(o365_email)

                        mime_email = get_email(mail_id, destination_user_data, config, "uniqueBody")["uniqueBody"][
                            "content"]
                        if not APPROVAL_REGEX.findall(mime_email):
                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                        "This email does not talk about approvals!")
                            continue

                        if "Email Powered by Peoplestrong Zippi" in mime_email or mail_id in destination_user_data.get(
                                "approved_rejected_task_mail_ids", []):
                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                        "Already approved email.")
                            continue

                        mime_email = ftfy.fix_text(mime_email)
                        if len(o365_email["toRecipients"]) == 1:
                            approver_email = o365_email["toRecipients"][0]["emailAddress"]["address"].strip()
                        from_user_email = o365_email["from"]["emailAddress"]["address"]
                        from_user_data = get_user_details_from_email(from_user_email, config)
                        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "TO-AND-FROM",
                                    {
                                        'to': ";".join(
                                            [x["emailAddress"]["address"] for x in o365_email["toRecipients"]]),
                                        'from': from_user_email
                                    }
                                    )
                        if from_user_data and from_user_data["org_id"] == destination_user["org_id"]:
                            # mime_email_body = ""
                            # mime_obj = email.message_from_string(mime_email)
                            # for text_part in mime_obj.walk():
                            #     if text_part.get_content_type() == 'text/plain':
                            #         mime_email_body = mime_email_body + text_part.get_payload(decode=True)
                            mime_email = PRESERVE_MAILTO_REGEX.sub(r"@{{_\1_}}\2", mime_email)
                            soup = BeautifulSoup(HTML_COMMENT_REGEX.sub("", mime_email),
                                                 'html.parser')
                            email_body = ftfy.fix_text(soup.get_text())
                            email_body = NEWLINE_CORRECT_RGX.sub(r"\n", email_body)
                            temp_lines = email_body.splitlines()
                            email_body = list()
                            for temp_line in temp_lines:
                                temp_line = temp_line.strip()
                                if temp_line:
                                    if not temp_line.endswith((".", "?", "!")):
                                        if temp_line[-1] in ["\"", "'"]:
                                            temp_line = temp_line + "."
                                        elif temp_line[-1] in string.punctuation:
                                            temp_line = temp_line[0:len(temp_line) - 1] + "."
                                        else:
                                            temp_line = temp_line + "."
                                    email_body.append(temp_line)
                            email_body = "\n".join(email_body)
                            # mime_obj = email.message_from_string(mime_email)
                            # for text_part in mime_obj.walk():
                            #     if text_part.get_content_type() == 'text/plain':
                            #         email_body = email_body + text_part.get_payload(decode=True)
                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL", "Processing email", {
                                "alt_id": destination_user_data["alt_id"],
                                "email": destination_user_data["mail_ms"],
                                "body": email_body,
                                "from": from_user_data
                            })
                            if email_body:
                                split_contents = EMAIL_SPLITTER_REGEX.split(email_body)
                                if split_contents:
                                    actual_mail_body = split_contents[0]
                                    matches = APPROVAL_REGEX_WITH_MENTION.finditer(actual_mail_body)
                                    if matches:
                                        for matchNum, match in enumerate(matches, start=1):
                                            approver_email = match.group(1).strip()
                                            approval_start_location = match.start() + len(match.group())
                                            break
                                    if approval_start_location < 0:
                                        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                                    "Not found approval using @mention, now checking salutation with smart name lookup...")

                                        salutation = actual_mail_body.splitlines()[0]
                                        new_emails = list()
                                        if approver_email:
                                            new_emails = [approver_email]
                                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                                        "Only one email in to, so we're good!")
                                        else:
                                            salutation = salutation.lower()
                                            salutation = SALUTATION_REGEX.sub("", salutation)
                                            # salutation = salutation.translate(None, string.punctuation)
                                            salutation = REMOVE_PUNCTUATION_RGX.sub("", salutation)
                                            salutation = re.sub(r"\s+", " ", salutation)
                                            salutation = salutation.strip()
                                            if re.findall(r"\b(all|every)", salutation):
                                                logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                                            "Confusing case - more than 1 person addressed")
                                                continue
                                            word_tokens = salutation.split()
                                            query_n_grams = generate_ngrams(word_tokens, 2) + word_tokens

                                            j = 0
                                            while j < len(query_n_grams):
                                                profile, _, _ = NameDisambiguation(query_n_grams[j],
                                                                                   from_user_data["alt_id"],
                                                                                   from_user_data[
                                                                                       "org_id"],
                                                                                   use_neo4j=False).resolve()
                                                if profile:
                                                    names = query_n_grams[j].split(" ")
                                                    new_emails.append(profile["official_email"])
                                                    i = j + 1
                                                    while i < len(query_n_grams):
                                                        for name in names:
                                                            if query_n_grams[i] == name:
                                                                del query_n_grams[i]
                                                                i = i - 1
                                                                break
                                                        i = i + 1
                                                j = j + 1
                                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                                        "Logging salutation and emails. If more than 1 email then I'll ignore",
                                                        {
                                                            "text": salutation,
                                                            "emails": new_emails
                                                        })
                                        if len(new_emails) == 1:
                                            approver_email = new_emails[0].strip()
                                            doc = nlp(actual_mail_body)
                                            running_len = 0
                                            for sent in doc.sents:
                                                matches = APPROVAL_REGEX.finditer(sent.string)
                                                if matches:
                                                    for matchNum, match in enumerate(matches, start=1):
                                                        approval_start_location = running_len + len(sent.string)
                                                running_len = running_len + len(sent.string)
                                    else:
                                        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                                    "Approval matched using @ format!")
                                else:
                                    logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL",
                                                "No body after regex split??")
                            else:
                                logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                            "No further processing since body empty")
                        else:
                            if from_user_data is None:
                                from_user_data = {}
                            logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL",
                                        "Src and Dest Org IDs mismatch or missing",
                                        {
                                            "from_org_id": from_user_data.get("org_id", "Missing"),
                                            "to_org_id": destination_user_data.get("org_id", "Missing")
                                        })
                    else:
                        logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL",
                                    "No user id from o365 id | Or Non PeopleStrong | or Not registered ??")

                    if approver_email and approval_start_location >= 0:
                        proceed = False

                        if not proceed:
                            for email_addr_meta in o365_email["toRecipients"]:
                                eml_addr = email_addr_meta["emailAddress"]["address"]
                                if eml_addr.lower() == approver_email.lower():
                                    proceed = True
                                    break

                        if proceed:
                            newline_count = 0
                            for i in range(approval_start_location - 1, -1, -1):
                                if actual_mail_body[i] == "\n":
                                    newline_count = newline_count + 1
                                    if newline_count >= 2:
                                        break
                                approval_text = actual_mail_body[i] + approval_text
                            approval_text = approval_text.strip()
                            approval_text = MENTION_REMOVAL_REGEX.sub("", approval_text)
                            approval_text = re.sub(r"[ \t]+", " ", approval_text)

                        ## Check negative rules
                        if proceed:
                            for rule in NEGATIVE_RULES:
                                if rule.findall(approval_text):
                                    proceed = False
                                    break
                        ## Check positive rules
                        if proceed:
                            proceed = False
                            for rule in APPROVAL_CRITERIA_REGEX_RULES:
                                if rule.findall(approval_text):
                                    proceed = True
                                    break

                        approver_user_details = None

                        if proceed:
                            proceed = False
                            approver_user_details = get_user_details_from_email(approver_email, config)
                            logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL",
                                        "Approver Details ", {"approver_details": approver_user_details})
                            if approver_user_details["alt_id"] == destination_user["alt_id"]:
                                proceed = True

                        if proceed:
                            email_subject = re.sub(r"\r", '', o365_email["subject"])
                            email_subject = re.sub(r"\n", '', email_subject)
                            email_subject = re.sub(r"\s+", " ", email_subject)
                            email_subject = email_subject.strip()

                            approvee = [
                                {
                                    "name": from_user_data["full_name"],
                                    "id": from_user_data["mail_ms"]
                                }
                            ]

                            last_modified = datetime.strptime(o365_email["lastModifiedDateTime"],
                                                              '%Y-%m-%dT%H:%M:%SZ')

                            greeting = u"<b>{}, you have a request for approval from {}!</b><br/><br/>" \
                                       u"<b> Subject:</b><br/><i>{}</i><br/><br/>" \
                                       u"<b> Summary:</b><br/><i>{}</i>".format(approver_user_details["first_name"],
                                                                                from_user_data["first_name"],
                                                                                email_subject,
                                                                                re.sub(ur"\n", "<br/>",
                                                                                       approval_text))

                            last_modified_local = convert_datetime_timezone(last_modified, "UTC",
                                                                            approver_user_details["timezone"])
                            last_modified_local = last_modified_local.strftime("%a %m %b %I:%M %p")

                            message = get_meeting_reminder_card_layout(str(approver_user_details["alt_id"]),
                                                                       greeting,
                                                                       approvee,
                                                                       from_user_data["full_name"],
                                                                       str(from_user_data["alt_id"]),
                                                                       approver_user_details["full_name"],
                                                                       str(approver_user_details["alt_id"]),
                                                                       approval_text,
                                                                       str(from_user_data["org_id"]),
                                                                       mail_id,
                                                                       approver_email,
                                                                       email_subject,
                                                                       last_modified_local,
                                                                       o365_email['importance'].title())

                            # TODO: This is the only place with http for 45 server hack - need to replicate this ASAP.
                            response = requests.post(
                                url="http://messenger.peoplestrong.com/custom/jinieNotificationRequest",
                                headers={"Content-Type": "application/json",
                                         "Authorization": "JinieNotificationITGGN575"},
                                data=json.dumps((message), separators=(',', ':')),
                                timeout=30)

                            if response.status_code == 200:
                                logging.log(logging.SEVERITY_DEBUG, "INCOMING-MAIL-PROCESSOR-WORKER",
                                            "Approval Notification SENT",
                                            {"approver": approver_user_details["alt_id"], "payload": message})
                            else:
                                logging.log(logging.SEVERITY_CRITICAL, "INCOMING-MAIL-PROCESSOR-WORKER",
                                            "Approval Notification NOT SENT",
                                            {"approver": approver_user_details["alt_id"], "payload": message})
                        else:
                            logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                        "Discovered user is not in recipients list or it is not an approval (false positive) or is not for the intended recipient!")
                    else:
                        logging.log(logging.SEVERITY_DEBUG, "HANDLE-INCOMING-EMAIL",
                                    "Finished processing with NO APPROVAL")
                else:
                    logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL", "Packet with no mail id??")
        else:
            logging.log(logging.SEVERITY_ERROR, "HANDLE-INCOMING-EMAIL", "No emails to parse??")
    except:
        logging.log(logging.SEVERITY_CRITICAL, "HANDLE-INCOMING-EMAIL", "General Exception")
        traceback.print_exc()


if __name__ == "__main__":
    handle_incoming_email(mail_data={
        "value": [
            # {
            #     "resource": "Users/ab1e8695-cdde-496c-a394-6a70d54aab02/Messages/AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHupA-qAAA=",
            #     "subscriptionExpirationDateTime": "2020-01-10T07:30:04+00:00",
            #     "resourceData": {
            #         "@odata.type": "#Microsoft.Graph.Message",
            #         "@odata.etag": "W/\"CQAAABYAAAD9xDscLxLmQJmqlKe+Yz9HAAHt0jR+\"",
            #         "id": "AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHupA-qAAA=",
            #         "@odata.id": "Users/ab1e8695-cdde-496c-a394-6a70d54aab02/Messages/AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHupA-qAAA="
            #     },
            #     "changeType": "created",
            #     "tenantId": "d68fa0a0-f757-4f50-8afb-8ad6a173f358",
            #     "subscriptionId": "d73e498c-33da-4928-a441-52ce2db3902d",
            #     "clientState": "aakash.roy@peoplestrong.com|2205395"
            # },
            # {
            #     "resource": "Users/ab1e8695-cdde-496c-a394-6a70d54aab02/Messages/AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHy8UHxAAA=",
            #     "subscriptionExpirationDateTime": "2020-01-17T16:05:50+00:00",
            #     "resourceData": {
            #         "@odata.type": "#Microsoft.Graph.Message",
            #         "@odata.etag": "W/\"CQAAABYAAAD9xDscLxLmQJmqlKe+Yz9HAAHyHHxJ\"",
            #         "id": "AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHy8UHxAAA=",
            #         "@odata.id": "Users/ab1e8695-cdde-496c-a394-6a70d54aab02/Messages/AAMkAGZiMTdlMWMxLWJlNGItNDBjMi04MTI0LTA2YzE4MjE0MmI5YgBGAAAAAAB5ebAPmtaYTZWRvcOIp7DXBwD9xDscLxLmQJmqlKe_Yz9HAAAAAAEMAAD9xDscLxLmQJmqlKe_Yz9HAAHy8UHxAAA="
            #     },
            #     "changeType": "created",
            #     "tenantId": "d68fa0a0-f757-4f50-8afb-8ad6a173f358",
            #     "subscriptionId": "d73e498c-33da-4928-a441-52ce2db3902d",
            #     "clientState": "aakash.roy@peoplestrong.com|2205395"
            # },
            # {
            #     "resource": "Users/5a573058-d1c1-4284-a4e4-c4f444831495/Messages/AAMkADJhMzljNjY5LThkZDctNGVjMi04ZWEwLWE1YjU0Y2NjNGMyOABGAAAAAAAthtnUvPMgR73mCOU3-IirBwC3G8pKnYeTR4oKYGoAB2x_AAAAAAEMAAC3G8pKnYeTR4oKYGoAB2x_AAGKyZEwAAA=",
            #     "subscriptionExpirationDateTime": "2020-01-17T07:37:06+00:00",
            #     "resourceData": {
            #         "@odata.type": "#Microsoft.Graph.Message",
            #         "@odata.etag": "W/\"CQAAABYAAAC3G8pKnYeTR4oKYGoAB2x+AAGJ/sRj\"",
            #         "id": "AAMkADJhMzljNjY5LThkZDctNGVjMi04ZWEwLWE1YjU0Y2NjNGMyOABGAAAAAAAthtnUvPMgR73mCOU3-IirBwC3G8pKnYeTR4oKYGoAB2x_AAAAAAEMAAC3G8pKnYeTR4oKYGoAB2x_AAGKyZEwAAA=",
            #         "@odata.id": "Users/5a573058-d1c1-4284-a4e4-c4f444831495/Messages/AAMkADJhMzljNjY5LThkZDctNGVjMi04ZWEwLWE1YjU0Y2NjNGMyOABGAAAAAAAthtnUvPMgR73mCOU3-IirBwC3G8pKnYeTR4oKYGoAB2x_AAAAAAEMAAC3G8pKnYeTR4oKYGoAB2x_AAGKyZEwAAA="
            #     },
            #     "changeType": "created",
            #     "tenantId": "d68fa0a0-f757-4f50-8afb-8ad6a173f358",
            #     "subscriptionId": "c3ef55e7-aff2-4dd4-ba71-1f56c571cb0a",
            #     "clientState": "aakash.ahuja@peoplestrong.com|2539417"
            # },
            # {
            #     "resource": "Users/c4b239c2-22bb-40e8-8d4d-423e15d99164/Messages/AAMkADI0YmM4M2IwLWY4MzctNDkyZi1hZWZmLWM4ODAxODdiODJhMQBGAAAAAAC14syDGyyuRqO0SCrg54HCBwBCiVha6EubQbxGc5KNUsIqAAAAAAEMAABCiVha6EubQbxGc5KNUsIqAAFVbs0AAAA=",
            #     "subscriptionExpirationDateTime": "2020-01-17T07:31:07+00:00",
            #     "resourceData": {
            #         "@odata.type": "#Microsoft.Graph.Message",
            #         "@odata.etag": "W/\"CQAAABYAAABCiVha6EubQbxGc5KNUsIqAAFU7s6e\"",
            #         "id": "AAMkADI0YmM4M2IwLWY4MzctNDkyZi1hZWZmLWM4ODAxODdiODJhMQBGAAAAAAC14syDGyyuRqO0SCrg54HCBwBCiVha6EubQbxGc5KNUsIqAAAAAAEMAABCiVha6EubQbxGc5KNUsIqAAFVbs0AAAA=",
            #         "@odata.id": "Users/c4b239c2-22bb-40e8-8d4d-423e15d99164/Messages/AAMkADI0YmM4M2IwLWY4MzctNDkyZi1hZWZmLWM4ODAxODdiODJhMQBGAAAAAAC14syDGyyuRqO0SCrg54HCBwBCiVha6EubQbxGc5KNUsIqAAAAAAEMAABCiVha6EubQbxGc5KNUsIqAAFVbs0AAAA="
            #     },
            #     "changeType": "created",
            #     "tenantId": "d68fa0a0-f757-4f50-8afb-8ad6a173f358",
            #     "subscriptionId": "44e1d148-5187-419a-a2e2-caaf67a5c312",
            #     "clientState": "gorla.edukondalu@peoplestrong.com|2721489"
            # },
            {
                "resource": "Users/836d03af-eaed-4e28-b301-4efd0c9dafc6/Messages/AAMkAGIzNjNkNTRlLTBjMWUtNDdkOS1iYmQ4LTRhYTVjODY1ZGJkYwBGAAAAAADJJH4pbvbwT4QSI3jaTI6SBwAtNp9SsymSR58-ZKQ4S-w9AAAAAAEMAAAtNp9SsymSR58-ZKQ4S-w9AADk01oaAAA=",
                "subscriptionExpirationDateTime": "2020-06-05T07:44:55+00:00",
                "resourceData": {
                    "@odata.type": "#Microsoft.Graph.Message",
                    "id": "AAMkAGIzNjNkNTRlLTBjMWUtNDdkOS1iYmQ4LTRhYTVjODY1ZGJkYwBGAAAAAADJJH4pbvbwT4QSI3jaTI6SBwAtNp9SsymSR58-ZKQ4S-w9AAAAAAEMAAAtNp9SsymSR58-ZKQ4S-w9AADk01oaAAA=",
                    "@odata.id": "Users/836d03af-eaed-4e28-b301-4efd0c9dafc6/Messages/AAMkAGIzNjNkNTRlLTBjMWUtNDdkOS1iYmQ4LTRhYTVjODY1ZGJkYwBGAAAAAADJJH4pbvbwT4QSI3jaTI6SBwAtNp9SsymSR58-ZKQ4S-w9AAAAAAEMAAAtNp9SsymSR58-ZKQ4S-w9AADk01oaAAA=",
                    "@odata.etag": "W/\"CQAAABYAAAAtNp9SsymSR58/ZKQ4S/w9AADkcraM\""
                },
                "changeType": "created",
                "tenantId": "d68fa0a0-f757-4f50-8afb-8ad6a173f358",
                "subscriptionId": "1e658922-bc47-4c1f-b990-c84fe2ef1199",
                "clientState": "ashish.gupta@peoplestrong.com|3593663"
            }
        ]
    }, config=config, debug=True)
