import idea_generation_helper
from common import mongo_helpers
from datetime import datetime
import config

def intent_fulfill(user_id, query_result):
    qt = query_result.get("queryText", "")
    params = query_result.get("parameters", {})
    print(params)
    user_response = params.get("user_response", "")
    question_id = params.get("question_id", "")
    result = "OOPs problem while submitting you idea"
    question_data = idea_generation_helper.get_question_details(question_id)
    if question_data:
        user_ids = question_data.get("user_ids",[])
        org_id = question_data.get("org_id","")
        expire_datetime = question_data.get("expiry_date",datetime.now())
        if user_id not in user_ids:
            result ="Sorry, you have not been invited to answer this question."
        elif expire_datetime< datetime.now():
            result = "Sorry, this question has been archived."
        elif user_response == "":
            question_data = idea_generation_helper.get_question_details(question_id)
            result = question_data["question_text"]
        else:
            user_data =  mongo_helpers.get_user_details_by_alt_id(int(user_id), config)
            user_name = user_data.get("first_name","")
            is_answer_saved = idea_generation_helper.insert_idea_response(question_id, user_id,user_name,org_id,user_response)
            if is_answer_saved:
                if user_response=="NO":
                    result = "Sure, thanks!."
                else:
                    result = "Awesome! Thanks for your idea "+user_name

    return result

if __name__ == "__main__":
    print("Ok")