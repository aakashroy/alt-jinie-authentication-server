from datetime import datetime
import json
from bson.objectid import ObjectId
import base64
import pymongo
import traceback
import config
import requests


def get_question_payload(question_id,user_id,question_lbl):
    try:
        intent = "ML_USER_IDEA"
        yes_json = {"payload":{"question_id":str(question_id)},"context":intent}
        no_json = {"payload": {"question_id": str(question_id),"user_response":"NO"}, "context": intent}
        yes_content = json.dumps(yes_json)
        no_content = json.dumps(no_json)
        yes_content= "__base64__"+base64.b64encode(yes_content)
        no_content = "__base64__"+base64.b64encode(no_content)
        #question_data = get_question_details(question_id)
        notification_payload ={
                    "input": {
                        "messageType": "NOTIFICATION",
                        "usersList": [
                            {
                                "messageType": "NOTIFICATION",
                                "userId": str(user_id),
                                "text": [
                                    {
                                        "design": [
                                            {
                                                "buttons": [
                                                    {
                                                        "messageText": "Yes",
                                                        "action": 2,
                                                        "label": "Yes",
                                                        "content": yes_content
                                                    },
                                                    {
                                                        "messageText": "No",
                                                        "action": 2,
                                                        "label": "No",
                                                        "content": no_content
                                                    }
                                                ],
                                                "label": question_lbl,
                                                "type": "QUESTION"
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                }
        return notification_payload
    except Exception as e:
        print(e)
        return None


def get_question_details(question_id):
    """
    Get Question details by id
    :param question_id: question_id
    :param config: Configuration
    :return: Question Details
    """

    result = None
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        db = conn[config.MONGO_DB_IDEA_GENERATION]
        coll = db["questions"]
        result = coll.find_one({"_id": ObjectId(question_id)})
    except Exception:
        pass
    return result


def insert_question(question_text, user_ids, org_id, expiry_datetime):
    insert_response = {
            "org_id":org_id,
            "question_text":question_text,
            "user_ids":user_ids,
            "group_ids":[],
            "expiry_date":expiry_datetime,
            "created_on": datetime.now()
        }
    result = False
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_IDEA_GENERATION]
        coll = dbdata['questions']
        inserted = coll.insert_one(insert_response)
        if inserted:
            result = True
    except Exception:
        traceback.print_exc()
    return result


def insert_idea_response(question_id, user_id,user_name, org_id, user_response):
    insert_response = {"question_id":question_id,
                    "user_id":user_id,
                    "org_id":org_id,
                    "response_text":user_response,
                    "user_full_name":user_name,
                    "responded_on": datetime.now(),
                    "status": "Open"
                }
    result = False
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_IDEA_GENERATION]
        coll = dbdata['responses']
        inserted = coll.insert_one(insert_response)
        if inserted:
            result = True
    except Exception:
        traceback.print_exc()
    return result

if __name__ == "__main__":
    date_time = datetime.strptime("05-05-2020 10:45","%m-%d-%Y %H:%M")
    question_text = "What kind of marketing campaign should we create around our new products?"
    user_ids = ["2721489","2205395"]
    org_id =19
    expiry_datetime = date_time
    #insert_question(question_text, user_ids, org_id, expiry_datetime)
    question_msg = "I'm Jinie, Nisa's personal productivity assistant. I'm looking for new ideas from you, would you like to share some ideas?"
    payload  = get_question_payload("5eac6d0277bf0c12b97a207f",2721489,question_msg)
    print(payload)
