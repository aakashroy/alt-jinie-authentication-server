""" Project Configuration """
MONGO_CONNECTION = "mongodb://appuser:password@mongo1.peoplestrong.com:27017"

MONGO_DB_ALT_JINIE = "altjinie"
MONGO_DB_IDEA_GENERATION = "ideadb"

MONGO_COLL_USERS = "users"

CLIENT_ID = '2861a4b4-6853-4318-ac15-2a623d10a66b'
CLIENT_SECRET = 'lbWEOHJR12=mkctxX102^(+'

SUBDOMAIN = "https://daas.peoplestrong.com"

ZOHO_MEETING_APIKEY = "8f5a27e7337538c959f62d7f140198c2"


REDIRECT_URI = SUBDOMAIN + '/login/authorized'

AUTHORITY_URL = 'https://login.microsoftonline.com'
AUTH_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/authorize?{0}')
TOKEN_ENDPOINT = '{0}{1}'.format(AUTHORITY_URL, '/common/oauth2/v2.0/token')
MS_GRAPH_ENDPOINT = 'https://graph.microsoft.com/v1.0{0}'

o365_EMAIL_REJECT_ACTION_ENDPOINT=  SUBDOMAIN+ "/actions/o365/emails/reject?id={}&alt_id={}&frm={}&subj={}"
o365_EMAIL_APPROVE_ACTION_ENDPOINT = SUBDOMAIN + "/actions/o365/emails/approve?id={}&alt_id={}&frm={}&subj={}"
create_task_ENDPOINT= SUBDOMAIN+ "/actions/o365/emails/createTask?cn={}&fid={}&tn={}&tai={}&ta={}&rm={}&orgId={}"
SCOPES = [
    'openid',
    'offline_access',  # This is the most imp, for getting refresh token!
    'Calendars.Read',
    'Calendars.Read.Shared',
    'Calendars.ReadWrite.Shared',
    'Calendars.ReadWrite',
    'Contacts.Read',
    'Contacts.Read.Shared',
    'Mail.Read',
    'Mail.Read.Shared',
    'People.Read',
    'User.Read',
    'Mail.Send',
    'OnlineMeetings.ReadWrite'
]  # Add other scopes/permissions as needed.


ACTIVE_MQ_USER_REGISTRATION_INLIMBO_QUEUE = "/queue/user_registration_inlimbo_queue"
ACTIVE_MQ_NOTIFICATION_SENDING_QUEUE = "/queue/notification_sending_queue"
ACTIVE_MQ_INCOMING_EMAIL_QUEUE = "/queue/incoming_email_queue"

ACCESS_TOKEN_TIMEOUT_THRESHOLD = 10 * 60  # In seconds

ACTIVEMQ_CONNECTION = [('10.226.0.76', 61613)]
# ACTIVEMQ_CONNECTION = [('127.0.0.1', 61613)]

WEBSERVER_HOST = "10.226.0.83"
# WEBSERVER_HOST = "localhost"

WEBSERVER_PORT = 7070

FAKE_JINIE_API_FOR_INTERNAL_TESTING = False

MYSQL_CREDENTIALS = {"IP": "13.71.27.119", "PORT": "3306", "USER": "ReportAccess", "PASSWORD": "DaT@f0rAcc355"}

# MSSQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"}
# MSSQL_CREDENTIALS = {"IP": "10.226.0.173", "USER": "AltMApp", "PASSWORD": "H0n35TL1F31SGr@t3"}
MSSQL_CREDENTIALS = {"IP": "10.226.0.187", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"}

TALENTPACT_DB = "TalentPact"
QTRAIN_DB = "qtrain"

DEFAULT_COURSE_RECOMMENDER_SOURCE_OVERRIDE = "MOOC"

RASA_NLU_URL = "http://0.0.0.0:7000/api/nlu_model/"

# Other option is QUEUE to route via queue, IGNORE to ignore or SERVER to handle in-place
EMAIL_HANDLING_LOGIC = "http://10.226.0.45:7070/listeners/emails/o365/created"

SMTP_HOST="email-1.peoplestrong.com"

SMTP_PORT=587

SMTP_PASSWORD="AltData@456"
FAQ_API_AUTHORIZATION="45304E6969AA0605155C7BA0DDA0CA84FBEFD28B"

UPDATE_FAQ_DIRECTORY_API="http://10.226.0.199:7080/update_faq"
ADD_STORYLINE_API="https://messenger.peoplestrong.com/XmppJinie/createDesign"
#UPDATE_FAQ_DIRECTORY_API="http://localhost:7080/update_faq"
#ADD_STORYLINE_API="http://localhost:8080/XmppJinie/createDesign"

GOOGLE_MEET_CLIENT_ID="819810700231-o70194rr9rc39f173ru64oujrfjhtfbe.apps.googleusercontent.com"
GOOGLE_MEET_CLIENT_SECRET="k8GtZCL-tgqOi2Jvoo_hhUSD"
GOOGLE_MEET_REDIRECT_URL="https://outlook.zippi.co/google/authorize"


GOOGLE_MEET_TOKEN_URL = "https://oauth2.googleapis.com/token"
GOOGLE_MEET_SCOPE="https://www.googleapis.com/auth/calendar"
GOOGLE_MEET_LINK_URL="https://accounts.google.com/o/oauth2/v2/auth"

MYSQL_CONNECTION_EJABBERD = {
    "host": "10.226.0.200",
    "user": "ZippiServices",
    "passwd": "Zipp!$073s",
    "database": "ejabberd"}

#MYSQL_CONNECTION_EJABBERD = {
#    "host": "192.168.2.140",
#    "user": "admin",
#    "passwd": "admin@123",
#    "database": "ejabberd"}
