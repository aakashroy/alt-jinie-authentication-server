"""Network Optimization Fuctions"""

import json


def json_dumps_minify(pydict):
    """
    Json Dumps Minify
    :param pydict:
    :return:
    """
    return json.dumps(pydict, separators=(',', ':'))
