"""Abstracts the logic of sending notifications to Jinie"""

import time

from jiniepush import jinie_notifications

import config
from common import logging


def send(alt_id, message):
    """
    Funtion to send notification
    :param alt_id: AltId
    :param message: Message
    :return:
    """
    start = time.time()
    result = False
    try:
        # message = message.replace("\n", "<br/><br/>")
        if config.FAKE_JINIE_API_FOR_INTERNAL_TESTING:
            print " = = = Sent Jinie Notification as follows = = = "
            print " ALT ID (RECEIVER): " + str(alt_id)
            print " MESSAGE: " + message
            print " = = = = = = = = = = = = = = = = = = = = = = = "
            result = True
        else:
            result = jinie_notifications.send_simple(alt_id=str(alt_id), message=message, format_message=False)
            # payload = jinie_notification_builders.build_normal_jinie_notification_payload(alt_id, message)
            # r = requests.post(url="https://altmessenger.peoplestrong.com/AltChatServer/jinieNotificationRequest/1.0",
            #                   headers={"Content-Type": "application/json"},
            #                   data=optimizations.json_dumps_minify(payload))
            # if r.status_code == 200:
            #     result = True
    except Exception:
        pass

    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "JINIE-NOTFN-API-SEND", "Took: " + str(end - start) + " seconds")
    return result


if __name__ == "__main__":
    jinie_notifications.send_simple("2205395", "Check 1 2 3 Testing Jinie Notification Delivery")
