"""Generic Functions"""

import re
import string
from datetime import datetime, timedelta
import requests
import tinyurl

import pytz
from dateutil.parser import parse
from tzlocal import get_localzone


def get_first_name(first_name, middle_name, last_name):
    """
    Return the first name basis the first, middle & last names
    :param first_name: First Name
    :param middle_name: Middle Name
    :param last_name: Last Name
    :return:
    """
    first_name = first_name.strip() if first_name else ""
    middle_name = middle_name.strip() if middle_name else ""
    last_name = last_name.strip() if last_name else ""

    if len(first_name) > 2:
        reqd_first_name = first_name
    else:
        if middle_name:
            reqd_first_name = first_name + " " + middle_name + " " + last_name
        else:
            reqd_first_name = first_name + " " + last_name

    return reqd_first_name


def is_single_word(input_str):
    '''
    :param input_str: string
    :return: if string contains only one word, return True else return False
    '''
    if input_str is not None:
        input_str = input_str.strip()
        word_list = input_str.split(' ')
        if len(word_list) == 1:
            return True
        return False
    return False


def sort_calendar_link_on_time(list_tuples):
    start_time = list()
    for lt in list_tuples:
        date = datetime.strptime(lt[2], "%I:%M %p")
        start_time.append(date)
    start_time = sorted(range(len(start_time)), key=lambda k: start_time[k])
    return start_time


def clean_string(inputstr):
    outputstr = inputstr.lower()
    if isinstance(outputstr, unicode):
        outputstr = translate_punctuation_to_empty(outputstr)
    else:
        outputstr = outputstr.translate(None, string.punctuation)
    outputstr = re.sub(r'\s+', '', outputstr)
    return outputstr.strip()


def translate_punctuation_to_empty(to_translate):
    not_letters_or_digits = u"" + string.punctuation
    translate_table = dict((ord(char), None) for char in not_letters_or_digits)
    return to_translate.translate(translate_table)


def convert_to_utc(datetimeobj):
    utc = datetimeobj.astimezone(pytz.timezone("UTC"))
    return utc


def convert_from_anytz_to_local(datetimestr, timezonestr):
    timezone = pytz.timezone(timezonestr)
    dtm_musecs = datetimestr.split(".")
    tz_aware_dt = timezone.localize(datetime.strptime(dtm_musecs[0], "%Y-%m-%dT%H:%M:%S"))
    return tz_aware_dt.astimezone(get_localzone())


def parse_meeting_timings(meeting_date,
                          meeting_start_time,
                          duration_min,
                          user_tz=None):
    '''
    :param datetime_str: string for datetime object in format  '%Y-%m-%dT%H:%M:%S'
    :param duration_min: meeting duration in minutes
    :return:
    '''
    format = '%Y-%m-%dT%H:%M:%S'

    # if isinstance(duration_min, str):
    #     duration_min = duration_min.lower()
    #     minute_variation = ['minutes', 'mins', 'mins.', 'min', 'min.']
    #     for val in minute_variation:
    #         duration_min = duration_min.replace(val, '')
    #     duration_min = duration_min.strip()
    #
    # duration_min = int(duration_min)
    #
    # if duration_min >= 60:
    #     duration_hour = duration_min / 60
    #     duration_min %= 60
    # else:
    #     duration_hour = 0

    meeting_date = parse(meeting_date.lower().strip())
    meeting_time = parse(meeting_start_time.lower().strip())

    # if meeting_date not in ['today', 'tomorrow']:
    #     meeting_date = parse(meeting_date)
    # elif meeting_date == 'today':
    #     meeting_date = datetime.combine(date.today(), datetime.min.time())
    # elif meeting_date == 'tomorrow':
    #     meeting_date = datetime.combine(date.today(), datetime.min.time()) + timedelta(days=1)
    #
    # meeting_start_time = meeting_start_time.lower()
    # if 'am' in meeting_start_time or 'a.m.' in meeting_start_time or 'a.m' in meeting_start_time:
    #     a_m = True
    # else:
    #     a_m = False
    #
    # if ' ' in meeting_start_time:
    #     meeting_start_time = meeting_start_time.split(' ')[0]
    # else:
    #     am_pm_variation = ['am', 'a.m.', 'am.', 'a.m', 'pm', 'p.m.', 'pm.', 'p.m']
    #     for variation in am_pm_variation:
    #         meeting_start_time = meeting_start_time.replace(variation, '')
    #     meeting_start_time = meeting_start_time.strip()
    #
    # meeting_start_time_components = meeting_start_time.split(':')
    # if len(meeting_start_time_components) == 1:
    #     meeting_start_time_hour = int(meeting_start_time_components[0])
    #     meeting_start_time_min = 0
    # else:
    #     meeting_start_time_hour = int(meeting_start_time_components[0])
    #     meeting_start_time_min = int(meeting_start_time_components[1])
    #
    # if not a_m:
    #     meeting_start_time_hour += 12
    #
    # meeting_date = meeting_date + timedelta(hours=meeting_start_time_hour,
    #                                         minutes=meeting_start_time_min + 23)

    meeting_date = meeting_date.replace(hour=meeting_time.hour, minute=meeting_time.minute, second=meeting_time.second,
                                        microsecond=meeting_time.microsecond)

    # datetime_object = datetime.strptime(datetime_str,format)
    # meeting_date_ist = meeting_date.replace(tzinfo=timezone('Asia/Kolkata'))

    # datetime_object_pacific = datetime_object_ist.astimezone(timezone('US/Pacific'))
    # meeting_date_gmt_start = meeting_date_ist.astimezone(timezone('UTC'))

    if user_tz == None:
        user_tz = "Asia/Kolkata"

    user_tz = pytz.timezone(user_tz)

    meeting_date_gmt_start = meeting_date.astimezone(user_tz)
    meeting_date_gmt_start = meeting_date_gmt_start.astimezone(pytz.utc)

    if meeting_date_gmt_start < pytz.utc.localize(datetime.utcnow()):
        print('requested meeting slot is in the past')
        return None, None

    meeting_date_gmt_end = meeting_date_gmt_start + timedelta(minutes=duration_min)

    return meeting_date_gmt_start.strftime(format), \
           meeting_date_gmt_end.strftime(format)


def convert_list_to_string(item_list):
    '''
    :param item_list:
    :return:
    '''
    item_list = [x.capitalize() for x in item_list]
    if not item_list:
        return None
    if len(item_list) == 1:
        return item_list[0]
    else:
        item_string = ''
        item_string += ', '.join(item_list[:-1])
        item_string += ' and ' + item_list[-1]
        return item_string


def verify_email(email_string):
    '''
    :param email_string: email string
    :return: boolean whether email address is valid
    '''

    email_regex = r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
    if re.search(email_regex, email_string):
        return True
    return False


def get_office_365_integration_link(user_id):
    if not user_id:
        user_id = -1
    return "https://outlook.zippi.co/?user_id={}".format(user_id)


def get_office_365_integration_embed_link(user_id, text=None):
    if not user_id:
        user_id = -1
    link = get_office_365_integration_link(user_id)
    if not text:
        text = link
    return "<a href='{}'>{}<a/>".format(link, text)


def get_zoho_integration_link(user_id):
    if not user_id:
        user_id = -1
    return "https://outlook.zippi.co/zoho_login?user_id={}".format(user_id)


def get_zoho_integration_embed_link(user_id, text=None):
    if not user_id:
        user_id = -1
    link = get_zoho_integration_link(user_id)
    if not text:
        text = link
    return "<a href='{}'>{}<a/>".format(link, text)

def get_google_integration_link(user_id):
    if not user_id:
        user_id = -1
    return "https://outlook.zippi.co/google_meet?user_id={}".format(user_id)


def get_google_integration_embed_link(user_id, text=None):
    if not user_id:
        user_id = -1
    link = get_google_integration_link(user_id)
    if not text:
        text = link
    return "<a href='{}'>{}<a/>".format(link, text)


def get_outlook_teaser(user_id):
    return {
        "custom_design": {
            "design": {
                "dataArray": [
                    {
                        "buttons": [
                            {
                                "action": 3,
                                "label": "Integrate Outlook",
                                "url": get_office_365_integration_link(user_id)
                            },
                            {
                                "action": 2,
                                "content": "__base64__eyJwYXlsb2FkIjp7fSwiY29udGV4dCI6Ik1PVERfTElOS19SRU1JTkRFUiJ9",
                                "label": "Later",
                                "messageText": "Later"
                            }
                        ],
                        "thumbUrl": "https://messenger.peoplestrong.com/MOTD_Teaser.png",
                        "url": "https://messenger.peoplestrong.com/MOTD_Teaser.png"
                    }
                ],
                "displaySeq": "thumbUrl,url,title",
                "label": "Time is precious.Get a quick summary of all your emails and meetings for the day from the Jinie. Linking your Outlook account now.",
                "selectedValue": [],
                "selectionOptions": [],
                "subType": "IMAGE",
                "type": "TEASER"
            }
        }
    }


def get_zoho_teaser(user_id):
    return {
        "custom_design": {
            "design":
                {
                    "buttons": [
                        {
                            "action": 3,
                            "url": get_zoho_integration_link(user_id),
                            "label": "Integrate Zoho Meetings"
                        }
                    ],
                    "type": "QUESTION",
                    "label": "Need to create a shareable meeting link in Zoho?"
                }

        }
    }

def get_bitlink(longurl):
    try:
        bitly_headers = {
            "Authorization": "Bearer 277160f21fd1818608ace60ae5e803edc47e8b0b"
        }
        response = requests.post("https://api-ssl.bitly.com/v4/shorten",
                                 headers=bitly_headers,
                                 json={

                                     "group_guid": "Bk3q31WMuTW",
                                     "domain": "bit.ly",
                                     "long_url": longurl

                                 })
        shorturl = response.json()["link"]
    except Exception:
        try:
            shorturl = tinyurl.create_one(longurl)
        except Exception:
            shorturl = longurls
    return shorturl



if __name__ == "__main__":
    # print(convert_to_utc_timezone('2019-08-29T16:24:15'))
    print(parse_meeting_timings('tomorrow',
                                '3:30 am',
                                "30 min"))
    # print(convert_list_to_string(['a', 'b']))
    # print(verify_email('nisgarg86.com'))
