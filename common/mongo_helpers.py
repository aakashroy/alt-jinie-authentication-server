"""Helper functions to fetch details from MongoDB"""
import traceback

import pymongo
import pytz

import config
from common import logging


def update_org_config(org_data, conf):
    """
    insert onboarding organization record into org_config
    :param alt_id
    :param conf: Config
    :return: org_id
    """
    result = {"status": False, "sync": False, "message": "success"}
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database["org_config"]
        org_id = int(org_data["org_id"])
        org_config_template = {"disallowed_domains": [],
                               "employment_status_ids": [],
                               "timezone": "Asia/Kolkata",
                               "sync_enabled": True,
                               "motd_remind_after_days": True}
        check_count = coll.find({"org_id": org_id}, {}).count()
        if check_count == 0:
            employment_status_ids = org_data.get("employment_status_ids", [])
            if len(employment_status_ids) == 0:
                result['message'] = "employee status ids list should not be empty"
            else:
                org_config_template["employment_status_ids"] = [int(id) for id in employment_status_ids]
                org_config_template["disallowed_domains"] = org_data.get("disallowed_domains", [])
                org_config_template["timezone"] = org_data.get("timezone", "Asia/Kolkata")
                org_config_template["sync_enabled"] = org_data.get("sync_enabled", True)
                org_config_template["motd_remind_after_days"] = int(org_data.get("motd_remind_after_days", 7))
                org_config_template["org_id"] = org_id
                mongo_res = coll.insert_one(org_config_template)
                if mongo_res:
                    result['status'] = True
                    result['sync'] = True
                    result['message'] = "successfully inserted org config data. Syncing is processing"
                else:
                    result['message'] = "Error while inserted org config data"
        else:
            update_fields = {}
            for key in org_data.keys():
                if org_config_template.has_key(key):
                    update_fields[key] = org_data.get(key)
            print update_fields
            if update_fields.has_key("employment_status_ids") and len(update_fields["employment_status_ids"]) == 0:
                result['message'] = "employee status ids list should not be empty"
            else:
                mongo_res = coll.update({"org_id": int(org_id)}, {"$set": update_fields})
                if mongo_res:
                    result['status'] = True
                    result['message'] = "successfully updated org config data"
                else:
                    result['message'] = "Error while updatating organization data"
    except Exception:
        traceback.print_exc()
    return result


def get_user_id_from_emp_id(emp_id, org_id, conf):
    """
        Get orgId from altId
        :param alt_id
        :param conf: Config
        :return: org_id
        """
    result = dict()
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"org_id": org_id, "emp_id": emp_id}).limit(1)
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result[0].get("alt_id", -1)


def get_org_id_from_alt_id(alt_id, conf):
    """
        Get orgId from altId
        :param alt_id
        :param conf: Config
        :return: org_id
        """
    result = dict()
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"alt_id": alt_id}).limit(1)
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result[0].get("org_id", -1)


def get_refresh_token_from_alt_id(alt_id, conf):
    """
        Get orgId from altId
        :param alt_id
        :param conf: Config
        :return: org_id
        """
    result = dict()
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({"alt_id": alt_id}).limit(1)
        result = [x for x in result]
        refresh_token = result[0].get('refresh_token')
    except Exception:
        traceback.print_exc()

    return refresh_token


def get_user_details_from_name(org_id, name, conf):
    """
    Get user details from name
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]

        import re
        email_regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'

        if (re.search(email_regex, name)):
            result = coll.find({
                "$and": [
                    {'search_email': name},
                    {
                        "org_id": org_id
                    }]
            }).limit(2)
        else:
            result = coll.find({
                "$and": [
                    {"$or": [
                        {'search_f_name': name},
                        {'search_fm_name': name},
                        {'search_fl_name': name},
                        {'search_fml_name': name}
                    ]},
                    {
                        "org_id": org_id
                    }]
            }).limit(2)
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result


def get_user_details_from_first_name(name, conf, org_id):
    """
    Get user details from fisrt name
    :param email: Email id
    :param conf: Config
    :param org_id: organization in which to search
    :return: User details
    """
    result = []
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({
            "$and": [
                {'search_f_name': name},
                {'org_id': org_id}
            ]
        })
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result


def get_user_details_from_full_name(name, conf, org_id):
    """
    Get user details from full name
    :param email: Email id
    :param conf: Config
    :param org_id: organization in which to search
    :return: User details
    """
    result = []
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find({
            "$and": [
                {'search_fl_name': name},
                {'org_id': org_id}
            ]
        })
        result = [x for x in result]
    except Exception:
        traceback.print_exc()

    return result


def get_user_details_by_alt_id(alt_id, config):
    """
    Get user details by their altid
    :param alt_id: AltId
    :param config: Configuration
    :return: User Details
    """

    result = None
    try:
        alt_id = int(alt_id)
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        db = conn[config.MONGO_DB_ALT_JINIE]
        coll = db[config.MONGO_COLL_USERS]
        result = coll.find_one({"alt_id": alt_id})
    except Exception:
        pass

    return result


def mongo_update_user_timezone(alt_id, timezone, config):
    """
    Update user token detials
    :param alt_id: AltId
    :param timezone: TimeZone
    :param config: Config
    :return: Updated values
    """

    result = None
    update_values = dict()
    try:
        if timezone in pytz.all_timezones:
            conn = pymongo.MongoClient(config.MONGO_CONNECTION)
            dbdata = conn[config.MONGO_DB_ALT_JINIE]
            coll = dbdata[config.MONGO_COLL_USERS]
            update_values["timezone"] = timezone
            select_clause = {"alt_id": alt_id}
            update_clause = {"$set": update_values}
            coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        traceback.print_exc()

    return result


def update_user_token_details(alt_id, tokens, config):
    """
    Update user token detials
    :param alt_id: AltId
    :param tokens: Tokens
    :param config: Config
    :return: Updated values
    """

    result = None
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]

        update_values = {}
        update_values["Bearer"] = tokens["token_type"]
        update_values["scopes"] = tokens["scope"]
        update_values["expires_in"] = tokens["expires_in"]
        update_values["ext_expires_in"] = tokens["ext_expires_in"]
        update_values["access_token"] = tokens["access_token"]
        update_values["refresh_token"] = tokens["refresh_token"]
        update_values["id_token"] = tokens["id_token"]

        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        traceback.print_exc()

    return result


def get_user_details_from_email(email, config):
    """
    Get user Details from Emailid
    :param email: Emailid
    :param config: Config
    :return: UserDetails
    """

    result = []
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]
        result = coll.find_one({"search_email": email.lower()})
    except Exception:
        traceback.print_exc()

    return result


def update_approved_rejected_email_details(alt_id,approved_rejected_task_email_list, approved_rejected_email_list, approved_or_rejected_email_list,
                                           config, is_approve):
    """
    Update user token detials
    :param alt_id: AltId
    :param tokens: Tokens
    :param config: Config
    :return: Updated values
    """

    result = None
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]

        update_values = {}
        update_values["approved_rejected_mail_ids"] = approved_rejected_email_list
        update_values["approved_rejected_task_mail_ids"]= approved_rejected_task_email_list
        if is_approve:
            update_values["approved_mail_ids"] = approved_or_rejected_email_list
        else:
            update_values["rejected_mail_ids"] = approved_or_rejected_email_list

        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        traceback.print_exc()

    return result

def update_task_email_details(alt_id,approved_rejected_task_email_list, task_email_list,config):

    """
    Update user token detials
    :param alt_id: AltId
    :param tokens: Tokens
    :param config: Config
    :return: Updated values
    """

    result = None
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]

        update_values = {}
        update_values["approved_rejected_task_mail_ids"]= approved_rejected_task_email_list
        update_values["task_mail_ids"] = task_email_list
        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        traceback.print_exc()

    return result


def update_user_with_next_motd_time(userid, datetime):
    """
    Update user token detials
    :param alt_id: AltId
    :param tokens: Tokens
    :param config: Config
    :return: Updated values
    """

    result = None
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]

        update_values = {}
        update_values["motd_remind_after_days"] = datetime

        select_clause = {"alt_id": userid}
        update_clause = {"$set": update_values}

        coll.update_one(select_clause, update_clause)

        result = update_values
    except Exception:
        traceback.print_exc()

    return result


def fetch_subscription_pending_users(orgId, conf):
    """
    Fetches org_mapping pending subscriptions users list from Mongo
    :param conf: Configuration details
    :param orgId: Configuration details
    :return:
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = (coll.find({"org_id": orgId, "access_token": None}, {"alt_id": 1}))
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def fetch_org_configuration(orgId, conf):
    """
    Fetches org_mapping pending subscriptions users list from Mongo
    :param conf: Configuration details
    :param orgId: Configuration details
    :return:
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database["org_config"]
        result = (coll.find({"org_id": orgId, "access_token": None}, {"motd_remind_after_days": 1}))
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "MONGO-HELPERS",
                    traceback.format_exc())

    return result


def user_exist_or_not(userid, conf):
    result = None
    try:
        userid = int(userid)
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = (coll.find_one({"alt_id": userid}))
    except Exception:
        traceback.print_exc()

    return result


def get_user_details_from_o365_email_created_subscription_creator_id(o365_email_created_subscription_creator_id, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database[conf.MONGO_COLL_USERS]
        result = coll.find_one(
            {"o365_email_created_subscription_creator_id": o365_email_created_subscription_creator_id})
    except Exception:
        traceback.print_exc()

    return result


def mongo_update_zoho_meeting_token(alt_id, zoho_meetings_data, config):
    """
    Update user token detials
    :param alt_id: AltId
    :param tauthToken: authToken
    :param config: Config
    :return: Updated values
    """

    result = None
    update_values = dict()
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]
        update_values["zoho_meetings"] = zoho_meetings_data
        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}
        coll.update_one(select_clause, update_clause)
        result = update_values
    except Exception:
        traceback.print_exc()
    return result


def update_google_meeting_oauth(alt_id, google_oauth_data, config):
    """
    Update user token detials
    :param alt_id: AltId
    :param google_oauth_data: google_oauth_data
    :param config: Config
    :return: Updated values
    """

    result = None
    update_values = dict()
    try:
        conn = pymongo.MongoClient(config.MONGO_CONNECTION)
        dbdata = conn[config.MONGO_DB_ALT_JINIE]
        coll = dbdata[config.MONGO_COLL_USERS]
        update_values["google_meetings"] = google_oauth_data
        select_clause = {"alt_id": alt_id}
        update_clause = {"$set": update_values}
        res = coll.update_one(select_clause, update_clause)
        result = res
    except Exception:
        traceback.print_exc()
    return result


def get_external_webhooks_for_module(module_name, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    results = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database["external_webhooks"]
        results = coll.find({"module": module_name})
    except Exception:
        traceback.print_exc()

    return results


def get_beta_users(feature_name, conf):
    """
    Get user details from email
    :param email: Email id
    :param conf: Config
    :return: User details
    """
    result = None
    try:
        conn = pymongo.MongoClient(conf.MONGO_CONNECTION)
        database = conn[conf.MONGO_DB_ALT_JINIE]
        coll = database["beta_testing"]
        result = coll.find_one({"feature": feature_name})
    except Exception:
        traceback.print_exc()

    return result


if __name__ == "__main__":
    print(get_user_details_by_alt_id(2653216, config))
    # print(get_user_details_by_alt_id(2721489, config))
