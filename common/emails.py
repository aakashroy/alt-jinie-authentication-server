import smtplib
import traceback
import config

EMAIL_TEMPLATE = """From: %s <%s>
To: %s
MIME-Version: 1.0
Content-type: text/html
Subject: %s

%s
"""


def send_mail(login_email, sender_email, friendly_sender_name, messages, options=None):
    smtp_server = None
    response = {'status': False, 'message': ''}
    if not sender_email and not isinstance(sender_email, str):
        response['message'] = "Parameter sender_email should be string or unicode."
    elif not friendly_sender_name and not isinstance(friendly_sender_name, str):
        response['message'] = "Parameter friendly_sender_name should be string or unicode."
    elif not isinstance(messages, list):
        response['message'] = "Parameter messages should be a list."
    elif login_email and sender_email:
        try:
            smtp_server = smtplib.SMTP(host=config.SMTP_HOST, port=config.SMTP_PORT)
            smtp_server.starttls()
            smtp_server.login(login_email, config.SMTP_PASSWORD)
            results = []
            success = True
            for message in messages:
                smtp_message = EMAIL_TEMPLATE % (
                    friendly_sender_name, sender_email, message['recipient_email'], message['subject'], message['body'])
                response = smtp_server.sendmail(from_addr=sender_email, to_addrs=message['recipient_email'],
                                                msg=smtp_message)
                # response is empty if everything went well
                if response:
                    success = False
                results.append(response)

            if success:
                response = {'status': True, 'message': ''}
            else:
                response['message'] = str(results)

        except Exception:
            response['message'] = traceback.format_exc()
    else:
        response['message'] = "Sender email is incorrect. You should know the sender email to use this function!"

    try:
        if smtp_server:
            smtp_server.quit()
    except Exception:
        pass

    return response


def _test_send_mail():
    # sender_email="altml@alt.peoplestrong.com"
    result = send_mail(login_email="altml@alt.peoplestrong.com",
                       sender_email="vishal.saha@peoplestrong.com",
                       friendly_sender_name="Vishal Saha",
                       messages=[{
                           "recipient_email": "gorla.edukondalu@peoplestrong.com",
                           "subject": "Testing email from Python",
                           "body": "Hello"
                       }])

    # result = send_mail(login_email="support@zippi.co",
    #               sender_email="support@zippi.co",
    #               friendly_sender_name="ZippiTest",
    #               messages=[{
    #                   "recipient_email": "gorla.edukondalu@peoplestrong.com",
    #                   "subject": "Testing email from Python",
    #                   "body": "Hello"
    #               }])
    print(result)


if __name__ == "__main__":
    _test_send_mail()
