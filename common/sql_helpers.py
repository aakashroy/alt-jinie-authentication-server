"""Helper Functions to create connection objects of different DB Endpoints"""

import pymssql
import time
import traceback

import MySQLdb

from common import logging

REATTEMPT_COUNT = 10


def connect_to_mssql_server(database, sql_credentials):
    """
    Create connection with MS-SQL server
    :param database: Database Name
    :param sql_credentials: SQL Credentials
    :return: Connection Object
    """
    start = time.time()
    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = pymssql.connect(sql_credentials["IP"], sql_credentials["USER"],
                                                    sql_credentials["PASSWORD"], database)
            crsr = sql_connection_client.cursor()
            connected_boolean = True

            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Connection Successful")

        except Exception:
            logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", traceback.format_exc())
            attempts += 1
            logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Reconnect attempt : {0}".format(attempts))
            time.sleep(5)

    if connected_boolean is False:
        logging.log(logging.SEVERITY_ERROR, "SQL-CONNECTOR", "Failed connecting to SQL Server. Check Error Log")

    end = time.time()
    logging.log(logging.SEVERITY_DEBUG, "SQL-CONNECTOR", "Took: " + str(end - start) + " seconds.")

    return crsr


def connect_to_mysql_server(database, sql_credentials):
    """
      Create connection with MY-SQL server
      :param database: Database Name
      :param sql_credentials: SQL Credentials
      :return: Connection Object
      """

    attempts = 1
    connected_boolean = False
    crsr = None
    while attempts <= REATTEMPT_COUNT and connected_boolean is False:
        try:
            sql_connection_client = MySQLdb.connect(sql_credentials["IP"], sql_credentials["USER"],
                                                    sql_credentials["PASSWORD"], database)
            crsr = sql_connection_client.cursor()
            connected_boolean = True
            # print("MYSQL Server connection successful")
            # addToLogFile("SQL Server connection successful")
        except Exception as ex:
            print "Error:", ex.message
            # addtoErrorFile("SQL Server Connection Failed : {0}".format(ex.message))
            attempts += 1
            print "Reconnect attempt : {0}".format(attempts)
            time.sleep(5)

    if connected_boolean is False:
        print "Failed connecting to SQL Server. Check Error Log"
    else:
        return crsr


if __name__ == "__main__":
    connect_to_mssql_server("TalentPact",
                            {"IP": "10.226.0.187", "USER": "JinieProdUsr", "PASSWORD": "J!n!ePr0dU$r@867"})
