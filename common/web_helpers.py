"""Web Helpers"""
import uuid

import requests

from common import optimizations


def make_api_call(method, url, token, payload=None, parameters=None):
    """
    Helps in Making o365 API Calls
    :param method: method
    :param url: url
    :param token: token
    :param payload: payload
    :param parameters: parameters
    :return: response
    """
    # Send these headers with all Api Calls
    headers = {'User-Agent': 'python_tutorial/1.0',
               'Authorization': 'Bearer {0}'.format(token),
               'Accept': 'application/json'}

    # Use these headers to instrument calls. Makes it easier
    # to correlate requests and responses in case of problems
    # and is a recommended best practice.
    request_id = str(uuid.uuid4())
    instrumentation = {'client-request-id': request_id,
                       'return-client-request-id': 'true'}

    headers.update(instrumentation)

    response = None

    if method.upper() == 'GET':
        response = requests.get(url, headers=headers, params=parameters)
    elif method.upper() == 'DELETE':
        response = requests.delete(url, headers=headers, params=parameters)
    elif method.upper() == 'PATCH':
        headers.update({'Content-Type': 'application/json'})
        response = requests.patch(url, headers=headers, data=optimizations.json_dumps_minify(payload),
                                  params=parameters)
    elif method.upper() == 'POST':
        headers.update({'Content-Type': 'application/json'})
        response = requests.post(url, headers=headers, data=optimizations.json_dumps_minify(payload), params=parameters)

    return response
