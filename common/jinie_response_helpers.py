"""Function(s) to construct responses to Jinie with different layouts"""

from jiniepush import jinie_api_responses

import ask_jinie.calendars
import ask_jinie.course_recommendation
import ask_jinie.msteams_meetings
import ask_jinie.zoho_meetings
import ask_jinie.performance
import ask_jinie.transactional
from common import logging


def create_jinie_response(intent, json_response):
    """
    Create Response for jinie
    :param intent: Intent
    :param json_response: Json Response
    :return: Response
    """
    if intent in ask_jinie.course_recommendation.intent_mapping.INTENT_MAP.keys():
        if json_response["success"] is True and json_response["results"]:
            response_structure = jinie_api_responses.get_cards("Below are the courses", json_response["results"])
        else:
            response_structure = jinie_api_responses.get_simple("Sorry,there are no courses to display.")
    elif intent in ask_jinie.transactional.intent_mapping.INTENT_MAP.keys():
        if json_response["success"] is True:
            final_message = json_response["results"]
            if 'design' in ask_jinie.transactional.intent_mapping.INTENT_MAP[intent].keys() \
                    and ask_jinie.transactional.intent_mapping.INTENT_MAP[intent]['design'] == 'table':
                theader = final_message["theader"]
                tbody = final_message["tbody"]
                label = final_message["label"]
                if tbody is not None:
                    response_structure = jinie_api_responses.get_table(theader, tbody, label)
                else:
                    response_structure_data = jinie_api_responses.get_table(theader, [], label)
                    response_structure_data["data"]["genericRequest"]["text"][1]["design"]["genericArray"] = []
                    response_structure = response_structure_data
            else:
                response_structure = jinie_api_responses.get_simple(final_message)
        else:
            response_structure = jinie_api_responses.get_simple("Oops some error occurred. Please try after sometime.")
    elif intent in ask_jinie.calendars.intent_mapping.INTENT_MAP.keys():
        if json_response["success"] is True and json_response["results"]:
            final_message = json_response["results"]
            if isinstance(final_message, dict):
                response_structure = final_message

            else:
                response_structure = {"messageCodeTO": {"status": "SUCCESS", "code": "S100",
                                                        "displayMsg": final_message}}
        else:
            response_structure = jinie_api_responses.get_simple("Oops some error occurred. Please try after sometime.")
    elif intent in ask_jinie.performance.intent_mapping.INTENT_MAP.keys():
        if isinstance(json_response["results"], basestring):
            return {"messageCodeTO": {"status": "SUCCESS", "code": "S100",
                                      "displayMsg": json_response["results"]}}
        return {
            "data": {
                "FAQ": {
                    "status": "Success",
                    "message": "Recommended courses for you.",
                    "code": "EC200",
                    "data": json_response["results"]
                }
            },
            "messageCodeTO": {
                "status": "Success",
                "message": "Request accepted successfully.",
                "code": "EC200",
                "data": None
            }
        }
    elif intent in ask_jinie.msteams_meetings.intent_mapping.INTENT_MAP.keys():
        response_structure = jinie_api_responses.get_simple(json_response["results"])
    elif intent in ask_jinie.zoho_meetings.intent_mapping.INTENT_MAP.keys():
        response_structure = jinie_api_responses.get_simple(json_response["results"])
    elif intent in ask_jinie.google_meetings.intent_mapping.INTENT_MAP.keys():
        response_structure = jinie_api_responses.get_simple(json_response["results"])
    else:
        response_structure = jinie_api_responses.get_simple(
            "Sorry, I did not understand your query. Please raise a helpdesk ticket!")

    logging.log(logging.SEVERITY_DEBUG, "SENDING-JINIE-RESPONSE-STRUCTURE", "Jinie response structure",
                {'response-structure': response_structure})

    return response_structure
