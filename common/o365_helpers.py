# coding=utf-8
"""O365 helpers"""
# Note that disable=no-member has been used for pylint because pylint fails to detect
# more nested members
import json
import traceback
import uuid
from datetime import datetime, timedelta

import pytz
import requests

from common import web_helpers, mongo_helpers, logging

TIME_ZONE = 'UTC'


def refresh_access_token(refresh_token, config, scopes):
    """
    Refresh Access Token
    :param refresh_token: Refresh Token
    :param config: Config
    :return:
    """
    result = None
    try:
        post_data = {'grant_type': 'refresh_token',
                     'refresh_token': refresh_token,
                     'redirect_uri': config.REDIRECT_URI,
                     # 'scope': ' '.join(unicode(i) for i in config.SCOPES),
                     'scope': scopes,
                     'client_id': config.CLIENT_ID,
                     'client_secret': config.CLIENT_SECRET}

        req = requests.post(config.TOKEN_ENDPOINT, data=post_data)
        result = req.json()
        if 'access_token' not in result:
            logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                        "Received different result from expected: " + unicode(result))
            result = None
    except Exception:
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())
    return result


def get_user_data(alt_id, config):
    """
    Get user data
    :param alt_id: Altid
    :param config: Config
    :return: user data
    """
    user_data = mongo_helpers.get_user_details_by_alt_id(alt_id, config)
    if user_data is not None:
        access_token_expires_on = user_data["updated_on"] + timedelta(seconds=user_data["expires_in"])
        access_token_threshold = access_token_expires_on - timedelta(seconds=config.ACCESS_TOKEN_TIMEOUT_THRESHOLD)
        if datetime.now() > access_token_threshold:
            new_tokens = refresh_access_token(user_data['refresh_token'], config, user_data["scopes"])
            if new_tokens is not None:
                update_tokens = mongo_helpers.update_user_token_details(alt_id, new_tokens, config)
                if update_tokens is not None:
                    user_data.update(update_tokens)
                else:
                    return None
            else:
                # Handle the mechanism to send URL notification to user again
                # Need to handle specific error codes
                return None
    return user_data


def find_meeting_times_o365(sender_data, config, attendees, timeconstraint, duration, max_candidates=5):
    fmt_url = config.MS_GRAPH_ENDPOINT.format('/me/findMeetingTimes')
    query_parameters = {
        "maxCandidates": max_candidates,
        "minimumAttendeePercentage": 100,
        "meetingDuration": duration
    }
    query_parameters.update(attendees)
    query_parameters.update(timeconstraint)
    # query_parameters = {'timeConstraint': {'activityDomain': 'work', 'timeslots': [
    #     {'start': {'timeZone': 'UTC', 'dateTime': '2019-09-01T18:30:00'},
    #      'end': {'timeZone': 'UTC', 'dateTime': '2019-09-06T18:29:59'}}]}, 'maxCandidates': 5,
    #                     'attendees': [{'emailAddress': {'address': u'aakash.roy@peoplestrong.com'}, 'type': 'required'},
    #                                   {'emailAddress': {'address': u'hartaran.singh@peoplestrong.com'},
    #                                    'type': 'required'},{'emailAddress': {'address': u'hartaranpanesar@gmail.com'},
    #                                    'type': 'required'}],
    #                     'meetingDuration': 'PT45M', 'minimumAttendeePercentage': 100}
    req = web_helpers.make_api_call('POST', fmt_url, sender_data['access_token'],
                                    payload=query_parameters)
    if req.status_code == requests.codes.ok:
        return req.json()
    return None


def o365_meeting_handler(config,
                         attendees_list,
                         start_time,
                         end_time,
                         token,
                         subject,
                         location,
                         mail_body,
                         onlineMeetingProvider):
    '''
    :param config:
    :param attendees_list: list of attendees going to attend meeting/ list of dictionaries of name and email
    :param start_time: meeting start time
    :param end_time: meeting end time
    :param token: organizer token
    :param subject: meeting subject/agenda
    :param location: meeting location
    :param mail_body: content to include in mail body
    :return:
    '''
    fmt_url = config.MS_GRAPH_ENDPOINT.format('/me/calendar/events')

    query_parameter = {
        "subject": "",
        "body": {
            "contentType": "HTML",
            "content": mail_body
        },
        "start": {
            "dateTime": "",
            "timeZone": ""
        },
        "end": {
            "dateTime": "",
            "timeZone": ""
        },
        "location": {
            "displayName": ""
        },
        "attendees": [
        ]
    }

    query_parameter['subject'] = subject
    query_parameter['start']['dateTime'] = start_time

    query_parameter['start']['timeZone'] = TIME_ZONE
    query_parameter['end']['dateTime'] = end_time
    query_parameter['end']['timeZone'] = TIME_ZONE
    query_parameter['location']['displayName'] = location
    if onlineMeetingProvider:
        query_parameter['onlineMeeting'] = {
            "conferenceId": "",
            "joinUrl": onlineMeetingProvider,
            "phones": [],
            "quickDial": "",
            "tollFreeNumbers": [],
            "tollNumber": ""
        }

    for attendee in attendees_list:
        query_parameter['attendees'].append({
            "emailAddress": {
                "address": attendee['email'],
                "name": attendee['name']
            },
            "type": "required"
        })

    req = web_helpers.make_api_call('POST', fmt_url, token,
                                    payload=query_parameter)
    if req.status_code == 201:
        return req.json()
    return None


def o365_cancel_meeting_handler(config,
                                token,
                                event_id):
    '''
    :param config: contains all configuration
    :param token: user access token
    :param event_id:
    :return:
    '''
    fmt_url = config.MS_GRAPH_ENDPOINT.format("/me/events/" + event_id)
    # print(fmt_url)

    # query_parameter = {
    #       "subject": "test poc for automatic cancel meeting in jinie",
    #       "body": {
    #         "contentType": "HTML",
    #         "content": "test poc for automatic cancel meeting in jinie"
    #       },
    #       "start": {
    #           "dateTime": "2019-09-10T16:30:00",
    #           "timeZone": "Pacific Standard Time"
    #       },
    #       "end": {
    #           "dateTime": "2019-09-10T17:00:00",
    #           "timeZone": "Pacific Standard Time"
    #       },
    #       "location":{
    #           "displayName":"office"
    #       },
    #       "attendees": [
    #         {
    #           "emailAddress": {
    #             "address":"aakash.roy@peoplestrong.com",
    #             "name": "aakash"
    #           },
    #           "type": "required"
    #         }
    #       ]
    #     }

    req = web_helpers.make_api_call('DELETE',
                                    fmt_url,
                                    token)
    return req.status_code == 204


def reply_all(email_id, user_details, config, isApprover):
    """
    Get Event
    :param event_id: Event id
    :param user_details: User Details
    :param config: Config
    :param select: parameters to be selected from Dictionary returned by MS_Graph
    :return: Event
    """

    result = None
    try:
        reply_all_url = config.MS_GRAPH_ENDPOINT.format(
            "/me/messages/{id}/replyAll".format(id=email_id))
        headers = {'User-Agent': 'python_agent/1.0',
                   'Authorization': 'Bearer {0}'.format(user_details["access_token"]),
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'}
        if isApprover:
            mail_content = json.dumps({
                "comment": "Approved. <br><br> Email Powered by Peoplestrong Zippi"
            })
        else:
            mail_content = json.dumps({
                "comment": "Rejected.<br><br>Email Powered by Peoplestrong Zippi"
            })
        result = requests.post(reply_all_url,
                               headers=headers,
                               data=mail_content)
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())
    return result


def send_mail(to, body, subject, user_data, config):
    token_str = user_data['access_token']
    headers = {'User-Agent': 'python_tutorial/1.0',
               'Authorization': 'Bearer {0}'.format(token_str),
               'Accept': 'application/json',
               'Content-Type': 'application/json'
               }
    request_id = str(uuid.uuid4())
    instrumentation = {'client-request-id': request_id,
                       'return-client-request-id': 'true'}
    headers.update(instrumentation)
    headers.update({'Content-Type': 'application/json'})
    body = body + u"<br/><br/>P.S.: Email Powered by PeopleStrong Zippi"

    email_msg = {
        "message": {
            "subject": subject,
            "body": {
                "contentType": "HTML",
                "content": body
            },
            "toRecipients": [
            ],
        },
        "saveToSentItems": "true"
    }

    for t in to:
        email_msg["message"]["toRecipients"].append({
            "emailAddress": {
                "address": str(t)
            }
        })

    fmt_url = config.MS_GRAPH_ENDPOINT.format("/me/sendMail/")
    response = requests.post(fmt_url, headers=headers, data=json.dumps(email_msg))
    print(response.text)
    status_code = response.status_code
    response.close()
    return status_code


def get_mime_email(mail_id, user_details, config):
    result = None
    try:
        get_messages_url = config.MS_GRAPH_ENDPOINT.format('/me/messages/{}/$value'.format(mail_id))
        req = web_helpers.make_api_call('GET', get_messages_url, user_details['access_token'])

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.content
    except Exception:
        traceback.print_exc()
    return result


def get_email(email_id, user_details, config,
              select="uniqueBody"):
    """
    Get Event
    :param event_id: Event id
    :param user_details: User Details
    :param config: Config
    :param select: parameters to be selected from Dictionary returned by MS_Graph
    :return: Event
    """

    result = None
    try:
        get_events_url = config.MS_GRAPH_ENDPOINT.format('/me/messages/' + str(email_id))

        if select == "all":
            query_parameters = None
        else:
            query_parameters = {
                '$select': select
            }

        req = web_helpers.make_api_call('GET', get_events_url, user_details['access_token'],
                                        parameters=query_parameters)

        # pylint: disable=no-member
        if req.status_code == requests.codes.ok:
            result = req.json()
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result


def msteams_create_online_meeting(meeting_subject, start_datetime, end_datetime,
                                  user_details, config):
    """
    Get Event
    :param event_id: Event id
    :param user_details: User Details
    :param config: Config
    :param select: parameters to be selected from Dictionary returned by MS_Graph
    :return: Event
    """

    try:
        colm_endpoint = config.MS_GRAPH_ENDPOINT.format("/me/onlineMeetings")
        headers = {'User-Agent': 'python_agent/1.0',
                   'Authorization': 'Bearer {0}'.format(user_details["access_token"]),
                   'Accept': 'application/json',
                   'Content-Type': 'application/json'}
        result = requests.post(colm_endpoint,
                               headers=headers,
                               data=json.dumps({
                                   "autoAdmittedUsers": "Everyone",
                                   "subject": meeting_subject,
                                   "startDateTime": start_datetime,
                                   "endDateTime": end_datetime,
                                   "participants": {
                                       "organizer": {
                                           "identity": {
                                               "user": {
                                                   "id": user_details["ms_id"]
                                               }
                                           }
                                       }
                                   }
                               }))
    except Exception:
        result = None
        logging.log(logging.SEVERITY_CRITICAL, "o365-HELPERS",
                    traceback.format_exc())

    return result


def convert_datetime_timezone(dt, source_tz, dest_tz):
    tz_src = pytz.timezone(source_tz)
    tz_dest = pytz.timezone(dest_tz)
    dt = dt.replace(tzinfo=None)
    dt = tz_src.localize(dt)
    dt = dt.astimezone(tz_dest)
    return dt.replace(tzinfo=None)
